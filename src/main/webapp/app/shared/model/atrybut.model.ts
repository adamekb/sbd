import { IProdukt } from 'app/shared/model//produkt.model';

export interface IAtrybut {
    id?: number;
    nazwa?: string;
    produkts?: IProdukt[];
}

export class Atrybut implements IAtrybut {
    constructor(public id?: number, public nazwa?: string, public produkts?: IProdukt[]) {}
}
