import { IPozycjazamowienia } from 'app/shared/model//pozycjazamowienia.model';
import { IAtrybut } from 'app/shared/model//atrybut.model';

export interface IProdukt {
    id?: number;
    nazwa?: string;
    cena?: number;
    ilosc?: number;
    zdjecieId?: number;
    pozycjazamowienias?: IPozycjazamowienia[];
    atrybuts?: IAtrybut[];
}

export class Produkt implements IProdukt {
    constructor(
        public id?: number,
        public nazwa?: string,
        public cena?: number,
        public ilosc?: number,
        public zdjecieId?: number,
        public pozycjazamowienias?: IPozycjazamowienia[],
        public atrybuts?: IAtrybut[]
    ) {}
}
