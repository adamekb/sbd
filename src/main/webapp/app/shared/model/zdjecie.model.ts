export interface IZdjecie {
    id?: number;
    opis?: string;
    produktId?: number;
}

export class Zdjecie implements IZdjecie {
    constructor(public id?: number, public opis?: string, public produktId?: number) {}
}
