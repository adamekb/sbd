export interface ISposobdostawy {
    id?: number;
    rodzaj?: string;
    cena?: number;
    zamowienieId?: number;
}

export class Sposobdostawy implements ISposobdostawy {
    constructor(public id?: number, public rodzaj?: string, public cena?: number, public zamowienieId?: number) {}
}
