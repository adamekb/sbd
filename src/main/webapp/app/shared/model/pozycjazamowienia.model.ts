export interface IPozycjazamowienia {
    id?: number;
    pozycja?: number;
    cenasztuka?: number;
    ilosc?: number;
    zamowienieId?: number;
    produktId?: number;
}

export class Pozycjazamowienia implements IPozycjazamowienia {
    constructor(
        public id?: number,
        public pozycja?: number,
        public cenasztuka?: number,
        public ilosc?: number,
        public zamowienieId?: number,
        public produktId?: number
    ) {}
}
