export interface IRabat {
    id?: number;
    nazwa?: string;
    wielkosc?: number;
    zamowienieId?: number;
}

export class Rabat implements IRabat {
    constructor(public id?: number, public nazwa?: string, public wielkosc?: number, public zamowienieId?: number) {}
}
