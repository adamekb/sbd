import { IPozycjazamowienia } from 'app/shared/model//pozycjazamowienia.model';
import { ISposobdostawy } from 'app/shared/model//sposobdostawy.model';

export interface IZamowienie {
    id?: number;
    cenacalkowita?: number;
    iloscproduktow?: number;
    rabatId?: number;
    adresId?: number;
    pozycjazamowienias?: IPozycjazamowienia[];
    sposobdostawies?: ISposobdostawy[];
    userId?: number;
    statusId?: number;
}

export class Zamowienie implements IZamowienie {
    constructor(
        public id?: number,
        public cenacalkowita?: number,
        public iloscproduktow?: number,
        public rabatId?: number,
        public adresId?: number,
        public pozycjazamowienias?: IPozycjazamowienia[],
        public sposobdostawies?: ISposobdostawy[],
        public userId?: number,
        public statusId?: number
    ) {}
}
