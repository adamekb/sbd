import { IZamowienie } from 'app/shared/model//zamowienie.model';

export interface IStatus {
    id?: number;
    status?: string;
    zamowienies?: IZamowienie[];
}

export class Status implements IStatus {
    constructor(public id?: number, public status?: string, public zamowienies?: IZamowienie[]) {}
}
