export interface IAdres {
    id?: number;
    panstwo?: string;
    miasto?: string;
    ulica?: string;
    numerdomu?: number;
    numermieszkania?: number;
    zamowienieId?: number;
}

export class Adres implements IAdres {
    constructor(
        public id?: number,
        public panstwo?: string,
        public miasto?: string,
        public ulica?: string,
        public numerdomu?: number,
        public numermieszkania?: number,
        public zamowienieId?: number
    ) {}
}
