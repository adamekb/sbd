import { NgModule } from '@angular/core';

import { SbdsklepSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [SbdsklepSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [SbdsklepSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class SbdsklepSharedCommonModule {}
