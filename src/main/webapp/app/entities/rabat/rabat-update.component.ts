import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRabat } from 'app/shared/model/rabat.model';
import { RabatService } from './rabat.service';
import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from 'app/entities/zamowienie';

@Component({
    selector: 'jhi-rabat-update',
    templateUrl: './rabat-update.component.html'
})
export class RabatUpdateComponent implements OnInit {
    rabat: IRabat;
    isSaving: boolean;

    zamowienies: IZamowienie[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected rabatService: RabatService,
        protected zamowienieService: ZamowienieService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ rabat }) => {
            this.rabat = rabat;
        });
        this.zamowienieService.query().subscribe(
            (res: HttpResponse<IZamowienie[]>) => {
                this.zamowienies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.rabat.id !== undefined) {
            this.subscribeToSaveResponse(this.rabatService.update(this.rabat));
        } else {
            this.subscribeToSaveResponse(this.rabatService.create(this.rabat));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IRabat>>) {
        result.subscribe((res: HttpResponse<IRabat>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackZamowienieById(index: number, item: IZamowienie) {
        return item.id;
    }
}
