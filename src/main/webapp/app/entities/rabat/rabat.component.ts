import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRabat } from 'app/shared/model/rabat.model';
import { AccountService } from 'app/core';
import { RabatService } from './rabat.service';

@Component({
    selector: 'jhi-rabat',
    templateUrl: './rabat.component.html'
})
export class RabatComponent implements OnInit, OnDestroy {
    rabats: IRabat[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected rabatService: RabatService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.rabatService.query().subscribe(
            (res: HttpResponse<IRabat[]>) => {
                this.rabats = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRabats();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRabat) {
        return item.id;
    }

    registerChangeInRabats() {
        this.eventSubscriber = this.eventManager.subscribe('rabatListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
