import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Rabat } from 'app/shared/model/rabat.model';
import { RabatService } from './rabat.service';
import { RabatComponent } from './rabat.component';
import { RabatDetailComponent } from './rabat-detail.component';
import { RabatUpdateComponent } from './rabat-update.component';
import { RabatDeletePopupComponent } from './rabat-delete-dialog.component';
import { IRabat } from 'app/shared/model/rabat.model';

@Injectable({ providedIn: 'root' })
export class RabatResolve implements Resolve<IRabat> {
    constructor(private service: RabatService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Rabat> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Rabat>) => response.ok),
                map((rabat: HttpResponse<Rabat>) => rabat.body)
            );
        }
        return of(new Rabat());
    }
}

export const rabatRoute: Routes = [
    {
        path: 'rabat',
        component: RabatComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rabats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rabat/:id/view',
        component: RabatDetailComponent,
        resolve: {
            rabat: RabatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rabats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rabat/new',
        component: RabatUpdateComponent,
        resolve: {
            rabat: RabatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rabats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rabat/:id/edit',
        component: RabatUpdateComponent,
        resolve: {
            rabat: RabatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rabats'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rabatPopupRoute: Routes = [
    {
        path: 'rabat/:id/delete',
        component: RabatDeletePopupComponent,
        resolve: {
            rabat: RabatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rabats'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
