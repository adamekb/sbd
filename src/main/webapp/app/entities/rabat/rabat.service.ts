import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRabat } from 'app/shared/model/rabat.model';

type EntityResponseType = HttpResponse<IRabat>;
type EntityArrayResponseType = HttpResponse<IRabat[]>;

@Injectable({ providedIn: 'root' })
export class RabatService {
    public resourceUrl = SERVER_API_URL + 'api/rabats';

    constructor(protected http: HttpClient) {}

    create(rabat: IRabat): Observable<EntityResponseType> {
        return this.http.post<IRabat>(this.resourceUrl, rabat, { observe: 'response' });
    }

    update(rabat: IRabat): Observable<EntityResponseType> {
        return this.http.put<IRabat>(this.resourceUrl, rabat, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRabat>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRabat[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
