import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRabat } from 'app/shared/model/rabat.model';

@Component({
    selector: 'jhi-rabat-detail',
    templateUrl: './rabat-detail.component.html'
})
export class RabatDetailComponent implements OnInit {
    rabat: IRabat;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ rabat }) => {
            this.rabat = rabat;
        });
    }

    previousState() {
        window.history.back();
    }
}
