import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    RabatComponent,
    RabatDetailComponent,
    RabatUpdateComponent,
    RabatDeletePopupComponent,
    RabatDeleteDialogComponent,
    rabatRoute,
    rabatPopupRoute
} from './';

const ENTITY_STATES = [...rabatRoute, ...rabatPopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [RabatComponent, RabatDetailComponent, RabatUpdateComponent, RabatDeleteDialogComponent, RabatDeletePopupComponent],
    entryComponents: [RabatComponent, RabatUpdateComponent, RabatDeleteDialogComponent, RabatDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepRabatModule {}
