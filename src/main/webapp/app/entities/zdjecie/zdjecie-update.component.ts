import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IZdjecie } from 'app/shared/model/zdjecie.model';
import { ZdjecieService } from './zdjecie.service';
import { IProdukt } from 'app/shared/model/produkt.model';
import { ProduktService } from 'app/entities/produkt';

@Component({
    selector: 'jhi-zdjecie-update',
    templateUrl: './zdjecie-update.component.html'
})
export class ZdjecieUpdateComponent implements OnInit {
    zdjecie: IZdjecie;
    isSaving: boolean;

    produkts: IProdukt[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected zdjecieService: ZdjecieService,
        protected produktService: ProduktService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ zdjecie }) => {
            this.zdjecie = zdjecie;
        });
        this.produktService.query().subscribe(
            (res: HttpResponse<IProdukt[]>) => {
                this.produkts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.zdjecie.id !== undefined) {
            this.subscribeToSaveResponse(this.zdjecieService.update(this.zdjecie));
        } else {
            this.subscribeToSaveResponse(this.zdjecieService.create(this.zdjecie));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IZdjecie>>) {
        result.subscribe((res: HttpResponse<IZdjecie>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProduktById(index: number, item: IProdukt) {
        return item.id;
    }
}
