import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IZdjecie } from 'app/shared/model/zdjecie.model';

@Component({
    selector: 'jhi-zdjecie-detail',
    templateUrl: './zdjecie-detail.component.html'
})
export class ZdjecieDetailComponent implements OnInit {
    zdjecie: IZdjecie;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ zdjecie }) => {
            this.zdjecie = zdjecie;
        });
    }

    previousState() {
        window.history.back();
    }
}
