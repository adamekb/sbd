export * from './zdjecie.service';
export * from './zdjecie-update.component';
export * from './zdjecie-delete-dialog.component';
export * from './zdjecie-detail.component';
export * from './zdjecie.component';
export * from './zdjecie.route';
