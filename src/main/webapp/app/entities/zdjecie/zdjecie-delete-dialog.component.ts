import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IZdjecie } from 'app/shared/model/zdjecie.model';
import { ZdjecieService } from './zdjecie.service';

@Component({
    selector: 'jhi-zdjecie-delete-dialog',
    templateUrl: './zdjecie-delete-dialog.component.html'
})
export class ZdjecieDeleteDialogComponent {
    zdjecie: IZdjecie;

    constructor(protected zdjecieService: ZdjecieService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.zdjecieService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'zdjecieListModification',
                content: 'Deleted an zdjecie'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-zdjecie-delete-popup',
    template: ''
})
export class ZdjecieDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ zdjecie }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ZdjecieDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.zdjecie = zdjecie;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
