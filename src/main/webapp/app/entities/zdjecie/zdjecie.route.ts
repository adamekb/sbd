import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Zdjecie } from 'app/shared/model/zdjecie.model';
import { ZdjecieService } from './zdjecie.service';
import { ZdjecieComponent } from './zdjecie.component';
import { ZdjecieDetailComponent } from './zdjecie-detail.component';
import { ZdjecieUpdateComponent } from './zdjecie-update.component';
import { ZdjecieDeletePopupComponent } from './zdjecie-delete-dialog.component';
import { IZdjecie } from 'app/shared/model/zdjecie.model';

@Injectable({ providedIn: 'root' })
export class ZdjecieResolve implements Resolve<IZdjecie> {
    constructor(private service: ZdjecieService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Zdjecie> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Zdjecie>) => response.ok),
                map((zdjecie: HttpResponse<Zdjecie>) => zdjecie.body)
            );
        }
        return of(new Zdjecie());
    }
}

export const zdjecieRoute: Routes = [
    {
        path: 'zdjecie',
        component: ZdjecieComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zdjecies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zdjecie/:id/view',
        component: ZdjecieDetailComponent,
        resolve: {
            zdjecie: ZdjecieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zdjecies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zdjecie/new',
        component: ZdjecieUpdateComponent,
        resolve: {
            zdjecie: ZdjecieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zdjecies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zdjecie/:id/edit',
        component: ZdjecieUpdateComponent,
        resolve: {
            zdjecie: ZdjecieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zdjecies'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const zdjeciePopupRoute: Routes = [
    {
        path: 'zdjecie/:id/delete',
        component: ZdjecieDeletePopupComponent,
        resolve: {
            zdjecie: ZdjecieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zdjecies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
