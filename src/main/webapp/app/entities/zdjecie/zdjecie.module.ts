import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    ZdjecieComponent,
    ZdjecieDetailComponent,
    ZdjecieUpdateComponent,
    ZdjecieDeletePopupComponent,
    ZdjecieDeleteDialogComponent,
    zdjecieRoute,
    zdjeciePopupRoute
} from './';

const ENTITY_STATES = [...zdjecieRoute, ...zdjeciePopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ZdjecieComponent,
        ZdjecieDetailComponent,
        ZdjecieUpdateComponent,
        ZdjecieDeleteDialogComponent,
        ZdjecieDeletePopupComponent
    ],
    entryComponents: [ZdjecieComponent, ZdjecieUpdateComponent, ZdjecieDeleteDialogComponent, ZdjecieDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepZdjecieModule {}
