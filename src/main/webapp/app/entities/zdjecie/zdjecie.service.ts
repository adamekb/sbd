import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IZdjecie } from 'app/shared/model/zdjecie.model';

type EntityResponseType = HttpResponse<IZdjecie>;
type EntityArrayResponseType = HttpResponse<IZdjecie[]>;

@Injectable({ providedIn: 'root' })
export class ZdjecieService {
    public resourceUrl = SERVER_API_URL + 'api/zdjecies';

    constructor(protected http: HttpClient) {}

    create(zdjecie: IZdjecie): Observable<EntityResponseType> {
        return this.http.post<IZdjecie>(this.resourceUrl, zdjecie, { observe: 'response' });
    }

    update(zdjecie: IZdjecie): Observable<EntityResponseType> {
        return this.http.put<IZdjecie>(this.resourceUrl, zdjecie, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IZdjecie>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IZdjecie[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
