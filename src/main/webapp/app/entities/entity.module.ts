import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SbdsklepAdresModule } from './adres/adres.module';
import { SbdsklepProduktModule } from './produkt/produkt.module';
import { SbdsklepZamowienieModule } from './zamowienie/zamowienie.module';
import { SbdsklepPozycjazamowieniaModule } from './pozycjazamowienia/pozycjazamowienia.module';
import { SbdsklepStatusModule } from './status/status.module';
import { SbdsklepSposobdostawyModule } from './sposobdostawy/sposobdostawy.module';
import { SbdsklepAtrybutModule } from './atrybut/atrybut.module';
import { SbdsklepRabatModule } from './rabat/rabat.module';
import { SbdsklepZdjecieModule } from './zdjecie/zdjecie.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        SbdsklepAdresModule,
        SbdsklepProduktModule,
        SbdsklepZamowienieModule,
        SbdsklepPozycjazamowieniaModule,
        SbdsklepStatusModule,
        SbdsklepSposobdostawyModule,
        SbdsklepAtrybutModule,
        SbdsklepRabatModule,
        SbdsklepZdjecieModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepEntityModule {}
