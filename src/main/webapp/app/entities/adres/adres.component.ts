import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAdres } from 'app/shared/model/adres.model';
import { AccountService } from 'app/core';
import { AdresService } from './adres.service';

@Component({
    selector: 'jhi-adres',
    templateUrl: './adres.component.html'
})
export class AdresComponent implements OnInit, OnDestroy {
    adres: IAdres[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected adresService: AdresService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.adresService.query().subscribe(
            (res: HttpResponse<IAdres[]>) => {
                this.adres = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAdres();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAdres) {
        return item.id;
    }

    registerChangeInAdres() {
        this.eventSubscriber = this.eventManager.subscribe('adresListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
