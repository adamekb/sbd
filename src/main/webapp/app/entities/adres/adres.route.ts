import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Adres } from 'app/shared/model/adres.model';
import { AdresService } from './adres.service';
import { AdresComponent } from './adres.component';
import { AdresDetailComponent } from './adres-detail.component';
import { AdresUpdateComponent } from './adres-update.component';
import { AdresDeletePopupComponent } from './adres-delete-dialog.component';
import { IAdres } from 'app/shared/model/adres.model';
import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { Zamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from './zamowienie.service';

@Injectable({ providedIn: 'root' })
export class AdresResolve implements Resolve<IAdres> {
    constructor(private service: AdresService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Adres> {
        const id = route.params['id'] ? route.params['id'] : null;        
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Adres>) => response.ok),
                map((adres: HttpResponse<Adres>) => adres.body)
            );
        }

        return of(new Adres());
    }
}

export const adresRoute: Routes = [
    {
        path: 'adres',
        component: AdresComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Adres'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'adres/:id/view',
        component: AdresDetailComponent,
        resolve: {
            adres: AdresResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Adres'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'adres/new',
        component: AdresUpdateComponent,
        resolve: {
            adres: AdresResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Adres'
        },
        canActivate: [UserRouteAccessService]
    },
    
    {
        path: 'adres/:id/edit',
        component: AdresUpdateComponent,
        resolve: {
            adres: AdresResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Adres'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const adresPopupRoute: Routes = [
    {
        path: 'adres/:id/delete',
        component: AdresDeletePopupComponent,
        resolve: {
            adres: AdresResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Adres'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
