import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    AdresComponent,
    AdresDetailComponent,
    AdresUpdateComponent,
    AdresDeletePopupComponent,
    AdresDeleteDialogComponent,
    adresRoute,
    adresPopupRoute
} from './';

const ENTITY_STATES = [...adresRoute, ...adresPopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [AdresComponent, AdresDetailComponent, AdresUpdateComponent, AdresDeleteDialogComponent, AdresDeletePopupComponent],
    entryComponents: [AdresComponent, AdresUpdateComponent, AdresDeleteDialogComponent, AdresDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepAdresModule {}
