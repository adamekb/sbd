import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IAdres } from 'app/shared/model/adres.model';
import { AdresService } from './adres.service';
import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from 'app/entities/zamowienie';

@Component({
    selector: 'jhi-adres-update',
    templateUrl: './adres-update.component.html'
})
export class AdresUpdateComponent implements OnInit {
    adres: IAdres;
    isSaving: boolean;

    zamowienies: IZamowienie[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected adresService: AdresService,
        protected zamowienieService: ZamowienieService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ adres }) => {
            this.adres = adres;
        });
        this.zamowienieService.query().subscribe(
            (res: HttpResponse<IZamowienie[]>) => {
                this.zamowienies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
         window.location = 'http://localhost:9000/#/';
    }

    save() {
        this.isSaving = true;
        if (this.adres.id !== undefined) {
            this.subscribeToSaveResponse(this.adresService.update(this.adres));
        } else {
            this.subscribeToSaveResponse(this.adresService.create(this.adres));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAdres>>) {
        result.subscribe((res: HttpResponse<IAdres>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackZamowienieById(index: number, item: IZamowienie) {
        return item.id;
    }
}
