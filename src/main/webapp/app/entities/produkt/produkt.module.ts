import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    ProduktComponent,
    ProduktDetailComponent,
    ProduktUpdateComponent,
    ProduktDeletePopupComponent,
    ProduktDeleteDialogComponent,
    produktRoute,
    produktPopupRoute
} from './';

const ENTITY_STATES = [...produktRoute, ...produktPopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProduktComponent,
        ProduktDetailComponent,
        ProduktUpdateComponent,
        ProduktDeleteDialogComponent,
        ProduktDeletePopupComponent
    ],
    entryComponents: [ProduktComponent, ProduktUpdateComponent, ProduktDeleteDialogComponent, ProduktDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepProduktModule {}
