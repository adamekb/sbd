import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IProdukt } from 'app/shared/model/produkt.model';
import { AccountService } from 'app/core';
import { ProduktService } from './produkt.service';

@Component({
    selector: 'jhi-produkt',
    templateUrl: './produkt.component.html'
})
export class ProduktComponent implements OnInit, OnDestroy {
    produkts: IProdukt[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected produktService: ProduktService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.produktService.query().subscribe(
            (res: HttpResponse<IProdukt[]>) => {
                this.produkts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProdukts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProdukt) {
        return item.id;
    }

    registerChangeInProdukts() {
        this.eventSubscriber = this.eventManager.subscribe('produktListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
