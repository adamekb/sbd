import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IProdukt } from 'app/shared/model/produkt.model';
import { ProduktService } from './produkt.service';
import { IZdjecie } from 'app/shared/model/zdjecie.model';
import { ZdjecieService } from 'app/entities/zdjecie';
import { IAtrybut } from 'app/shared/model/atrybut.model';
import { AtrybutService } from 'app/entities/atrybut';

@Component({
    selector: 'jhi-produkt-update',
    templateUrl: './produkt-update.component.html'
})
export class ProduktUpdateComponent implements OnInit {
    produkt: IProdukt;
    isSaving: boolean;

    zdjecies: IZdjecie[];

    atrybuts: IAtrybut[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected produktService: ProduktService,
        protected zdjecieService: ZdjecieService,
        protected atrybutService: AtrybutService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ produkt }) => {
            this.produkt = produkt;
        });
        this.zdjecieService.query({ filter: 'produkt-is-null' }).subscribe(
            (res: HttpResponse<IZdjecie[]>) => {
                if (!this.produkt.zdjecieId) {
                    this.zdjecies = res.body;
                } else {
                    this.zdjecieService.find(this.produkt.zdjecieId).subscribe(
                        (subRes: HttpResponse<IZdjecie>) => {
                            this.zdjecies = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.atrybutService.query().subscribe(
            (res: HttpResponse<IAtrybut[]>) => {
                this.atrybuts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.produkt.id !== undefined) {
            this.subscribeToSaveResponse(this.produktService.update(this.produkt));
        } else {
            this.subscribeToSaveResponse(this.produktService.create(this.produkt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IProdukt>>) {
        result.subscribe((res: HttpResponse<IProdukt>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackZdjecieById(index: number, item: IZdjecie) {
        return item.id;
    }

    trackAtrybutById(index: number, item: IAtrybut) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
