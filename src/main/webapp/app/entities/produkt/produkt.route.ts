import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Produkt } from 'app/shared/model/produkt.model';
import { ProduktService } from './produkt.service';
import { ProduktComponent } from './produkt.component';
import { ProduktDetailComponent } from './produkt-detail.component';
import { ProduktUpdateComponent } from './produkt-update.component';
import { ProduktDeletePopupComponent } from './produkt-delete-dialog.component';
import { IProdukt } from 'app/shared/model/produkt.model';

@Injectable({ providedIn: 'root' })
export class ProduktResolve implements Resolve<IProdukt> {
    constructor(private service: ProduktService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Produkt> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Produkt>) => response.ok),
                map((produkt: HttpResponse<Produkt>) => produkt.body)
            );
        }
        return of(new Produkt());
    }
}

export const produktRoute: Routes = [
    {
        path: 'produkt',
        component: ProduktComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Produkts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'produkt/:id/view',
        component: ProduktDetailComponent,
        resolve: {
            produkt: ProduktResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Produkts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'produkt/new',
        component: ProduktUpdateComponent,
        resolve: {
            produkt: ProduktResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Produkts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'produkt/:id/edit',
        component: ProduktUpdateComponent,
        resolve: {
            produkt: ProduktResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Produkts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const produktPopupRoute: Routes = [
    {
        path: 'produkt/:id/delete',
        component: ProduktDeletePopupComponent,
        resolve: {
            produkt: ProduktResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Produkts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
