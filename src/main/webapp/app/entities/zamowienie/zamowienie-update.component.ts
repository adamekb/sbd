import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from './zamowienie.service';
import { IRabat } from 'app/shared/model/rabat.model';
import { RabatService } from 'app/entities/rabat';
import { IAdres } from 'app/shared/model/adres.model';
import { AdresService } from 'app/entities/adres';
import { IUser, UserService } from 'app/core';
import { IStatus } from 'app/shared/model/status.model';
import { StatusService } from 'app/entities/status';

@Component({
    selector: 'jhi-zamowienie-update',
    templateUrl: './zamowienie-update.component.html'
})
export class ZamowienieUpdateComponent implements OnInit {
    zamowienie: IZamowienie;
    isSaving: boolean;

    rabats: IRabat[];

    adres: IAdres[];

    users: IUser[];

    statuses: IStatus[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected zamowienieService: ZamowienieService,
        protected rabatService: RabatService,
        protected adresService: AdresService,
        protected userService: UserService,
        protected statusService: StatusService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ zamowienie }) => {
            this.zamowienie = zamowienie;
        });
        this.rabatService.query({ filter: 'zamowienie-is-null' }).subscribe(
            (res: HttpResponse<IRabat[]>) => {
                if (!this.zamowienie.rabatId) {
                    this.rabats = res.body;
                } else {
                    this.rabatService.find(this.zamowienie.rabatId).subscribe(
                        (subRes: HttpResponse<IRabat>) => {
                            this.rabats = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.adresService.query({ filter: 'zamowienie-is-null' }).subscribe(
            (res: HttpResponse<IAdres[]>) => {
                if (!this.zamowienie.adresId) {
                    this.adres = res.body;
                } else {
                    this.adresService.find(this.zamowienie.adresId).subscribe(
                        (subRes: HttpResponse<IAdres>) => {
                            this.adres = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.statusService.query().subscribe(
            (res: HttpResponse<IStatus[]>) => {
                this.statuses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.zamowienie.id !== undefined) {
            this.subscribeToSaveResponse(this.zamowienieService.update(this.zamowienie));
        } else {
            this.subscribeToSaveResponse(this.zamowienieService.create(this.zamowienie));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IZamowienie>>) {
        result.subscribe((res: HttpResponse<IZamowienie>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRabatById(index: number, item: IRabat) {
        return item.id;
    }

    trackAdresById(index: number, item: IAdres) {
        return item.id;
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    trackStatusById(index: number, item: IStatus) {
        return item.id;
    }
}
