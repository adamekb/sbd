import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { AccountService } from 'app/core';
import { ZamowienieService } from './zamowienie.service';

@Component({
    selector: 'jhi-zamowienie',
    templateUrl: './zamowienie.component.html'
})
export class ZamowienieComponent implements OnInit, OnDestroy {
    zamowienies: IZamowienie[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected zamowienieService: ZamowienieService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.zamowienieService.query().subscribe(
            (res: HttpResponse<IZamowienie[]>) => {
                this.zamowienies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInZamowienies();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IZamowienie) {
        return item.id;
    }

    registerChangeInZamowienies() {
        this.eventSubscriber = this.eventManager.subscribe('zamowienieListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
