import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Zamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from './zamowienie.service';
import { ZamowienieComponent } from './zamowienie.component';
import { ZamowienieDetailComponent } from './zamowienie-detail.component';
import { ZamowienieUpdateComponent } from './zamowienie-update.component';
import { ZamowienieDeletePopupComponent } from './zamowienie-delete-dialog.component';
import { IZamowienie } from 'app/shared/model/zamowienie.model';

@Injectable({ providedIn: 'root' })
export class ZamowienieResolve implements Resolve<IZamowienie> {
    constructor(private service: ZamowienieService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Zamowienie> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Zamowienie>) => response.ok),
                map((zamowienie: HttpResponse<Zamowienie>) => zamowienie.body)
            );
        }
        return of(new Zamowienie());
    }
}

export const zamowienieRoute: Routes = [
    {
        path: 'zamowienie',
        component: ZamowienieComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zamowienies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zamowienie/:id/view',
        component: ZamowienieDetailComponent,
        resolve: {
            zamowienie: ZamowienieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zamowienies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zamowienie/new',
        component: ZamowienieUpdateComponent,
        resolve: {
            zamowienie: ZamowienieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zamowienies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zamowienie/:id/edit',
        component: ZamowienieUpdateComponent,
        resolve: {
            zamowienie: ZamowienieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zamowienies'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const zamowieniePopupRoute: Routes = [
    {
        path: 'zamowienie/:id/delete',
        component: ZamowienieDeletePopupComponent,
        resolve: {
            zamowienie: ZamowienieResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Zamowienies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
