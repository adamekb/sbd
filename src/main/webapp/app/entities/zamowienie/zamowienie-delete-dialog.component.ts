import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from './zamowienie.service';

@Component({
    selector: 'jhi-zamowienie-delete-dialog',
    templateUrl: './zamowienie-delete-dialog.component.html'
})
export class ZamowienieDeleteDialogComponent {
    zamowienie: IZamowienie;

    constructor(
        protected zamowienieService: ZamowienieService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.zamowienieService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'zamowienieListModification',
                content: 'Deleted an zamowienie'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-zamowienie-delete-popup',
    template: ''
})
export class ZamowienieDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ zamowienie }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ZamowienieDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.zamowienie = zamowienie;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
