import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import { SbdsklepAdminModule } from 'app/admin/admin.module';
import {
    ZamowienieComponent,
    ZamowienieDetailComponent,
    ZamowienieUpdateComponent,
    ZamowienieDeletePopupComponent,
    ZamowienieDeleteDialogComponent,
    zamowienieRoute,
    zamowieniePopupRoute
} from './';

const ENTITY_STATES = [...zamowienieRoute, ...zamowieniePopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, SbdsklepAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ZamowienieComponent,
        ZamowienieDetailComponent,
        ZamowienieUpdateComponent,
        ZamowienieDeleteDialogComponent,
        ZamowienieDeletePopupComponent
    ],
    entryComponents: [ZamowienieComponent, ZamowienieUpdateComponent, ZamowienieDeleteDialogComponent, ZamowienieDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepZamowienieModule {}
