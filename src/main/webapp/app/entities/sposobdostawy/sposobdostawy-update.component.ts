import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ISposobdostawy } from 'app/shared/model/sposobdostawy.model';
import { SposobdostawyService } from './sposobdostawy.service';
import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from 'app/entities/zamowienie';

@Component({
    selector: 'jhi-sposobdostawy-update',
    templateUrl: './sposobdostawy-update.component.html'
})
export class SposobdostawyUpdateComponent implements OnInit {
    sposobdostawy: ISposobdostawy;
    isSaving: boolean;

    zamowienies: IZamowienie[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected sposobdostawyService: SposobdostawyService,
        protected zamowienieService: ZamowienieService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sposobdostawy }) => {
            this.sposobdostawy = sposobdostawy;
        });
        this.zamowienieService.query().subscribe(
            (res: HttpResponse<IZamowienie[]>) => {
                this.zamowienies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.sposobdostawy.id !== undefined) {
            this.subscribeToSaveResponse(this.sposobdostawyService.update(this.sposobdostawy));
        } else {
            this.subscribeToSaveResponse(this.sposobdostawyService.create(this.sposobdostawy));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISposobdostawy>>) {
        result.subscribe((res: HttpResponse<ISposobdostawy>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackZamowienieById(index: number, item: IZamowienie) {
        return item.id;
    }
}
