import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISposobdostawy } from 'app/shared/model/sposobdostawy.model';
import { SposobdostawyService } from './sposobdostawy.service';

@Component({
    selector: 'jhi-sposobdostawy-delete-dialog',
    templateUrl: './sposobdostawy-delete-dialog.component.html'
})
export class SposobdostawyDeleteDialogComponent {
    sposobdostawy: ISposobdostawy;

    constructor(
        protected sposobdostawyService: SposobdostawyService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sposobdostawyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sposobdostawyListModification',
                content: 'Deleted an sposobdostawy'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sposobdostawy-delete-popup',
    template: ''
})
export class SposobdostawyDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sposobdostawy }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SposobdostawyDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sposobdostawy = sposobdostawy;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
