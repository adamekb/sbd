import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISposobdostawy } from 'app/shared/model/sposobdostawy.model';

@Component({
    selector: 'jhi-sposobdostawy-detail',
    templateUrl: './sposobdostawy-detail.component.html'
})
export class SposobdostawyDetailComponent implements OnInit {
    sposobdostawy: ISposobdostawy;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sposobdostawy }) => {
            this.sposobdostawy = sposobdostawy;
        });
    }

    previousState() {
        window.history.back();
    }
}
