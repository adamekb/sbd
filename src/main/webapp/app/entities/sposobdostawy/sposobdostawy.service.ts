import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISposobdostawy } from 'app/shared/model/sposobdostawy.model';

type EntityResponseType = HttpResponse<ISposobdostawy>;
type EntityArrayResponseType = HttpResponse<ISposobdostawy[]>;

@Injectable({ providedIn: 'root' })
export class SposobdostawyService {
    public resourceUrl = SERVER_API_URL + 'api/sposobdostawies';

    constructor(protected http: HttpClient) {}

    create(sposobdostawy: ISposobdostawy): Observable<EntityResponseType> {
        return this.http.post<ISposobdostawy>(this.resourceUrl, sposobdostawy, { observe: 'response' });
    }

    update(sposobdostawy: ISposobdostawy): Observable<EntityResponseType> {
        return this.http.put<ISposobdostawy>(this.resourceUrl, sposobdostawy, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISposobdostawy>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISposobdostawy[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
