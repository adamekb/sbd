import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Sposobdostawy } from 'app/shared/model/sposobdostawy.model';
import { SposobdostawyService } from './sposobdostawy.service';
import { SposobdostawyComponent } from './sposobdostawy.component';
import { SposobdostawyDetailComponent } from './sposobdostawy-detail.component';
import { SposobdostawyUpdateComponent } from './sposobdostawy-update.component';
import { SposobdostawyDeletePopupComponent } from './sposobdostawy-delete-dialog.component';
import { ISposobdostawy } from 'app/shared/model/sposobdostawy.model';

@Injectable({ providedIn: 'root' })
export class SposobdostawyResolve implements Resolve<ISposobdostawy> {
    constructor(private service: SposobdostawyService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Sposobdostawy> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Sposobdostawy>) => response.ok),
                map((sposobdostawy: HttpResponse<Sposobdostawy>) => sposobdostawy.body)
            );
        }
        return of(new Sposobdostawy());
    }
}

export const sposobdostawyRoute: Routes = [
    {
        path: 'sposobdostawy',
        component: SposobdostawyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sposobdostawies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sposobdostawy/:id/view',
        component: SposobdostawyDetailComponent,
        resolve: {
            sposobdostawy: SposobdostawyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sposobdostawies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sposobdostawy/new',
        component: SposobdostawyUpdateComponent,
        resolve: {
            sposobdostawy: SposobdostawyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sposobdostawies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sposobdostawy/:id/edit',
        component: SposobdostawyUpdateComponent,
        resolve: {
            sposobdostawy: SposobdostawyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sposobdostawies'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sposobdostawyPopupRoute: Routes = [
    {
        path: 'sposobdostawy/:id/delete',
        component: SposobdostawyDeletePopupComponent,
        resolve: {
            sposobdostawy: SposobdostawyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sposobdostawies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
