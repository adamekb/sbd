import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    SposobdostawyComponent,
    SposobdostawyDetailComponent,
    SposobdostawyUpdateComponent,
    SposobdostawyDeletePopupComponent,
    SposobdostawyDeleteDialogComponent,
    sposobdostawyRoute,
    sposobdostawyPopupRoute
} from './';

const ENTITY_STATES = [...sposobdostawyRoute, ...sposobdostawyPopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SposobdostawyComponent,
        SposobdostawyDetailComponent,
        SposobdostawyUpdateComponent,
        SposobdostawyDeleteDialogComponent,
        SposobdostawyDeletePopupComponent
    ],
    entryComponents: [
        SposobdostawyComponent,
        SposobdostawyUpdateComponent,
        SposobdostawyDeleteDialogComponent,
        SposobdostawyDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepSposobdostawyModule {}
