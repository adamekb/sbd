import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISposobdostawy } from 'app/shared/model/sposobdostawy.model';
import { AccountService } from 'app/core';
import { SposobdostawyService } from './sposobdostawy.service';

@Component({
    selector: 'jhi-sposobdostawy',
    templateUrl: './sposobdostawy.component.html'
})
export class SposobdostawyComponent implements OnInit, OnDestroy {
    sposobdostawies: ISposobdostawy[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected sposobdostawyService: SposobdostawyService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.sposobdostawyService.query().subscribe(
            (res: HttpResponse<ISposobdostawy[]>) => {
                this.sposobdostawies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSposobdostawies();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISposobdostawy) {
        return item.id;
    }

    registerChangeInSposobdostawies() {
        this.eventSubscriber = this.eventManager.subscribe('sposobdostawyListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
