import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAtrybut } from 'app/shared/model/atrybut.model';
import { AtrybutService } from './atrybut.service';

@Component({
    selector: 'jhi-atrybut-delete-dialog',
    templateUrl: './atrybut-delete-dialog.component.html'
})
export class AtrybutDeleteDialogComponent {
    atrybut: IAtrybut;

    constructor(protected atrybutService: AtrybutService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.atrybutService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'atrybutListModification',
                content: 'Deleted an atrybut'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-atrybut-delete-popup',
    template: ''
})
export class AtrybutDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ atrybut }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AtrybutDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.atrybut = atrybut;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
