export * from './atrybut.service';
export * from './atrybut-update.component';
export * from './atrybut-delete-dialog.component';
export * from './atrybut-detail.component';
export * from './atrybut.component';
export * from './atrybut.route';
