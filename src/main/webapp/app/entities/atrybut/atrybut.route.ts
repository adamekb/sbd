import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Atrybut } from 'app/shared/model/atrybut.model';
import { AtrybutService } from './atrybut.service';
import { AtrybutComponent } from './atrybut.component';
import { AtrybutDetailComponent } from './atrybut-detail.component';
import { AtrybutUpdateComponent } from './atrybut-update.component';
import { AtrybutDeletePopupComponent } from './atrybut-delete-dialog.component';
import { IAtrybut } from 'app/shared/model/atrybut.model';

@Injectable({ providedIn: 'root' })
export class AtrybutResolve implements Resolve<IAtrybut> {
    constructor(private service: AtrybutService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Atrybut> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Atrybut>) => response.ok),
                map((atrybut: HttpResponse<Atrybut>) => atrybut.body)
            );
        }
        return of(new Atrybut());
    }
}

export const atrybutRoute: Routes = [
    {
        path: 'atrybut',
        component: AtrybutComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Atrybuts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'atrybut/:id/view',
        component: AtrybutDetailComponent,
        resolve: {
            atrybut: AtrybutResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Atrybuts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'atrybut/new',
        component: AtrybutUpdateComponent,
        resolve: {
            atrybut: AtrybutResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Atrybuts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'atrybut/:id/edit',
        component: AtrybutUpdateComponent,
        resolve: {
            atrybut: AtrybutResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Atrybuts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const atrybutPopupRoute: Routes = [
    {
        path: 'atrybut/:id/delete',
        component: AtrybutDeletePopupComponent,
        resolve: {
            atrybut: AtrybutResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Atrybuts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
