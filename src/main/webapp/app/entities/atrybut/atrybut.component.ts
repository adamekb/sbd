import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAtrybut } from 'app/shared/model/atrybut.model';
import { AccountService } from 'app/core';
import { AtrybutService } from './atrybut.service';

@Component({
    selector: 'jhi-atrybut',
    templateUrl: './atrybut.component.html'
})
export class AtrybutComponent implements OnInit, OnDestroy {
    atrybuts: IAtrybut[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected atrybutService: AtrybutService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.atrybutService.query().subscribe(
            (res: HttpResponse<IAtrybut[]>) => {
                this.atrybuts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAtrybuts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAtrybut) {
        return item.id;
    }

    registerChangeInAtrybuts() {
        this.eventSubscriber = this.eventManager.subscribe('atrybutListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
