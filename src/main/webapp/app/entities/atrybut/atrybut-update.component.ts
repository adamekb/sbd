import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IAtrybut } from 'app/shared/model/atrybut.model';
import { AtrybutService } from './atrybut.service';
import { IProdukt } from 'app/shared/model/produkt.model';
import { ProduktService } from 'app/entities/produkt';

@Component({
    selector: 'jhi-atrybut-update',
    templateUrl: './atrybut-update.component.html'
})
export class AtrybutUpdateComponent implements OnInit {
    atrybut: IAtrybut;
    isSaving: boolean;

    produkts: IProdukt[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected atrybutService: AtrybutService,
        protected produktService: ProduktService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ atrybut }) => {
            this.atrybut = atrybut;
        });
        this.produktService.query().subscribe(
            (res: HttpResponse<IProdukt[]>) => {
                this.produkts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.atrybut.id !== undefined) {
            this.subscribeToSaveResponse(this.atrybutService.update(this.atrybut));
        } else {
            this.subscribeToSaveResponse(this.atrybutService.create(this.atrybut));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAtrybut>>) {
        result.subscribe((res: HttpResponse<IAtrybut>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProduktById(index: number, item: IProdukt) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
