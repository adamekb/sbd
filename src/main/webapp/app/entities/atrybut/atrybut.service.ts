import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAtrybut } from 'app/shared/model/atrybut.model';

type EntityResponseType = HttpResponse<IAtrybut>;
type EntityArrayResponseType = HttpResponse<IAtrybut[]>;

@Injectable({ providedIn: 'root' })
export class AtrybutService {
    public resourceUrl = SERVER_API_URL + 'api/atrybuts';

    constructor(protected http: HttpClient) {}

    create(atrybut: IAtrybut): Observable<EntityResponseType> {
        return this.http.post<IAtrybut>(this.resourceUrl, atrybut, { observe: 'response' });
    }

    update(atrybut: IAtrybut): Observable<EntityResponseType> {
        return this.http.put<IAtrybut>(this.resourceUrl, atrybut, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAtrybut>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAtrybut[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
