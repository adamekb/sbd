import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    AtrybutComponent,
    AtrybutDetailComponent,
    AtrybutUpdateComponent,
    AtrybutDeletePopupComponent,
    AtrybutDeleteDialogComponent,
    atrybutRoute,
    atrybutPopupRoute
} from './';

const ENTITY_STATES = [...atrybutRoute, ...atrybutPopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AtrybutComponent,
        AtrybutDetailComponent,
        AtrybutUpdateComponent,
        AtrybutDeleteDialogComponent,
        AtrybutDeletePopupComponent
    ],
    entryComponents: [AtrybutComponent, AtrybutUpdateComponent, AtrybutDeleteDialogComponent, AtrybutDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepAtrybutModule {}
