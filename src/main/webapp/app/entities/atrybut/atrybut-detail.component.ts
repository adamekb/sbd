import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAtrybut } from 'app/shared/model/atrybut.model';

@Component({
    selector: 'jhi-atrybut-detail',
    templateUrl: './atrybut-detail.component.html'
})
export class AtrybutDetailComponent implements OnInit {
    atrybut: IAtrybut;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ atrybut }) => {
            this.atrybut = atrybut;
        });
    }

    previousState() {
        window.history.back();
    }
}
