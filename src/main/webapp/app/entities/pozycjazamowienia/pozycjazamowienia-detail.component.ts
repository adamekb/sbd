import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';

@Component({
    selector: 'jhi-pozycjazamowienia-detail',
    templateUrl: './pozycjazamowienia-detail.component.html'
})
export class PozycjazamowieniaDetailComponent implements OnInit {
    pozycjazamowienia: IPozycjazamowienia;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pozycjazamowienia }) => {
            this.pozycjazamowienia = pozycjazamowienia;
        });
    }

    previousState() {
        window.history.back();
    }
}
