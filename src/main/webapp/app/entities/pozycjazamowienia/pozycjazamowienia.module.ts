import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SbdsklepSharedModule } from 'app/shared';
import {
    PozycjazamowieniaComponent,
    PozycjazamowieniaDetailComponent,
    PozycjazamowieniaUpdateComponent,
    PozycjazamowieniaDeletePopupComponent,
    PozycjazamowieniaDeleteDialogComponent,
    pozycjazamowieniaRoute,
    pozycjazamowieniaPopupRoute
} from './';

const ENTITY_STATES = [...pozycjazamowieniaRoute, ...pozycjazamowieniaPopupRoute];

@NgModule({
    imports: [SbdsklepSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PozycjazamowieniaComponent,
        PozycjazamowieniaDetailComponent,
        PozycjazamowieniaUpdateComponent,
        PozycjazamowieniaDeleteDialogComponent,
        PozycjazamowieniaDeletePopupComponent
    ],
    entryComponents: [
        PozycjazamowieniaComponent,
        PozycjazamowieniaUpdateComponent,
        PozycjazamowieniaDeleteDialogComponent,
        PozycjazamowieniaDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SbdsklepPozycjazamowieniaModule {}
