import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';
import { PozycjazamowieniaService } from './pozycjazamowienia.service';

@Component({
    selector: 'jhi-pozycjazamowienia-delete-dialog',
    templateUrl: './pozycjazamowienia-delete-dialog.component.html'
})
export class PozycjazamowieniaDeleteDialogComponent {
    pozycjazamowienia: IPozycjazamowienia;

    constructor(
        protected pozycjazamowieniaService: PozycjazamowieniaService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pozycjazamowieniaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pozycjazamowieniaListModification',
                content: 'Deleted an pozycjazamowienia'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pozycjazamowienia-delete-popup',
    template: ''
})
export class PozycjazamowieniaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pozycjazamowienia }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PozycjazamowieniaDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.pozycjazamowienia = pozycjazamowienia;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
