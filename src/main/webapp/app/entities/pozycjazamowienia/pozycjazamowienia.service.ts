import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';

type EntityResponseType = HttpResponse<IPozycjazamowienia>;
type EntityArrayResponseType = HttpResponse<IPozycjazamowienia[]>;

@Injectable({ providedIn: 'root' })
export class PozycjazamowieniaService {
    public resourceUrl = SERVER_API_URL + 'api/pozycjazamowienias';

    constructor(protected http: HttpClient) {}

    create(pozycjazamowienia: IPozycjazamowienia): Observable<EntityResponseType> {
        return this.http.post<IPozycjazamowienia>(this.resourceUrl, pozycjazamowienia, { observe: 'response' });
    }

    update(pozycjazamowienia: IPozycjazamowienia): Observable<EntityResponseType> {
        return this.http.put<IPozycjazamowienia>(this.resourceUrl, pozycjazamowienia, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPozycjazamowienia>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPozycjazamowienia[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
