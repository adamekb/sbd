import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Pozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';
import { PozycjazamowieniaService } from './pozycjazamowienia.service';
import { PozycjazamowieniaComponent } from './pozycjazamowienia.component';
import { PozycjazamowieniaDetailComponent } from './pozycjazamowienia-detail.component';
import { PozycjazamowieniaUpdateComponent } from './pozycjazamowienia-update.component';
import { PozycjazamowieniaDeletePopupComponent } from './pozycjazamowienia-delete-dialog.component';
import { IPozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';

@Injectable({ providedIn: 'root' })
export class PozycjazamowieniaResolve implements Resolve<IPozycjazamowienia> {
    constructor(private service: PozycjazamowieniaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Pozycjazamowienia> {
        const id = route.params['id'] ? route.params['id'] : null;
        const produkt = route.params['produkt'] ? route.params['produkt'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Pozycjazamowienia>) => response.ok),
                map((pozycjazamowienia: HttpResponse<Pozycjazamowienia>) => pozycjazamowienia.body)
            );
        }
         if (produkt) {
            let x = new Pozycjazamowienia();
            x.produktId = produkt;
            return of(x);                        
        }
       return of(new Pozycjazamowienia());
    }
}

export const pozycjazamowieniaRoute: Routes = [
    {
        path: 'pozycjazamowienia',
        component: PozycjazamowieniaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pozycjazamowienias'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pozycjazamowienia/:id/view',
        component: PozycjazamowieniaDetailComponent,
        resolve: {
            pozycjazamowienia: PozycjazamowieniaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pozycjazamowienias'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pozycjazamowienia/new/:produkt',
        component: PozycjazamowieniaUpdateComponent,
        resolve: {
            pozycjazamowienia: PozycjazamowieniaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pozycjazamowienias'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pozycjazamowienia/:id/edit',
        component: PozycjazamowieniaUpdateComponent,
        resolve: {
            pozycjazamowienia: PozycjazamowieniaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pozycjazamowienias'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pozycjazamowieniaPopupRoute: Routes = [
    {
        path: 'pozycjazamowienia/:id/delete',
        component: PozycjazamowieniaDeletePopupComponent,
        resolve: {
            pozycjazamowienia: PozycjazamowieniaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Pozycjazamowienias'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
