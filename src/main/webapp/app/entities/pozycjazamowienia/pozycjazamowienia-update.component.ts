import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IPozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';
import { PozycjazamowieniaService } from './pozycjazamowienia.service';
import { IZamowienie } from 'app/shared/model/zamowienie.model';
import { ZamowienieService } from 'app/entities/zamowienie';
import { IProdukt } from 'app/shared/model/produkt.model';
import { ProduktService } from 'app/entities/produkt';
import { Zamowienie } from 'app/shared/model/zamowienie.model';
import { IAdres } from 'app/shared/model/adres.model';
import { AdresService } from 'app/entities/adres/adres.service';
import { Adres } from 'app/shared/model/adres.model';

@Component({
    selector: 'jhi-pozycjazamowienia-update',
    templateUrl: './pozycjazamowienia-update.component.html'
})
export class PozycjazamowieniaUpdateComponent implements OnInit {
    pozycjazamowienia: IPozycjazamowienia;
    isSaving: boolean;

    zamowienies: IZamowienie[];

    produkts: IProdukt[];
	
	length: number;
	zamTmp: IZamowienie;
    constructor(
        protected jhiAlertService: JhiAlertService,
        protected pozycjazamowieniaService: PozycjazamowieniaService,
        protected zamowienieService: ZamowienieService,
        protected produktService: ProduktService,
        protected activatedRoute: ActivatedRoute
        protected adresService: AdresService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pozycjazamowienia }) => {
            this.pozycjazamowienia = pozycjazamowienia;
        });
        this.zamowienieService.query().subscribe(
            (res: HttpResponse<IZamowienie[]>) => {
                this.zamowienies = res.body;
                this.length = res.body.length;
                console.log('LLENGHT: ' + this.length);
               
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.produktService.query().subscribe(
            (res: HttpResponse<IProdukt[]>) => {
                this.produkts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        
        this.produktService.find(this.pozycjazamowienia.produktId).subscribe(
        	(res: HttpResponse<IProdukt>) => {
        			this.pozycjazamowienia.cenasztuka = res.body.cena;
        			this.pozycjazamowienia.produktId = res.body.id;
        			console.log(this.pozycjazamowienia);
        		}
        	);
        console.log('XDD' + this.pozycjazamowienia.id);
        
        
      /**  console.log('HEH' + this.zamowienieService.create(this.zamTmp).response.body);
       * this.zamowienieService.update(this.zamTmp);
       * this.pozycjazamowienia.zamowienieId = x.id;
       * console.log('pozycja zamowienia.zamowienie = ' + this.pozycjazamowienia.zamowienieId);
        */
       
        
        let ax;
        adres: IAdres;
        this.activatedRoute.data.subscribe(({ adres }) => {
            this.adres = adres;
        });
        this.adres = new Adres();
        this.adresService.create(this.adres).subscribe(
        	(res: HttpResponse<IAdres>) => {
        		this.ax = res.body.id;
        		console.log('NOO' + res.body.id);
        		this.adres = res.body;
        	}
        );
        console.log('ADRES ' + this.ax);
        
        zamowienie: IZamowienie;
        this.activatedRoute.data.subscribe(({ zamowienie }) => {
            this.zamowienie = zamowienie;
        });
        this.zamowienie = new Zamowienie();
      
        
        this.zamowienieService.create(this.zamowienie).subscribe(
        	(res: HttpResponse<IZamowienie>) => {
					this.zamowienie = res.body;
        			console.log('NOO' + res.body.id);
        			this.pozycjazamowienia.zamowienieId = res.body.id;
        			res.body.adresId = this.adres.id;
        			this.zamowienieService.update(res.body).subscribe(
        					(response: HttpResponse<IZamowienie>) => {
					
        			console.log('NOOsz' + response.body);
        			
        		}
        	);	
        			
        		}
        	);

        	
        	
        
        	
       
        	
        	
        	
 
        	
        	
        
        
    }

    previousState() {
    /**
      *  window.history.back();
      window.location = 'http://localhost:9000/#/adres/new'
      */
      console.log('XDD' + this.adres.id);
      window.location = 'http://localhost:9000/#/adres/ '+  this.adres.id +' /edit';
      
    }

    save() {
        this.isSaving = true;
        if (this.pozycjazamowienia.id !== undefined) {
            this.subscribeToSaveResponse(this.pozycjazamowieniaService.update(this.pozycjazamowienia));
        } else {
            this.subscribeToSaveResponse(this.pozycjazamowieniaService.create(this.pozycjazamowienia));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPozycjazamowienia>>) {
        result.subscribe((res: HttpResponse<IPozycjazamowienia>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackZamowienieById(index: number, item: IZamowienie) {
        return item.id;
    }

    trackProduktById(index: number, item: IProdukt) {
        return item.id;
    }
}
