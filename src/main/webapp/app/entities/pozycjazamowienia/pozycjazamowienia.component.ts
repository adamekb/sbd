import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';
import { AccountService } from 'app/core';
import { PozycjazamowieniaService } from './pozycjazamowienia.service';

@Component({
    selector: 'jhi-pozycjazamowienia',
    templateUrl: './pozycjazamowienia.component.html'
})
export class PozycjazamowieniaComponent implements OnInit, OnDestroy {
    pozycjazamowienias: IPozycjazamowienia[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected pozycjazamowieniaService: PozycjazamowieniaService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.pozycjazamowieniaService.query().subscribe(
            (res: HttpResponse<IPozycjazamowienia[]>) => {
                this.pozycjazamowienias = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPozycjazamowienias();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPozycjazamowienia) {
        return item.id;
    }

    registerChangeInPozycjazamowienias() {
        this.eventSubscriber = this.eventManager.subscribe('pozycjazamowieniaListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
