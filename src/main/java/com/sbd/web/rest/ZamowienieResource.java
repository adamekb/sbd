package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.ZamowienieService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.ZamowienieDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Zamowienie.
 */
@RestController
@RequestMapping("/api")
public class ZamowienieResource {

    private final Logger log = LoggerFactory.getLogger(ZamowienieResource.class);

    private static final String ENTITY_NAME = "zamowienie";

    private final ZamowienieService zamowienieService;

    public ZamowienieResource(ZamowienieService zamowienieService) {
        this.zamowienieService = zamowienieService;
    }

    /**
     * POST  /zamowienies : Create a new zamowienie.
     *
     * @param zamowienieDTO the zamowienieDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new zamowienieDTO, or with status 400 (Bad Request) if the zamowienie has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/zamowienies")
    @Timed
    public ResponseEntity<ZamowienieDTO> createZamowienie(@RequestBody ZamowienieDTO zamowienieDTO) throws URISyntaxException {
        log.debug("REST request to save Zamowienie : {}", zamowienieDTO);
        if (zamowienieDTO.getId() != null) {
            throw new BadRequestAlertException("A new zamowienie cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ZamowienieDTO result = zamowienieService.save(zamowienieDTO);
        return ResponseEntity.created(new URI("/api/zamowienies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /zamowienies : Updates an existing zamowienie.
     *
     * @param zamowienieDTO the zamowienieDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated zamowienieDTO,
     * or with status 400 (Bad Request) if the zamowienieDTO is not valid,
     * or with status 500 (Internal Server Error) if the zamowienieDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/zamowienies")
    @Timed
    public ResponseEntity<ZamowienieDTO> updateZamowienie(@RequestBody ZamowienieDTO zamowienieDTO) throws URISyntaxException {
        log.debug("REST request to update Zamowienie : {}", zamowienieDTO);
        if (zamowienieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ZamowienieDTO result = zamowienieService.save(zamowienieDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, zamowienieDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /zamowienies : get all the zamowienies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of zamowienies in body
     */
    @GetMapping("/zamowienies")
    @Timed
    public List<ZamowienieDTO> getAllZamowienies() {
        log.debug("REST request to get all Zamowienies");
        return zamowienieService.findAll();
    }

    /**
     * GET  /zamowienies/:id : get the "id" zamowienie.
     *
     * @param id the id of the zamowienieDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the zamowienieDTO, or with status 404 (Not Found)
     */
    @GetMapping("/zamowienies/{id}")
    @Timed
    public ResponseEntity<ZamowienieDTO> getZamowienie(@PathVariable Long id) {
        log.debug("REST request to get Zamowienie : {}", id);
        Optional<ZamowienieDTO> zamowienieDTO = zamowienieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(zamowienieDTO);
    }

    /**
     * DELETE  /zamowienies/:id : delete the "id" zamowienie.
     *
     * @param id the id of the zamowienieDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/zamowienies/{id}")
    @Timed
    public ResponseEntity<Void> deleteZamowienie(@PathVariable Long id) {
        log.debug("REST request to delete Zamowienie : {}", id);
        zamowienieService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
