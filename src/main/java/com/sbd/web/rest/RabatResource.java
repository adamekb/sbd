package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.RabatService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.RabatDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Rabat.
 */
@RestController
@RequestMapping("/api")
public class RabatResource {

    private final Logger log = LoggerFactory.getLogger(RabatResource.class);

    private static final String ENTITY_NAME = "rabat";

    private final RabatService rabatService;

    public RabatResource(RabatService rabatService) {
        this.rabatService = rabatService;
    }

    /**
     * POST  /rabats : Create a new rabat.
     *
     * @param rabatDTO the rabatDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rabatDTO, or with status 400 (Bad Request) if the rabat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rabats")
    @Timed
    public ResponseEntity<RabatDTO> createRabat(@RequestBody RabatDTO rabatDTO) throws URISyntaxException {
        log.debug("REST request to save Rabat : {}", rabatDTO);
        if (rabatDTO.getId() != null) {
            throw new BadRequestAlertException("A new rabat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RabatDTO result = rabatService.save(rabatDTO);
        return ResponseEntity.created(new URI("/api/rabats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rabats : Updates an existing rabat.
     *
     * @param rabatDTO the rabatDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rabatDTO,
     * or with status 400 (Bad Request) if the rabatDTO is not valid,
     * or with status 500 (Internal Server Error) if the rabatDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rabats")
    @Timed
    public ResponseEntity<RabatDTO> updateRabat(@RequestBody RabatDTO rabatDTO) throws URISyntaxException {
        log.debug("REST request to update Rabat : {}", rabatDTO);
        if (rabatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RabatDTO result = rabatService.save(rabatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rabatDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rabats : get all the rabats.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of rabats in body
     */
    @GetMapping("/rabats")
    @Timed
    public List<RabatDTO> getAllRabats(@RequestParam(required = false) String filter) {
        if ("zamowienie-is-null".equals(filter)) {
            log.debug("REST request to get all Rabats where zamowienie is null");
            return rabatService.findAllWhereZamowienieIsNull();
        }
        log.debug("REST request to get all Rabats");
        return rabatService.findAll();
    }

    /**
     * GET  /rabats/:id : get the "id" rabat.
     *
     * @param id the id of the rabatDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rabatDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rabats/{id}")
    @Timed
    public ResponseEntity<RabatDTO> getRabat(@PathVariable Long id) {
        log.debug("REST request to get Rabat : {}", id);
        Optional<RabatDTO> rabatDTO = rabatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rabatDTO);
    }

    /**
     * DELETE  /rabats/:id : delete the "id" rabat.
     *
     * @param id the id of the rabatDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rabats/{id}")
    @Timed
    public ResponseEntity<Void> deleteRabat(@PathVariable Long id) {
        log.debug("REST request to delete Rabat : {}", id);
        rabatService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
