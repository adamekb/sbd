package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.AtrybutService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.AtrybutDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Atrybut.
 */
@RestController
@RequestMapping("/api")
public class AtrybutResource {

    private final Logger log = LoggerFactory.getLogger(AtrybutResource.class);

    private static final String ENTITY_NAME = "atrybut";

    private final AtrybutService atrybutService;

    public AtrybutResource(AtrybutService atrybutService) {
        this.atrybutService = atrybutService;
    }

    /**
     * POST  /atrybuts : Create a new atrybut.
     *
     * @param atrybutDTO the atrybutDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new atrybutDTO, or with status 400 (Bad Request) if the atrybut has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/atrybuts")
    @Timed
    public ResponseEntity<AtrybutDTO> createAtrybut(@RequestBody AtrybutDTO atrybutDTO) throws URISyntaxException {
        log.debug("REST request to save Atrybut : {}", atrybutDTO);
        if (atrybutDTO.getId() != null) {
            throw new BadRequestAlertException("A new atrybut cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AtrybutDTO result = atrybutService.save(atrybutDTO);
        return ResponseEntity.created(new URI("/api/atrybuts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /atrybuts : Updates an existing atrybut.
     *
     * @param atrybutDTO the atrybutDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated atrybutDTO,
     * or with status 400 (Bad Request) if the atrybutDTO is not valid,
     * or with status 500 (Internal Server Error) if the atrybutDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/atrybuts")
    @Timed
    public ResponseEntity<AtrybutDTO> updateAtrybut(@RequestBody AtrybutDTO atrybutDTO) throws URISyntaxException {
        log.debug("REST request to update Atrybut : {}", atrybutDTO);
        if (atrybutDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AtrybutDTO result = atrybutService.save(atrybutDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, atrybutDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /atrybuts : get all the atrybuts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of atrybuts in body
     */
    @GetMapping("/atrybuts")
    @Timed
    public List<AtrybutDTO> getAllAtrybuts() {
        log.debug("REST request to get all Atrybuts");
        return atrybutService.findAll();
    }

    /**
     * GET  /atrybuts/:id : get the "id" atrybut.
     *
     * @param id the id of the atrybutDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the atrybutDTO, or with status 404 (Not Found)
     */
    @GetMapping("/atrybuts/{id}")
    @Timed
    public ResponseEntity<AtrybutDTO> getAtrybut(@PathVariable Long id) {
        log.debug("REST request to get Atrybut : {}", id);
        Optional<AtrybutDTO> atrybutDTO = atrybutService.findOne(id);
        return ResponseUtil.wrapOrNotFound(atrybutDTO);
    }

    /**
     * DELETE  /atrybuts/:id : delete the "id" atrybut.
     *
     * @param id the id of the atrybutDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/atrybuts/{id}")
    @Timed
    public ResponseEntity<Void> deleteAtrybut(@PathVariable Long id) {
        log.debug("REST request to delete Atrybut : {}", id);
        atrybutService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
