package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.ZdjecieService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.ZdjecieDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Zdjecie.
 */
@RestController
@RequestMapping("/api")
public class ZdjecieResource {

    private final Logger log = LoggerFactory.getLogger(ZdjecieResource.class);

    private static final String ENTITY_NAME = "zdjecie";

    private final ZdjecieService zdjecieService;

    public ZdjecieResource(ZdjecieService zdjecieService) {
        this.zdjecieService = zdjecieService;
    }

    /**
     * POST  /zdjecies : Create a new zdjecie.
     *
     * @param zdjecieDTO the zdjecieDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new zdjecieDTO, or with status 400 (Bad Request) if the zdjecie has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/zdjecies")
    @Timed
    public ResponseEntity<ZdjecieDTO> createZdjecie(@RequestBody ZdjecieDTO zdjecieDTO) throws URISyntaxException {
        log.debug("REST request to save Zdjecie : {}", zdjecieDTO);
        if (zdjecieDTO.getId() != null) {
            throw new BadRequestAlertException("A new zdjecie cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ZdjecieDTO result = zdjecieService.save(zdjecieDTO);
        return ResponseEntity.created(new URI("/api/zdjecies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /zdjecies : Updates an existing zdjecie.
     *
     * @param zdjecieDTO the zdjecieDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated zdjecieDTO,
     * or with status 400 (Bad Request) if the zdjecieDTO is not valid,
     * or with status 500 (Internal Server Error) if the zdjecieDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/zdjecies")
    @Timed
    public ResponseEntity<ZdjecieDTO> updateZdjecie(@RequestBody ZdjecieDTO zdjecieDTO) throws URISyntaxException {
        log.debug("REST request to update Zdjecie : {}", zdjecieDTO);
        if (zdjecieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ZdjecieDTO result = zdjecieService.save(zdjecieDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, zdjecieDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /zdjecies : get all the zdjecies.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of zdjecies in body
     */
    @GetMapping("/zdjecies")
    @Timed
    public List<ZdjecieDTO> getAllZdjecies(@RequestParam(required = false) String filter) {
        if ("produkt-is-null".equals(filter)) {
            log.debug("REST request to get all Zdjecies where produkt is null");
            return zdjecieService.findAllWhereProduktIsNull();
        }
        log.debug("REST request to get all Zdjecies");
        return zdjecieService.findAll();
    }

    /**
     * GET  /zdjecies/:id : get the "id" zdjecie.
     *
     * @param id the id of the zdjecieDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the zdjecieDTO, or with status 404 (Not Found)
     */
    @GetMapping("/zdjecies/{id}")
    @Timed
    public ResponseEntity<ZdjecieDTO> getZdjecie(@PathVariable Long id) {
        log.debug("REST request to get Zdjecie : {}", id);
        Optional<ZdjecieDTO> zdjecieDTO = zdjecieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(zdjecieDTO);
    }

    /**
     * DELETE  /zdjecies/:id : delete the "id" zdjecie.
     *
     * @param id the id of the zdjecieDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/zdjecies/{id}")
    @Timed
    public ResponseEntity<Void> deleteZdjecie(@PathVariable Long id) {
        log.debug("REST request to delete Zdjecie : {}", id);
        zdjecieService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
