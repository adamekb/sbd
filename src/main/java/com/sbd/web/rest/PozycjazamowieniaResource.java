package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.PozycjazamowieniaService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.PozycjazamowieniaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Pozycjazamowienia.
 */
@RestController
@RequestMapping("/api")
public class PozycjazamowieniaResource {

    private final Logger log = LoggerFactory.getLogger(PozycjazamowieniaResource.class);

    private static final String ENTITY_NAME = "pozycjazamowienia";

    private final PozycjazamowieniaService pozycjazamowieniaService;

    public PozycjazamowieniaResource(PozycjazamowieniaService pozycjazamowieniaService) {
        this.pozycjazamowieniaService = pozycjazamowieniaService;
    }

    /**
     * POST  /pozycjazamowienias : Create a new pozycjazamowienia.
     *
     * @param pozycjazamowieniaDTO the pozycjazamowieniaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pozycjazamowieniaDTO, or with status 400 (Bad Request) if the pozycjazamowienia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pozycjazamowienias")
    @Timed
    public ResponseEntity<PozycjazamowieniaDTO> createPozycjazamowienia(@RequestBody PozycjazamowieniaDTO pozycjazamowieniaDTO) throws URISyntaxException {
        log.debug("REST request to save Pozycjazamowienia : {}", pozycjazamowieniaDTO);
        if (pozycjazamowieniaDTO.getId() != null) {
            throw new BadRequestAlertException("A new pozycjazamowienia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PozycjazamowieniaDTO result = pozycjazamowieniaService.save(pozycjazamowieniaDTO);
        return ResponseEntity.created(new URI("/api/pozycjazamowienias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pozycjazamowienias : Updates an existing pozycjazamowienia.
     *
     * @param pozycjazamowieniaDTO the pozycjazamowieniaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pozycjazamowieniaDTO,
     * or with status 400 (Bad Request) if the pozycjazamowieniaDTO is not valid,
     * or with status 500 (Internal Server Error) if the pozycjazamowieniaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pozycjazamowienias")
    @Timed
    public ResponseEntity<PozycjazamowieniaDTO> updatePozycjazamowienia(@RequestBody PozycjazamowieniaDTO pozycjazamowieniaDTO) throws URISyntaxException {
        log.debug("REST request to update Pozycjazamowienia : {}", pozycjazamowieniaDTO);
        if (pozycjazamowieniaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PozycjazamowieniaDTO result = pozycjazamowieniaService.save(pozycjazamowieniaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pozycjazamowieniaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pozycjazamowienias : get all the pozycjazamowienias.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pozycjazamowienias in body
     */
    @GetMapping("/pozycjazamowienias")
    @Timed
    public List<PozycjazamowieniaDTO> getAllPozycjazamowienias() {
        log.debug("REST request to get all Pozycjazamowienias");
        return pozycjazamowieniaService.findAll();
    }

    /**
     * GET  /pozycjazamowienias/:id : get the "id" pozycjazamowienia.
     *
     * @param id the id of the pozycjazamowieniaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pozycjazamowieniaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pozycjazamowienias/{id}")
    @Timed
    public ResponseEntity<PozycjazamowieniaDTO> getPozycjazamowienia(@PathVariable Long id) {
        log.debug("REST request to get Pozycjazamowienia : {}", id);
        Optional<PozycjazamowieniaDTO> pozycjazamowieniaDTO = pozycjazamowieniaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pozycjazamowieniaDTO);
    }

    /**
     * DELETE  /pozycjazamowienias/:id : delete the "id" pozycjazamowienia.
     *
     * @param id the id of the pozycjazamowieniaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pozycjazamowienias/{id}")
    @Timed
    public ResponseEntity<Void> deletePozycjazamowienia(@PathVariable Long id) {
        log.debug("REST request to delete Pozycjazamowienia : {}", id);
        pozycjazamowieniaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
