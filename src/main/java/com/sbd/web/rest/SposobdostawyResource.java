package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.SposobdostawyService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.SposobdostawyDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Sposobdostawy.
 */
@RestController
@RequestMapping("/api")
public class SposobdostawyResource {

    private final Logger log = LoggerFactory.getLogger(SposobdostawyResource.class);

    private static final String ENTITY_NAME = "sposobdostawy";

    private final SposobdostawyService sposobdostawyService;

    public SposobdostawyResource(SposobdostawyService sposobdostawyService) {
        this.sposobdostawyService = sposobdostawyService;
    }

    /**
     * POST  /sposobdostawies : Create a new sposobdostawy.
     *
     * @param sposobdostawyDTO the sposobdostawyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sposobdostawyDTO, or with status 400 (Bad Request) if the sposobdostawy has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sposobdostawies")
    @Timed
    public ResponseEntity<SposobdostawyDTO> createSposobdostawy(@RequestBody SposobdostawyDTO sposobdostawyDTO) throws URISyntaxException {
        log.debug("REST request to save Sposobdostawy : {}", sposobdostawyDTO);
        if (sposobdostawyDTO.getId() != null) {
            throw new BadRequestAlertException("A new sposobdostawy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SposobdostawyDTO result = sposobdostawyService.save(sposobdostawyDTO);
        return ResponseEntity.created(new URI("/api/sposobdostawies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sposobdostawies : Updates an existing sposobdostawy.
     *
     * @param sposobdostawyDTO the sposobdostawyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sposobdostawyDTO,
     * or with status 400 (Bad Request) if the sposobdostawyDTO is not valid,
     * or with status 500 (Internal Server Error) if the sposobdostawyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sposobdostawies")
    @Timed
    public ResponseEntity<SposobdostawyDTO> updateSposobdostawy(@RequestBody SposobdostawyDTO sposobdostawyDTO) throws URISyntaxException {
        log.debug("REST request to update Sposobdostawy : {}", sposobdostawyDTO);
        if (sposobdostawyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SposobdostawyDTO result = sposobdostawyService.save(sposobdostawyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sposobdostawyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sposobdostawies : get all the sposobdostawies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sposobdostawies in body
     */
    @GetMapping("/sposobdostawies")
    @Timed
    public List<SposobdostawyDTO> getAllSposobdostawies() {
        log.debug("REST request to get all Sposobdostawies");
        return sposobdostawyService.findAll();
    }

    /**
     * GET  /sposobdostawies/:id : get the "id" sposobdostawy.
     *
     * @param id the id of the sposobdostawyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sposobdostawyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sposobdostawies/{id}")
    @Timed
    public ResponseEntity<SposobdostawyDTO> getSposobdostawy(@PathVariable Long id) {
        log.debug("REST request to get Sposobdostawy : {}", id);
        Optional<SposobdostawyDTO> sposobdostawyDTO = sposobdostawyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sposobdostawyDTO);
    }

    /**
     * DELETE  /sposobdostawies/:id : delete the "id" sposobdostawy.
     *
     * @param id the id of the sposobdostawyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sposobdostawies/{id}")
    @Timed
    public ResponseEntity<Void> deleteSposobdostawy(@PathVariable Long id) {
        log.debug("REST request to delete Sposobdostawy : {}", id);
        sposobdostawyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
