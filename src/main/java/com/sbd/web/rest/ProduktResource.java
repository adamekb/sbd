package com.sbd.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sbd.service.ProduktService;
import com.sbd.web.rest.errors.BadRequestAlertException;
import com.sbd.web.rest.util.HeaderUtil;
import com.sbd.service.dto.ProduktDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Produkt.
 */
@RestController
@RequestMapping("/api")
public class ProduktResource {

    private final Logger log = LoggerFactory.getLogger(ProduktResource.class);

    private static final String ENTITY_NAME = "produkt";

    private final ProduktService produktService;

    public ProduktResource(ProduktService produktService) {
        this.produktService = produktService;
    }

    /**
     * POST  /produkts : Create a new produkt.
     *
     * @param produktDTO the produktDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new produktDTO, or with status 400 (Bad Request) if the produkt has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/produkts")
    @Timed
    public ResponseEntity<ProduktDTO> createProdukt(@RequestBody ProduktDTO produktDTO) throws URISyntaxException {
        log.debug("REST request to save Produkt : {}", produktDTO);
        if (produktDTO.getId() != null) {
            throw new BadRequestAlertException("A new produkt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProduktDTO result = produktService.save(produktDTO);
        return ResponseEntity.created(new URI("/api/produkts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /produkts : Updates an existing produkt.
     *
     * @param produktDTO the produktDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated produktDTO,
     * or with status 400 (Bad Request) if the produktDTO is not valid,
     * or with status 500 (Internal Server Error) if the produktDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/produkts")
    @Timed
    public ResponseEntity<ProduktDTO> updateProdukt(@RequestBody ProduktDTO produktDTO) throws URISyntaxException {
        log.debug("REST request to update Produkt : {}", produktDTO);
        if (produktDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProduktDTO result = produktService.save(produktDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, produktDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /produkts : get all the produkts.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of produkts in body
     */
    @GetMapping("/produkts")
    @Timed
    public List<ProduktDTO> getAllProdukts(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Produkts");
        return produktService.findAll();
    }

    /**
     * GET  /produkts/:id : get the "id" produkt.
     *
     * @param id the id of the produktDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the produktDTO, or with status 404 (Not Found)
     */
    @GetMapping("/produkts/{id}")
    @Timed
    public ResponseEntity<ProduktDTO> getProdukt(@PathVariable Long id) {
        log.debug("REST request to get Produkt : {}", id);
        Optional<ProduktDTO> produktDTO = produktService.findOne(id);
        return ResponseUtil.wrapOrNotFound(produktDTO);
    }

    /**
     * DELETE  /produkts/:id : delete the "id" produkt.
     *
     * @param id the id of the produktDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/produkts/{id}")
    @Timed
    public ResponseEntity<Void> deleteProdukt(@PathVariable Long id) {
        log.debug("REST request to delete Produkt : {}", id);
        produktService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
