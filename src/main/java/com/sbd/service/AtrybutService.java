package com.sbd.service;

import com.sbd.service.dto.AtrybutDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Atrybut.
 */
public interface AtrybutService {

    /**
     * Save a atrybut.
     *
     * @param atrybutDTO the entity to save
     * @return the persisted entity
     */
    AtrybutDTO save(AtrybutDTO atrybutDTO);

    /**
     * Get all the atrybuts.
     *
     * @return the list of entities
     */
    List<AtrybutDTO> findAll();


    /**
     * Get the "id" atrybut.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AtrybutDTO> findOne(Long id);

    /**
     * Delete the "id" atrybut.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
