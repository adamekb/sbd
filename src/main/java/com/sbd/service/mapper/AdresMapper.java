package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.AdresDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Adres and its DTO AdresDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdresMapper extends EntityMapper<AdresDTO, Adres> {


    @Mapping(target = "zamowienie", ignore = true)
    Adres toEntity(AdresDTO adresDTO);

    default Adres fromId(Long id) {
        if (id == null) {
            return null;
        }
        Adres adres = new Adres();
        adres.setId(id);
        return adres;
    }
}
