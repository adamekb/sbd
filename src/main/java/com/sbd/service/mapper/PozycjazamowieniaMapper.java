package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.PozycjazamowieniaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Pozycjazamowienia and its DTO PozycjazamowieniaDTO.
 */
@Mapper(componentModel = "spring", uses = {ZamowienieMapper.class, ProduktMapper.class})
public interface PozycjazamowieniaMapper extends EntityMapper<PozycjazamowieniaDTO, Pozycjazamowienia> {

    @Mapping(source = "zamowienie.id", target = "zamowienieId")
    @Mapping(source = "produkt.id", target = "produktId")
    PozycjazamowieniaDTO toDto(Pozycjazamowienia pozycjazamowienia);

    @Mapping(source = "zamowienieId", target = "zamowienie")
    @Mapping(source = "produktId", target = "produkt")
    Pozycjazamowienia toEntity(PozycjazamowieniaDTO pozycjazamowieniaDTO);

    default Pozycjazamowienia fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pozycjazamowienia pozycjazamowienia = new Pozycjazamowienia();
        pozycjazamowienia.setId(id);
        return pozycjazamowienia;
    }
}
