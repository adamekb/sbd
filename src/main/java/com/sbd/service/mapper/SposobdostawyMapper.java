package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.SposobdostawyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Sposobdostawy and its DTO SposobdostawyDTO.
 */
@Mapper(componentModel = "spring", uses = {ZamowienieMapper.class})
public interface SposobdostawyMapper extends EntityMapper<SposobdostawyDTO, Sposobdostawy> {

    @Mapping(source = "zamowienie.id", target = "zamowienieId")
    SposobdostawyDTO toDto(Sposobdostawy sposobdostawy);

    @Mapping(source = "zamowienieId", target = "zamowienie")
    Sposobdostawy toEntity(SposobdostawyDTO sposobdostawyDTO);

    default Sposobdostawy fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sposobdostawy sposobdostawy = new Sposobdostawy();
        sposobdostawy.setId(id);
        return sposobdostawy;
    }
}
