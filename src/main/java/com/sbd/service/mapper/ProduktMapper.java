package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.ProduktDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Produkt and its DTO ProduktDTO.
 */
@Mapper(componentModel = "spring", uses = {ZdjecieMapper.class, AtrybutMapper.class})
public interface ProduktMapper extends EntityMapper<ProduktDTO, Produkt> {

    @Mapping(source = "zdjecie.id", target = "zdjecieId")
    ProduktDTO toDto(Produkt produkt);

    @Mapping(source = "zdjecieId", target = "zdjecie")
    @Mapping(target = "pozycjazamowienias", ignore = true)
    Produkt toEntity(ProduktDTO produktDTO);

    default Produkt fromId(Long id) {
        if (id == null) {
            return null;
        }
        Produkt produkt = new Produkt();
        produkt.setId(id);
        return produkt;
    }
}
