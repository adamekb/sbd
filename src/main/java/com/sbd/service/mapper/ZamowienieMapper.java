package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.ZamowienieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Zamowienie and its DTO ZamowienieDTO.
 */
@Mapper(componentModel = "spring", uses = {RabatMapper.class, AdresMapper.class, UserMapper.class, StatusMapper.class})
public interface ZamowienieMapper extends EntityMapper<ZamowienieDTO, Zamowienie> {

    @Mapping(source = "rabat.id", target = "rabatId")
    @Mapping(source = "adres.id", target = "adresId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "status.id", target = "statusId")
    ZamowienieDTO toDto(Zamowienie zamowienie);

    @Mapping(source = "rabatId", target = "rabat")
    @Mapping(source = "adresId", target = "adres")
    @Mapping(target = "pozycjazamowienias", ignore = true)
    @Mapping(target = "sposobdostawies", ignore = true)
    @Mapping(source = "userId", target = "user")
    @Mapping(source = "statusId", target = "status")
    Zamowienie toEntity(ZamowienieDTO zamowienieDTO);

    default Zamowienie fromId(Long id) {
        if (id == null) {
            return null;
        }
        Zamowienie zamowienie = new Zamowienie();
        zamowienie.setId(id);
        return zamowienie;
    }
}
