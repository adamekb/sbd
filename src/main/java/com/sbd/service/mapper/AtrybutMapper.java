package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.AtrybutDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Atrybut and its DTO AtrybutDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AtrybutMapper extends EntityMapper<AtrybutDTO, Atrybut> {


    @Mapping(target = "produkts", ignore = true)
    Atrybut toEntity(AtrybutDTO atrybutDTO);

    default Atrybut fromId(Long id) {
        if (id == null) {
            return null;
        }
        Atrybut atrybut = new Atrybut();
        atrybut.setId(id);
        return atrybut;
    }
}
