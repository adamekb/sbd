package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.RabatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Rabat and its DTO RabatDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RabatMapper extends EntityMapper<RabatDTO, Rabat> {


    @Mapping(target = "zamowienie", ignore = true)
    Rabat toEntity(RabatDTO rabatDTO);

    default Rabat fromId(Long id) {
        if (id == null) {
            return null;
        }
        Rabat rabat = new Rabat();
        rabat.setId(id);
        return rabat;
    }
}
