package com.sbd.service.mapper;

import com.sbd.domain.*;
import com.sbd.service.dto.ZdjecieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Zdjecie and its DTO ZdjecieDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ZdjecieMapper extends EntityMapper<ZdjecieDTO, Zdjecie> {


    @Mapping(target = "produkt", ignore = true)
    Zdjecie toEntity(ZdjecieDTO zdjecieDTO);

    default Zdjecie fromId(Long id) {
        if (id == null) {
            return null;
        }
        Zdjecie zdjecie = new Zdjecie();
        zdjecie.setId(id);
        return zdjecie;
    }
}
