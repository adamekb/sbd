package com.sbd.service.impl;

import com.sbd.service.PozycjazamowieniaService;
import com.sbd.domain.Pozycjazamowienia;
import com.sbd.repository.PozycjazamowieniaRepository;
import com.sbd.service.dto.PozycjazamowieniaDTO;
import com.sbd.service.mapper.PozycjazamowieniaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Pozycjazamowienia.
 */
@Service
@Transactional
public class PozycjazamowieniaServiceImpl implements PozycjazamowieniaService {

    private final Logger log = LoggerFactory.getLogger(PozycjazamowieniaServiceImpl.class);

    private final PozycjazamowieniaRepository pozycjazamowieniaRepository;

    private final PozycjazamowieniaMapper pozycjazamowieniaMapper;

    public PozycjazamowieniaServiceImpl(PozycjazamowieniaRepository pozycjazamowieniaRepository, PozycjazamowieniaMapper pozycjazamowieniaMapper) {
        this.pozycjazamowieniaRepository = pozycjazamowieniaRepository;
        this.pozycjazamowieniaMapper = pozycjazamowieniaMapper;
    }

    /**
     * Save a pozycjazamowienia.
     *
     * @param pozycjazamowieniaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PozycjazamowieniaDTO save(PozycjazamowieniaDTO pozycjazamowieniaDTO) {
        log.debug("Request to save Pozycjazamowienia : {}", pozycjazamowieniaDTO);

        Pozycjazamowienia pozycjazamowienia = pozycjazamowieniaMapper.toEntity(pozycjazamowieniaDTO);
        pozycjazamowienia = pozycjazamowieniaRepository.save(pozycjazamowienia);
        return pozycjazamowieniaMapper.toDto(pozycjazamowienia);
    }

    /**
     * Get all the pozycjazamowienias.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PozycjazamowieniaDTO> findAll() {
        log.debug("Request to get all Pozycjazamowienias");
        return pozycjazamowieniaRepository.findAll().stream()
            .map(pozycjazamowieniaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one pozycjazamowienia by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PozycjazamowieniaDTO> findOne(Long id) {
        log.debug("Request to get Pozycjazamowienia : {}", id);
        return pozycjazamowieniaRepository.findById(id)
            .map(pozycjazamowieniaMapper::toDto);
    }

    /**
     * Delete the pozycjazamowienia by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pozycjazamowienia : {}", id);
        pozycjazamowieniaRepository.deleteById(id);
    }
}
