package com.sbd.service.impl;

import com.sbd.service.ZdjecieService;
import com.sbd.domain.Zdjecie;
import com.sbd.repository.ZdjecieRepository;
import com.sbd.service.dto.ZdjecieDTO;
import com.sbd.service.mapper.ZdjecieMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Zdjecie.
 */
@Service
@Transactional
public class ZdjecieServiceImpl implements ZdjecieService {

    private final Logger log = LoggerFactory.getLogger(ZdjecieServiceImpl.class);

    private final ZdjecieRepository zdjecieRepository;

    private final ZdjecieMapper zdjecieMapper;

    public ZdjecieServiceImpl(ZdjecieRepository zdjecieRepository, ZdjecieMapper zdjecieMapper) {
        this.zdjecieRepository = zdjecieRepository;
        this.zdjecieMapper = zdjecieMapper;
    }

    /**
     * Save a zdjecie.
     *
     * @param zdjecieDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ZdjecieDTO save(ZdjecieDTO zdjecieDTO) {
        log.debug("Request to save Zdjecie : {}", zdjecieDTO);

        Zdjecie zdjecie = zdjecieMapper.toEntity(zdjecieDTO);
        zdjecie = zdjecieRepository.save(zdjecie);
        return zdjecieMapper.toDto(zdjecie);
    }

    /**
     * Get all the zdjecies.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ZdjecieDTO> findAll() {
        log.debug("Request to get all Zdjecies");
        return zdjecieRepository.findAll().stream()
            .map(zdjecieMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  get all the zdjecies where Produkt is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ZdjecieDTO> findAllWhereProduktIsNull() {
        log.debug("Request to get all zdjecies where Produkt is null");
        return StreamSupport
            .stream(zdjecieRepository.findAll().spliterator(), false)
            .filter(zdjecie -> zdjecie.getProdukt() == null)
            .map(zdjecieMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one zdjecie by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ZdjecieDTO> findOne(Long id) {
        log.debug("Request to get Zdjecie : {}", id);
        return zdjecieRepository.findById(id)
            .map(zdjecieMapper::toDto);
    }

    /**
     * Delete the zdjecie by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Zdjecie : {}", id);
        zdjecieRepository.deleteById(id);
    }
}
