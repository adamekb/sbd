package com.sbd.service.impl;

import com.sbd.service.RabatService;
import com.sbd.domain.Rabat;
import com.sbd.repository.RabatRepository;
import com.sbd.service.dto.RabatDTO;
import com.sbd.service.mapper.RabatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Rabat.
 */
@Service
@Transactional
public class RabatServiceImpl implements RabatService {

    private final Logger log = LoggerFactory.getLogger(RabatServiceImpl.class);

    private final RabatRepository rabatRepository;

    private final RabatMapper rabatMapper;

    public RabatServiceImpl(RabatRepository rabatRepository, RabatMapper rabatMapper) {
        this.rabatRepository = rabatRepository;
        this.rabatMapper = rabatMapper;
    }

    /**
     * Save a rabat.
     *
     * @param rabatDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RabatDTO save(RabatDTO rabatDTO) {
        log.debug("Request to save Rabat : {}", rabatDTO);

        Rabat rabat = rabatMapper.toEntity(rabatDTO);
        rabat = rabatRepository.save(rabat);
        return rabatMapper.toDto(rabat);
    }

    /**
     * Get all the rabats.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RabatDTO> findAll() {
        log.debug("Request to get all Rabats");
        return rabatRepository.findAll().stream()
            .map(rabatMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  get all the rabats where Zamowienie is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<RabatDTO> findAllWhereZamowienieIsNull() {
        log.debug("Request to get all rabats where Zamowienie is null");
        return StreamSupport
            .stream(rabatRepository.findAll().spliterator(), false)
            .filter(rabat -> rabat.getZamowienie() == null)
            .map(rabatMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one rabat by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RabatDTO> findOne(Long id) {
        log.debug("Request to get Rabat : {}", id);
        return rabatRepository.findById(id)
            .map(rabatMapper::toDto);
    }

    /**
     * Delete the rabat by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Rabat : {}", id);
        rabatRepository.deleteById(id);
    }
}
