package com.sbd.service.impl;

import com.sbd.service.ProduktService;
import com.sbd.domain.Produkt;
import com.sbd.repository.ProduktRepository;
import com.sbd.service.dto.ProduktDTO;
import com.sbd.service.mapper.ProduktMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Produkt.
 */
@Service
@Transactional
public class ProduktServiceImpl implements ProduktService {

    private final Logger log = LoggerFactory.getLogger(ProduktServiceImpl.class);

    private final ProduktRepository produktRepository;

    private final ProduktMapper produktMapper;

    public ProduktServiceImpl(ProduktRepository produktRepository, ProduktMapper produktMapper) {
        this.produktRepository = produktRepository;
        this.produktMapper = produktMapper;
    }

    /**
     * Save a produkt.
     *
     * @param produktDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProduktDTO save(ProduktDTO produktDTO) {
        log.debug("Request to save Produkt : {}", produktDTO);

        Produkt produkt = produktMapper.toEntity(produktDTO);
        produkt = produktRepository.save(produkt);
        return produktMapper.toDto(produkt);
    }

    /**
     * Get all the produkts.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProduktDTO> findAll() {
        log.debug("Request to get all Produkts");
        return produktRepository.findAllWithEagerRelationships().stream()
            .map(produktMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the Produkt with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<ProduktDTO> findAllWithEagerRelationships(Pageable pageable) {
        return produktRepository.findAllWithEagerRelationships(pageable).map(produktMapper::toDto);
    }
    

    /**
     * Get one produkt by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProduktDTO> findOne(Long id) {
        log.debug("Request to get Produkt : {}", id);
        return produktRepository.findOneWithEagerRelationships(id)
            .map(produktMapper::toDto);
    }

    /**
     * Delete the produkt by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Produkt : {}", id);
        produktRepository.deleteById(id);
    }
}
