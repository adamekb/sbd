package com.sbd.service.impl;

import com.sbd.service.SposobdostawyService;
import com.sbd.domain.Sposobdostawy;
import com.sbd.repository.SposobdostawyRepository;
import com.sbd.service.dto.SposobdostawyDTO;
import com.sbd.service.mapper.SposobdostawyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Sposobdostawy.
 */
@Service
@Transactional
public class SposobdostawyServiceImpl implements SposobdostawyService {

    private final Logger log = LoggerFactory.getLogger(SposobdostawyServiceImpl.class);

    private final SposobdostawyRepository sposobdostawyRepository;

    private final SposobdostawyMapper sposobdostawyMapper;

    public SposobdostawyServiceImpl(SposobdostawyRepository sposobdostawyRepository, SposobdostawyMapper sposobdostawyMapper) {
        this.sposobdostawyRepository = sposobdostawyRepository;
        this.sposobdostawyMapper = sposobdostawyMapper;
    }

    /**
     * Save a sposobdostawy.
     *
     * @param sposobdostawyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SposobdostawyDTO save(SposobdostawyDTO sposobdostawyDTO) {
        log.debug("Request to save Sposobdostawy : {}", sposobdostawyDTO);

        Sposobdostawy sposobdostawy = sposobdostawyMapper.toEntity(sposobdostawyDTO);
        sposobdostawy = sposobdostawyRepository.save(sposobdostawy);
        return sposobdostawyMapper.toDto(sposobdostawy);
    }

    /**
     * Get all the sposobdostawies.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SposobdostawyDTO> findAll() {
        log.debug("Request to get all Sposobdostawies");
        return sposobdostawyRepository.findAll().stream()
            .map(sposobdostawyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one sposobdostawy by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SposobdostawyDTO> findOne(Long id) {
        log.debug("Request to get Sposobdostawy : {}", id);
        return sposobdostawyRepository.findById(id)
            .map(sposobdostawyMapper::toDto);
    }

    /**
     * Delete the sposobdostawy by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sposobdostawy : {}", id);
        sposobdostawyRepository.deleteById(id);
    }
}
