package com.sbd.service.impl;

import com.sbd.service.AtrybutService;
import com.sbd.domain.Atrybut;
import com.sbd.repository.AtrybutRepository;
import com.sbd.service.dto.AtrybutDTO;
import com.sbd.service.mapper.AtrybutMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Atrybut.
 */
@Service
@Transactional
public class AtrybutServiceImpl implements AtrybutService {

    private final Logger log = LoggerFactory.getLogger(AtrybutServiceImpl.class);

    private final AtrybutRepository atrybutRepository;

    private final AtrybutMapper atrybutMapper;

    public AtrybutServiceImpl(AtrybutRepository atrybutRepository, AtrybutMapper atrybutMapper) {
        this.atrybutRepository = atrybutRepository;
        this.atrybutMapper = atrybutMapper;
    }

    /**
     * Save a atrybut.
     *
     * @param atrybutDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AtrybutDTO save(AtrybutDTO atrybutDTO) {
        log.debug("Request to save Atrybut : {}", atrybutDTO);

        Atrybut atrybut = atrybutMapper.toEntity(atrybutDTO);
        atrybut = atrybutRepository.save(atrybut);
        return atrybutMapper.toDto(atrybut);
    }

    /**
     * Get all the atrybuts.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AtrybutDTO> findAll() {
        log.debug("Request to get all Atrybuts");
        return atrybutRepository.findAll().stream()
            .map(atrybutMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one atrybut by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AtrybutDTO> findOne(Long id) {
        log.debug("Request to get Atrybut : {}", id);
        return atrybutRepository.findById(id)
            .map(atrybutMapper::toDto);
    }

    /**
     * Delete the atrybut by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Atrybut : {}", id);
        atrybutRepository.deleteById(id);
    }
}
