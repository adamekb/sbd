package com.sbd.service.impl;

import com.sbd.service.AdresService;
import com.sbd.domain.Adres;
import com.sbd.repository.AdresRepository;
import com.sbd.service.dto.AdresDTO;
import com.sbd.service.mapper.AdresMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Adres.
 */
@Service
@Transactional
public class AdresServiceImpl implements AdresService {

    private final Logger log = LoggerFactory.getLogger(AdresServiceImpl.class);

    private final AdresRepository adresRepository;

    private final AdresMapper adresMapper;

    public AdresServiceImpl(AdresRepository adresRepository, AdresMapper adresMapper) {
        this.adresRepository = adresRepository;
        this.adresMapper = adresMapper;
    }

    /**
     * Save a adres.
     *
     * @param adresDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AdresDTO save(AdresDTO adresDTO) {
        log.debug("Request to save Adres : {}", adresDTO);

        Adres adres = adresMapper.toEntity(adresDTO);
        adres = adresRepository.save(adres);
        return adresMapper.toDto(adres);
    }

    /**
     * Get all the adres.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AdresDTO> findAll() {
        log.debug("Request to get all Adres");
        return adresRepository.findAll().stream()
            .map(adresMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  get all the adres where Zamowienie is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AdresDTO> findAllWhereZamowienieIsNull() {
        log.debug("Request to get all adres where Zamowienie is null");
        return StreamSupport
            .stream(adresRepository.findAll().spliterator(), false)
            .filter(adres -> adres.getZamowienie() == null)
            .map(adresMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one adres by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AdresDTO> findOne(Long id) {
        log.debug("Request to get Adres : {}", id);
        return adresRepository.findById(id)
            .map(adresMapper::toDto);
    }

    /**
     * Delete the adres by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Adres : {}", id);
        adresRepository.deleteById(id);
    }
}
