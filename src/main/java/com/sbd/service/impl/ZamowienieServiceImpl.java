package com.sbd.service.impl;

import com.sbd.service.ZamowienieService;
import com.sbd.domain.Zamowienie;
import com.sbd.repository.ZamowienieRepository;
import com.sbd.service.dto.ZamowienieDTO;
import com.sbd.service.mapper.ZamowienieMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Zamowienie.
 */
@Service
@Transactional
public class ZamowienieServiceImpl implements ZamowienieService {

    private final Logger log = LoggerFactory.getLogger(ZamowienieServiceImpl.class);

    private final ZamowienieRepository zamowienieRepository;

    private final ZamowienieMapper zamowienieMapper;

    public ZamowienieServiceImpl(ZamowienieRepository zamowienieRepository, ZamowienieMapper zamowienieMapper) {
        this.zamowienieRepository = zamowienieRepository;
        this.zamowienieMapper = zamowienieMapper;
    }

    /**
     * Save a zamowienie.
     *
     * @param zamowienieDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ZamowienieDTO save(ZamowienieDTO zamowienieDTO) {
        log.debug("Request to save Zamowienie : {}", zamowienieDTO);

        Zamowienie zamowienie = zamowienieMapper.toEntity(zamowienieDTO);
        zamowienie = zamowienieRepository.save(zamowienie);
        return zamowienieMapper.toDto(zamowienie);
    }

    /**
     * Get all the zamowienies.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ZamowienieDTO> findAll() {
        log.debug("Request to get all Zamowienies");
        return zamowienieRepository.findAll().stream()
            .map(zamowienieMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one zamowienie by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ZamowienieDTO> findOne(Long id) {
        log.debug("Request to get Zamowienie : {}", id);
        return zamowienieRepository.findById(id)
            .map(zamowienieMapper::toDto);
    }

    /**
     * Delete the zamowienie by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Zamowienie : {}", id);
        zamowienieRepository.deleteById(id);
    }
}
