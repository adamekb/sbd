package com.sbd.service;

import com.sbd.service.dto.ProduktDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Produkt.
 */
public interface ProduktService {

    /**
     * Save a produkt.
     *
     * @param produktDTO the entity to save
     * @return the persisted entity
     */
    ProduktDTO save(ProduktDTO produktDTO);

    /**
     * Get all the produkts.
     *
     * @return the list of entities
     */
    List<ProduktDTO> findAll();

    /**
     * Get all the Produkt with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<ProduktDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" produkt.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProduktDTO> findOne(Long id);

    /**
     * Delete the "id" produkt.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
