package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Atrybut entity.
 */
public class AtrybutDTO implements Serializable {

    private Long id;

    private String nazwa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AtrybutDTO atrybutDTO = (AtrybutDTO) o;
        if (atrybutDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), atrybutDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AtrybutDTO{" +
            "id=" + getId() +
            ", nazwa='" + getNazwa() + "'" +
            "}";
    }
}
