package com.sbd.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Produkt entity.
 */
public class ProduktDTO implements Serializable {

    private Long id;

    private String nazwa;

    private Float cena;

    private Integer ilosc;

    private Long zdjecieId;

    private Set<AtrybutDTO> atrybuts = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Float getCena() {
        return cena;
    }

    public void setCena(Float cena) {
        this.cena = cena;
    }

    public Integer getIlosc() {
        return ilosc;
    }

    public void setIlosc(Integer ilosc) {
        this.ilosc = ilosc;
    }

    public Long getZdjecieId() {
        return zdjecieId;
    }

    public void setZdjecieId(Long zdjecieId) {
        this.zdjecieId = zdjecieId;
    }

    public Set<AtrybutDTO> getAtrybuts() {
        return atrybuts;
    }

    public void setAtrybuts(Set<AtrybutDTO> atrybuts) {
        this.atrybuts = atrybuts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProduktDTO produktDTO = (ProduktDTO) o;
        if (produktDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), produktDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProduktDTO{" +
            "id=" + getId() +
            ", nazwa='" + getNazwa() + "'" +
            ", cena=" + getCena() +
            ", ilosc=" + getIlosc() +
            ", zdjecie=" + getZdjecieId() +
            "}";
    }
}
