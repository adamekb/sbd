package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Adres entity.
 */
public class AdresDTO implements Serializable {

    private Long id;

    private String panstwo;

    private String miasto;

    private String ulica;

    private Integer numerdomu;

    private Integer numermieszkania;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPanstwo() {
        return panstwo;
    }

    public void setPanstwo(String panstwo) {
        this.panstwo = panstwo;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public Integer getNumerdomu() {
        return numerdomu;
    }

    public void setNumerdomu(Integer numerdomu) {
        this.numerdomu = numerdomu;
    }

    public Integer getNumermieszkania() {
        return numermieszkania;
    }

    public void setNumermieszkania(Integer numermieszkania) {
        this.numermieszkania = numermieszkania;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdresDTO adresDTO = (AdresDTO) o;
        if (adresDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), adresDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdresDTO{" +
            "id=" + getId() +
            ", panstwo='" + getPanstwo() + "'" +
            ", miasto='" + getMiasto() + "'" +
            ", ulica='" + getUlica() + "'" +
            ", numerdomu=" + getNumerdomu() +
            ", numermieszkania=" + getNumermieszkania() +
            "}";
    }
}
