package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Pozycjazamowienia entity.
 */
public class PozycjazamowieniaDTO implements Serializable {

    private Long id;

    private Integer pozycja;

    private Float cenasztuka;

    private Integer ilosc;

    private Long zamowienieId;

    private Long produktId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPozycja() {
        return pozycja;
    }

    public void setPozycja(Integer pozycja) {
        this.pozycja = pozycja;
    }

    public Float getCenasztuka() {
        return cenasztuka;
    }

    public void setCenasztuka(Float cenasztuka) {
        this.cenasztuka = cenasztuka;
    }

    public Integer getIlosc() {
        return ilosc;
    }

    public void setIlosc(Integer ilosc) {
        this.ilosc = ilosc;
    }

    public Long getZamowienieId() {
        return zamowienieId;
    }

    public void setZamowienieId(Long zamowienieId) {
        this.zamowienieId = zamowienieId;
    }

    public Long getProduktId() {
        return produktId;
    }

    public void setProduktId(Long produktId) {
        this.produktId = produktId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PozycjazamowieniaDTO pozycjazamowieniaDTO = (PozycjazamowieniaDTO) o;
        if (pozycjazamowieniaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pozycjazamowieniaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PozycjazamowieniaDTO{" +
            "id=" + getId() +
            ", pozycja=" + getPozycja() +
            ", cenasztuka=" + getCenasztuka() +
            ", ilosc=" + getIlosc() +
            ", zamowienie=" + getZamowienieId() +
            ", produkt=" + getProduktId() +
            "}";
    }
}
