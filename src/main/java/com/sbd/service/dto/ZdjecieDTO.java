package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Zdjecie entity.
 */
public class ZdjecieDTO implements Serializable {

    private Long id;

    private String opis;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ZdjecieDTO zdjecieDTO = (ZdjecieDTO) o;
        if (zdjecieDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zdjecieDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ZdjecieDTO{" +
            "id=" + getId() +
            ", opis='" + getOpis() + "'" +
            "}";
    }
}
