package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Zamowienie entity.
 */
public class ZamowienieDTO implements Serializable {

    private Long id;

    private Float cenacalkowita;

    private Integer iloscproduktow;

    private Long rabatId;

    private Long adresId;

    private Long userId;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getCenacalkowita() {
        return cenacalkowita;
    }

    public void setCenacalkowita(Float cenacalkowita) {
        this.cenacalkowita = cenacalkowita;
    }

    public Integer getIloscproduktow() {
        return iloscproduktow;
    }

    public void setIloscproduktow(Integer iloscproduktow) {
        this.iloscproduktow = iloscproduktow;
    }

    public Long getRabatId() {
        return rabatId;
    }

    public void setRabatId(Long rabatId) {
        this.rabatId = rabatId;
    }

    public Long getAdresId() {
        return adresId;
    }

    public void setAdresId(Long adresId) {
        this.adresId = adresId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ZamowienieDTO zamowienieDTO = (ZamowienieDTO) o;
        if (zamowienieDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zamowienieDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ZamowienieDTO{" +
            "id=" + getId() +
            ", cenacalkowita=" + getCenacalkowita() +
            ", iloscproduktow=" + getIloscproduktow() +
            ", rabat=" + getRabatId() +
            ", adres=" + getAdresId() +
            ", user=" + getUserId() +
            ", status=" + getStatusId() +
            "}";
    }
}
