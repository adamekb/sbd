package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Rabat entity.
 */
public class RabatDTO implements Serializable {

    private Long id;

    private String nazwa;

    private Float wielkosc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Float getWielkosc() {
        return wielkosc;
    }

    public void setWielkosc(Float wielkosc) {
        this.wielkosc = wielkosc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RabatDTO rabatDTO = (RabatDTO) o;
        if (rabatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rabatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RabatDTO{" +
            "id=" + getId() +
            ", nazwa='" + getNazwa() + "'" +
            ", wielkosc=" + getWielkosc() +
            "}";
    }
}
