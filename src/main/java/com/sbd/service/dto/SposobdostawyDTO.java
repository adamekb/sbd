package com.sbd.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Sposobdostawy entity.
 */
public class SposobdostawyDTO implements Serializable {

    private Long id;

    private String rodzaj;

    private Float cena;

    private Long zamowienieId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public Float getCena() {
        return cena;
    }

    public void setCena(Float cena) {
        this.cena = cena;
    }

    public Long getZamowienieId() {
        return zamowienieId;
    }

    public void setZamowienieId(Long zamowienieId) {
        this.zamowienieId = zamowienieId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SposobdostawyDTO sposobdostawyDTO = (SposobdostawyDTO) o;
        if (sposobdostawyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sposobdostawyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SposobdostawyDTO{" +
            "id=" + getId() +
            ", rodzaj='" + getRodzaj() + "'" +
            ", cena=" + getCena() +
            ", zamowienie=" + getZamowienieId() +
            "}";
    }
}
