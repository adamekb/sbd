package com.sbd.service;

import com.sbd.service.dto.ZdjecieDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Zdjecie.
 */
public interface ZdjecieService {

    /**
     * Save a zdjecie.
     *
     * @param zdjecieDTO the entity to save
     * @return the persisted entity
     */
    ZdjecieDTO save(ZdjecieDTO zdjecieDTO);

    /**
     * Get all the zdjecies.
     *
     * @return the list of entities
     */
    List<ZdjecieDTO> findAll();
    /**
     * Get all the ZdjecieDTO where Produkt is null.
     *
     * @return the list of entities
     */
    List<ZdjecieDTO> findAllWhereProduktIsNull();


    /**
     * Get the "id" zdjecie.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ZdjecieDTO> findOne(Long id);

    /**
     * Delete the "id" zdjecie.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
