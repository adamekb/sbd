package com.sbd.service;

import com.sbd.service.dto.RabatDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Rabat.
 */
public interface RabatService {

    /**
     * Save a rabat.
     *
     * @param rabatDTO the entity to save
     * @return the persisted entity
     */
    RabatDTO save(RabatDTO rabatDTO);

    /**
     * Get all the rabats.
     *
     * @return the list of entities
     */
    List<RabatDTO> findAll();
    /**
     * Get all the RabatDTO where Zamowienie is null.
     *
     * @return the list of entities
     */
    List<RabatDTO> findAllWhereZamowienieIsNull();


    /**
     * Get the "id" rabat.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RabatDTO> findOne(Long id);

    /**
     * Delete the "id" rabat.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
