package com.sbd.service;

import com.sbd.service.dto.AdresDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Adres.
 */
public interface AdresService {

    /**
     * Save a adres.
     *
     * @param adresDTO the entity to save
     * @return the persisted entity
     */
    AdresDTO save(AdresDTO adresDTO);

    /**
     * Get all the adres.
     *
     * @return the list of entities
     */
    List<AdresDTO> findAll();
    /**
     * Get all the AdresDTO where Zamowienie is null.
     *
     * @return the list of entities
     */
    List<AdresDTO> findAllWhereZamowienieIsNull();


    /**
     * Get the "id" adres.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AdresDTO> findOne(Long id);

    /**
     * Delete the "id" adres.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
