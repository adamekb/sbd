package com.sbd.service;

import com.sbd.service.dto.PozycjazamowieniaDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Pozycjazamowienia.
 */
public interface PozycjazamowieniaService {

    /**
     * Save a pozycjazamowienia.
     *
     * @param pozycjazamowieniaDTO the entity to save
     * @return the persisted entity
     */
    PozycjazamowieniaDTO save(PozycjazamowieniaDTO pozycjazamowieniaDTO);

    /**
     * Get all the pozycjazamowienias.
     *
     * @return the list of entities
     */
    List<PozycjazamowieniaDTO> findAll();


    /**
     * Get the "id" pozycjazamowienia.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PozycjazamowieniaDTO> findOne(Long id);

    /**
     * Delete the "id" pozycjazamowienia.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
