package com.sbd.service;

import com.sbd.service.dto.SposobdostawyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Sposobdostawy.
 */
public interface SposobdostawyService {

    /**
     * Save a sposobdostawy.
     *
     * @param sposobdostawyDTO the entity to save
     * @return the persisted entity
     */
    SposobdostawyDTO save(SposobdostawyDTO sposobdostawyDTO);

    /**
     * Get all the sposobdostawies.
     *
     * @return the list of entities
     */
    List<SposobdostawyDTO> findAll();


    /**
     * Get the "id" sposobdostawy.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SposobdostawyDTO> findOne(Long id);

    /**
     * Delete the "id" sposobdostawy.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
