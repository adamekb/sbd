package com.sbd.service;

import com.sbd.service.dto.ZamowienieDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Zamowienie.
 */
public interface ZamowienieService {

    /**
     * Save a zamowienie.
     *
     * @param zamowienieDTO the entity to save
     * @return the persisted entity
     */
    ZamowienieDTO save(ZamowienieDTO zamowienieDTO);

    /**
     * Get all the zamowienies.
     *
     * @return the list of entities
     */
    List<ZamowienieDTO> findAll();


    /**
     * Get the "id" zamowienie.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ZamowienieDTO> findOne(Long id);

    /**
     * Delete the "id" zamowienie.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
