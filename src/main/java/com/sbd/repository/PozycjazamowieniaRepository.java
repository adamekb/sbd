package com.sbd.repository;

import com.sbd.domain.Pozycjazamowienia;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Pozycjazamowienia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PozycjazamowieniaRepository extends JpaRepository<Pozycjazamowienia, Long> {

}
