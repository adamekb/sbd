package com.sbd.repository;

import com.sbd.domain.Sposobdostawy;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Sposobdostawy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SposobdostawyRepository extends JpaRepository<Sposobdostawy, Long> {

}
