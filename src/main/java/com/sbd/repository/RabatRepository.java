package com.sbd.repository;

import com.sbd.domain.Rabat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Rabat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RabatRepository extends JpaRepository<Rabat, Long> {

}
