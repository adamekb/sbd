package com.sbd.repository;

import com.sbd.domain.Zdjecie;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Zdjecie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ZdjecieRepository extends JpaRepository<Zdjecie, Long> {

}
