package com.sbd.repository;

import com.sbd.domain.Atrybut;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Atrybut entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtrybutRepository extends JpaRepository<Atrybut, Long> {

}
