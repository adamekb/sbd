package com.sbd.repository;

import com.sbd.domain.Produkt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Produkt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProduktRepository extends JpaRepository<Produkt, Long> {

    @Query(value = "select distinct produkt from Produkt produkt left join fetch produkt.atrybuts",
        countQuery = "select count(distinct produkt) from Produkt produkt")
    Page<Produkt> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct produkt from Produkt produkt left join fetch produkt.atrybuts")
    List<Produkt> findAllWithEagerRelationships();

    @Query("select produkt from Produkt produkt left join fetch produkt.atrybuts where produkt.id =:id")
    Optional<Produkt> findOneWithEagerRelationships(@Param("id") Long id);

}
