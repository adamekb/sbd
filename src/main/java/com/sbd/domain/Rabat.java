package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Rabat.
 */
@Entity
@Table(name = "rabat")
public class Rabat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nazwa")
    private String nazwa;

    @Column(name = "wielkosc")
    private Float wielkosc;

    @OneToOne(mappedBy = "rabat")
    @JsonIgnore
    private Zamowienie zamowienie;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Rabat nazwa(String nazwa) {
        this.nazwa = nazwa;
        return this;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Float getWielkosc() {
        return wielkosc;
    }

    public Rabat wielkosc(Float wielkosc) {
        this.wielkosc = wielkosc;
        return this;
    }

    public void setWielkosc(Float wielkosc) {
        this.wielkosc = wielkosc;
    }

    public Zamowienie getZamowienie() {
        return zamowienie;
    }

    public Rabat zamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
        return this;
    }

    public void setZamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rabat rabat = (Rabat) o;
        if (rabat.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rabat.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Rabat{" +
            "id=" + getId() +
            ", nazwa='" + getNazwa() + "'" +
            ", wielkosc=" + getWielkosc() +
            "}";
    }
}
