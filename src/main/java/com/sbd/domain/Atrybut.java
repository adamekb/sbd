package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Atrybut.
 */
@Entity
@Table(name = "atrybut")
public class Atrybut implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nazwa")
    private String nazwa;

    @ManyToMany(mappedBy = "atrybuts")
    @JsonIgnore
    private Set<Produkt> produkts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Atrybut nazwa(String nazwa) {
        this.nazwa = nazwa;
        return this;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Set<Produkt> getProdukts() {
        return produkts;
    }

    public Atrybut produkts(Set<Produkt> produkts) {
        this.produkts = produkts;
        return this;
    }

    public Atrybut addProdukt(Produkt produkt) {
        this.produkts.add(produkt);
        produkt.getAtrybuts().add(this);
        return this;
    }

    public Atrybut removeProdukt(Produkt produkt) {
        this.produkts.remove(produkt);
        produkt.getAtrybuts().remove(this);
        return this;
    }

    public void setProdukts(Set<Produkt> produkts) {
        this.produkts = produkts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Atrybut atrybut = (Atrybut) o;
        if (atrybut.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), atrybut.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Atrybut{" +
            "id=" + getId() +
            ", nazwa='" + getNazwa() + "'" +
            "}";
    }
}
