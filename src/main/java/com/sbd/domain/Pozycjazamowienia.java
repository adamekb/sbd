package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Pozycjazamowienia.
 */
@Entity
@Table(name = "pozycjazamowienia")
public class Pozycjazamowienia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pozycja")
    private Integer pozycja;

    @Column(name = "cenasztuka")
    private Float cenasztuka;

    @Column(name = "ilosc")
    private Integer ilosc;

    @ManyToOne
    @JsonIgnoreProperties("pozycjazamowienias")
    private Zamowienie zamowienie;

    @ManyToOne
    @JsonIgnoreProperties("pozycjazamowienias")
    private Produkt produkt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPozycja() {
        return pozycja;
    }

    public Pozycjazamowienia pozycja(Integer pozycja) {
        this.pozycja = pozycja;
        return this;
    }

    public void setPozycja(Integer pozycja) {
        this.pozycja = pozycja;
    }

    public Float getCenasztuka() {
        return cenasztuka;
    }

    public Pozycjazamowienia cenasztuka(Float cenasztuka) {
        this.cenasztuka = cenasztuka;
        return this;
    }

    public void setCenasztuka(Float cenasztuka) {
        this.cenasztuka = cenasztuka;
    }

    public Integer getIlosc() {
        return ilosc;
    }

    public Pozycjazamowienia ilosc(Integer ilosc) {
        this.ilosc = ilosc;
        return this;
    }

    public void setIlosc(Integer ilosc) {
        this.ilosc = ilosc;
    }

    public Zamowienie getZamowienie() {
        return zamowienie;
    }

    public Pozycjazamowienia zamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
        return this;
    }

    public void setZamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public Pozycjazamowienia produkt(Produkt produkt) {
        this.produkt = produkt;
        return this;
    }

    public void setProdukt(Produkt produkt) {
        this.produkt = produkt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pozycjazamowienia pozycjazamowienia = (Pozycjazamowienia) o;
        if (pozycjazamowienia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pozycjazamowienia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pozycjazamowienia{" +
            "id=" + getId() +
            ", pozycja=" + getPozycja() +
            ", cenasztuka=" + getCenasztuka() +
            ", ilosc=" + getIlosc() +
            "}";
    }
}
