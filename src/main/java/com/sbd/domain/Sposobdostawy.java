package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Sposobdostawy.
 */
@Entity
@Table(name = "sposobdostawy")
public class Sposobdostawy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "rodzaj")
    private String rodzaj;

    @Column(name = "cena")
    private Float cena;

    @ManyToOne
    @JsonIgnoreProperties("sposobdostawies")
    private Zamowienie zamowienie;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public Sposobdostawy rodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
        return this;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public Float getCena() {
        return cena;
    }

    public Sposobdostawy cena(Float cena) {
        this.cena = cena;
        return this;
    }

    public void setCena(Float cena) {
        this.cena = cena;
    }

    public Zamowienie getZamowienie() {
        return zamowienie;
    }

    public Sposobdostawy zamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
        return this;
    }

    public void setZamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sposobdostawy sposobdostawy = (Sposobdostawy) o;
        if (sposobdostawy.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sposobdostawy.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sposobdostawy{" +
            "id=" + getId() +
            ", rodzaj='" + getRodzaj() + "'" +
            ", cena=" + getCena() +
            "}";
    }
}
