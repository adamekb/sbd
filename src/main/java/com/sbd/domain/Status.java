package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Status.
 */
@Entity
@Table(name = "status")
public class Status implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private String status;

    @OneToMany(mappedBy = "status")
    private Set<Zamowienie> zamowienies = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public Status status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Zamowienie> getZamowienies() {
        return zamowienies;
    }

    public Status zamowienies(Set<Zamowienie> zamowienies) {
        this.zamowienies = zamowienies;
        return this;
    }

    public Status addZamowienie(Zamowienie zamowienie) {
        this.zamowienies.add(zamowienie);
        zamowienie.setStatus(this);
        return this;
    }

    public Status removeZamowienie(Zamowienie zamowienie) {
        this.zamowienies.remove(zamowienie);
        zamowienie.setStatus(null);
        return this;
    }

    public void setZamowienies(Set<Zamowienie> zamowienies) {
        this.zamowienies = zamowienies;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Status status = (Status) o;
        if (status.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), status.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Status{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
