package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * not an ignored comment
 */
@ApiModel(description = "not an ignored comment")
@Entity
@Table(name = "produkt")
public class Produkt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nazwa")
    private String nazwa;

    @Column(name = "cena")
    private Float cena;

    @Column(name = "ilosc")
    private Integer ilosc;

    @OneToOne    @JoinColumn(unique = true)
    private Zdjecie zdjecie;

    @OneToMany(mappedBy = "produkt")
    private Set<Pozycjazamowienia> pozycjazamowienias = new HashSet<>();
    @ManyToMany
    @JoinTable(name = "produkt_atrybut",
               joinColumns = @JoinColumn(name = "produkts_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "atrybuts_id", referencedColumnName = "id"))
    private Set<Atrybut> atrybuts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Produkt nazwa(String nazwa) {
        this.nazwa = nazwa;
        return this;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Float getCena() {
        return cena;
    }

    public Produkt cena(Float cena) {
        this.cena = cena;
        return this;
    }

    public void setCena(Float cena) {
        this.cena = cena;
    }

    public Integer getIlosc() {
        return ilosc;
    }

    public Produkt ilosc(Integer ilosc) {
        this.ilosc = ilosc;
        return this;
    }

    public void setIlosc(Integer ilosc) {
        this.ilosc = ilosc;
    }

    public Zdjecie getZdjecie() {
        return zdjecie;
    }

    public Produkt zdjecie(Zdjecie zdjecie) {
        this.zdjecie = zdjecie;
        return this;
    }

    public void setZdjecie(Zdjecie zdjecie) {
        this.zdjecie = zdjecie;
    }

    public Set<Pozycjazamowienia> getPozycjazamowienias() {
        return pozycjazamowienias;
    }

    public Produkt pozycjazamowienias(Set<Pozycjazamowienia> pozycjazamowienias) {
        this.pozycjazamowienias = pozycjazamowienias;
        return this;
    }

    public Produkt addPozycjazamowienia(Pozycjazamowienia pozycjazamowienia) {
        this.pozycjazamowienias.add(pozycjazamowienia);
        pozycjazamowienia.setProdukt(this);
        return this;
    }

    public Produkt removePozycjazamowienia(Pozycjazamowienia pozycjazamowienia) {
        this.pozycjazamowienias.remove(pozycjazamowienia);
        pozycjazamowienia.setProdukt(null);
        return this;
    }

    public void setPozycjazamowienias(Set<Pozycjazamowienia> pozycjazamowienias) {
        this.pozycjazamowienias = pozycjazamowienias;
    }

    public Set<Atrybut> getAtrybuts() {
        return atrybuts;
    }

    public Produkt atrybuts(Set<Atrybut> atrybuts) {
        this.atrybuts = atrybuts;
        return this;
    }

    public Produkt addAtrybut(Atrybut atrybut) {
        this.atrybuts.add(atrybut);
        atrybut.getProdukts().add(this);
        return this;
    }

    public Produkt removeAtrybut(Atrybut atrybut) {
        this.atrybuts.remove(atrybut);
        atrybut.getProdukts().remove(this);
        return this;
    }

    public void setAtrybuts(Set<Atrybut> atrybuts) {
        this.atrybuts = atrybuts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Produkt produkt = (Produkt) o;
        if (produkt.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), produkt.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Produkt{" +
            "id=" + getId() +
            ", nazwa='" + getNazwa() + "'" +
            ", cena=" + getCena() +
            ", ilosc=" + getIlosc() +
            "}";
    }
}
