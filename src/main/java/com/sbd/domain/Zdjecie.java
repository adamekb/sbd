package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Zdjecie.
 */
@Entity
@Table(name = "zdjecie")
public class Zdjecie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "opis")
    private String opis;

    @OneToOne(mappedBy = "zdjecie")
    @JsonIgnore
    private Produkt produkt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpis() {
        return opis;
    }

    public Zdjecie opis(String opis) {
        this.opis = opis;
        return this;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public Zdjecie produkt(Produkt produkt) {
        this.produkt = produkt;
        return this;
    }

    public void setProdukt(Produkt produkt) {
        this.produkt = produkt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Zdjecie zdjecie = (Zdjecie) o;
        if (zdjecie.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zdjecie.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Zdjecie{" +
            "id=" + getId() +
            ", opis='" + getOpis() + "'" +
            "}";
    }
}
