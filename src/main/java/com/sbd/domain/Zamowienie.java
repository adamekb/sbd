package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Zamowienie.
 */
@Entity
@Table(name = "zamowienie")
public class Zamowienie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cenacalkowita")
    private Float cenacalkowita;

    @Column(name = "iloscproduktow")
    private Integer iloscproduktow;

    @OneToOne    @JoinColumn(unique = true)
    private Rabat rabat;

    @OneToOne    @JoinColumn(unique = true)
    private Adres adres;

    @OneToMany(mappedBy = "zamowienie")
    private Set<Pozycjazamowienia> pozycjazamowienias = new HashSet<>();
    @OneToMany(mappedBy = "zamowienie")
    private Set<Sposobdostawy> sposobdostawies = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("zamowienies")
    private Status status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getCenacalkowita() {
        return cenacalkowita;
    }

    public Zamowienie cenacalkowita(Float cenacalkowita) {
        this.cenacalkowita = cenacalkowita;
        return this;
    }

    public void setCenacalkowita(Float cenacalkowita) {
        this.cenacalkowita = cenacalkowita;
    }

    public Integer getIloscproduktow() {
        return iloscproduktow;
    }

    public Zamowienie iloscproduktow(Integer iloscproduktow) {
        this.iloscproduktow = iloscproduktow;
        return this;
    }

    public void setIloscproduktow(Integer iloscproduktow) {
        this.iloscproduktow = iloscproduktow;
    }

    public Rabat getRabat() {
        return rabat;
    }

    public Zamowienie rabat(Rabat rabat) {
        this.rabat = rabat;
        return this;
    }

    public void setRabat(Rabat rabat) {
        this.rabat = rabat;
    }

    public Adres getAdres() {
        return adres;
    }

    public Zamowienie adres(Adres adres) {
        this.adres = adres;
        return this;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }

    public Set<Pozycjazamowienia> getPozycjazamowienias() {
        return pozycjazamowienias;
    }

    public Zamowienie pozycjazamowienias(Set<Pozycjazamowienia> pozycjazamowienias) {
        this.pozycjazamowienias = pozycjazamowienias;
        return this;
    }

    public Zamowienie addPozycjazamowienia(Pozycjazamowienia pozycjazamowienia) {
        this.pozycjazamowienias.add(pozycjazamowienia);
        pozycjazamowienia.setZamowienie(this);
        return this;
    }

    public Zamowienie removePozycjazamowienia(Pozycjazamowienia pozycjazamowienia) {
        this.pozycjazamowienias.remove(pozycjazamowienia);
        pozycjazamowienia.setZamowienie(null);
        return this;
    }

    public void setPozycjazamowienias(Set<Pozycjazamowienia> pozycjazamowienias) {
        this.pozycjazamowienias = pozycjazamowienias;
    }

    public Set<Sposobdostawy> getSposobdostawies() {
        return sposobdostawies;
    }

    public Zamowienie sposobdostawies(Set<Sposobdostawy> sposobdostawies) {
        this.sposobdostawies = sposobdostawies;
        return this;
    }

    public Zamowienie addSposobdostawy(Sposobdostawy sposobdostawy) {
        this.sposobdostawies.add(sposobdostawy);
        sposobdostawy.setZamowienie(this);
        return this;
    }

    public Zamowienie removeSposobdostawy(Sposobdostawy sposobdostawy) {
        this.sposobdostawies.remove(sposobdostawy);
        sposobdostawy.setZamowienie(null);
        return this;
    }

    public void setSposobdostawies(Set<Sposobdostawy> sposobdostawies) {
        this.sposobdostawies = sposobdostawies;
    }

    public User getUser() {
        return user;
    }

    public Zamowienie user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public Zamowienie status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Zamowienie zamowienie = (Zamowienie) o;
        if (zamowienie.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zamowienie.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Zamowienie{" +
            "id=" + getId() +
            ", cenacalkowita=" + getCenacalkowita() +
            ", iloscproduktow=" + getIloscproduktow() +
            "}";
    }
}
