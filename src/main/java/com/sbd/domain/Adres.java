package com.sbd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Adres.
 */
@Entity
@Table(name = "adres")
public class Adres implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "panstwo")
    private String panstwo;

    @Column(name = "miasto")
    private String miasto;

    @Column(name = "ulica")
    private String ulica;

    @Column(name = "numerdomu")
    private Integer numerdomu;

    @Column(name = "numermieszkania")
    private Integer numermieszkania;

    @OneToOne(mappedBy = "adres")
    @JsonIgnore
    private Zamowienie zamowienie;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPanstwo() {
        return panstwo;
    }

    public Adres panstwo(String panstwo) {
        this.panstwo = panstwo;
        return this;
    }

    public void setPanstwo(String panstwo) {
        this.panstwo = panstwo;
    }

    public String getMiasto() {
        return miasto;
    }

    public Adres miasto(String miasto) {
        this.miasto = miasto;
        return this;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getUlica() {
        return ulica;
    }

    public Adres ulica(String ulica) {
        this.ulica = ulica;
        return this;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public Integer getNumerdomu() {
        return numerdomu;
    }

    public Adres numerdomu(Integer numerdomu) {
        this.numerdomu = numerdomu;
        return this;
    }

    public void setNumerdomu(Integer numerdomu) {
        this.numerdomu = numerdomu;
    }

    public Integer getNumermieszkania() {
        return numermieszkania;
    }

    public Adres numermieszkania(Integer numermieszkania) {
        this.numermieszkania = numermieszkania;
        return this;
    }

    public void setNumermieszkania(Integer numermieszkania) {
        this.numermieszkania = numermieszkania;
    }

    public Zamowienie getZamowienie() {
        return zamowienie;
    }

    public Adres zamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
        return this;
    }

    public void setZamowienie(Zamowienie zamowienie) {
        this.zamowienie = zamowienie;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Adres adres = (Adres) o;
        if (adres.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), adres.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Adres{" +
            "id=" + getId() +
            ", panstwo='" + getPanstwo() + "'" +
            ", miasto='" + getMiasto() + "'" +
            ", ulica='" + getUlica() + "'" +
            ", numerdomu=" + getNumerdomu() +
            ", numermieszkania=" + getNumermieszkania() +
            "}";
    }
}
