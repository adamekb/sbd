import { element, by, ElementFinder } from 'protractor';

export class ProduktComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-produkt div table .btn-danger'));
    title = element.all(by.css('jhi-produkt div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ProduktUpdatePage {
    pageTitle = element(by.id('jhi-produkt-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nazwaInput = element(by.id('field_nazwa'));
    cenaInput = element(by.id('field_cena'));
    iloscInput = element(by.id('field_ilosc'));
    zdjecieSelect = element(by.id('field_zdjecie'));
    atrybutSelect = element(by.id('field_atrybut'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNazwaInput(nazwa) {
        await this.nazwaInput.sendKeys(nazwa);
    }

    async getNazwaInput() {
        return this.nazwaInput.getAttribute('value');
    }

    async setCenaInput(cena) {
        await this.cenaInput.sendKeys(cena);
    }

    async getCenaInput() {
        return this.cenaInput.getAttribute('value');
    }

    async setIloscInput(ilosc) {
        await this.iloscInput.sendKeys(ilosc);
    }

    async getIloscInput() {
        return this.iloscInput.getAttribute('value');
    }

    async zdjecieSelectLastOption() {
        await this.zdjecieSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async zdjecieSelectOption(option) {
        await this.zdjecieSelect.sendKeys(option);
    }

    getZdjecieSelect(): ElementFinder {
        return this.zdjecieSelect;
    }

    async getZdjecieSelectedOption() {
        return this.zdjecieSelect.element(by.css('option:checked')).getText();
    }

    async atrybutSelectLastOption() {
        await this.atrybutSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async atrybutSelectOption(option) {
        await this.atrybutSelect.sendKeys(option);
    }

    getAtrybutSelect(): ElementFinder {
        return this.atrybutSelect;
    }

    async getAtrybutSelectedOption() {
        return this.atrybutSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ProduktDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-produkt-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-produkt'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
