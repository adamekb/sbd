/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ProduktComponentsPage, ProduktDeleteDialog, ProduktUpdatePage } from './produkt.page-object';

const expect = chai.expect;

describe('Produkt e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let produktUpdatePage: ProduktUpdatePage;
    let produktComponentsPage: ProduktComponentsPage;
    let produktDeleteDialog: ProduktDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Produkts', async () => {
        await navBarPage.goToEntity('produkt');
        produktComponentsPage = new ProduktComponentsPage();
        expect(await produktComponentsPage.getTitle()).to.eq('Produkts');
    });

    it('should load create Produkt page', async () => {
        await produktComponentsPage.clickOnCreateButton();
        produktUpdatePage = new ProduktUpdatePage();
        expect(await produktUpdatePage.getPageTitle()).to.eq('Create or edit a Produkt');
        await produktUpdatePage.cancel();
    });

    it('should create and save Produkts', async () => {
        const nbButtonsBeforeCreate = await produktComponentsPage.countDeleteButtons();

        await produktComponentsPage.clickOnCreateButton();
        await promise.all([
            produktUpdatePage.setNazwaInput('nazwa'),
            produktUpdatePage.setCenaInput('5'),
            produktUpdatePage.setIloscInput('5'),
            produktUpdatePage.zdjecieSelectLastOption()
            // produktUpdatePage.atrybutSelectLastOption(),
        ]);
        expect(await produktUpdatePage.getNazwaInput()).to.eq('nazwa');
        expect(await produktUpdatePage.getCenaInput()).to.eq('5');
        expect(await produktUpdatePage.getIloscInput()).to.eq('5');
        await produktUpdatePage.save();
        expect(await produktUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await produktComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Produkt', async () => {
        const nbButtonsBeforeDelete = await produktComponentsPage.countDeleteButtons();
        await produktComponentsPage.clickOnLastDeleteButton();

        produktDeleteDialog = new ProduktDeleteDialog();
        expect(await produktDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Produkt?');
        await produktDeleteDialog.clickOnConfirmButton();

        expect(await produktComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
