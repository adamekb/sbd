import { element, by, ElementFinder } from 'protractor';

export class PozycjazamowieniaComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-pozycjazamowienia div table .btn-danger'));
    title = element.all(by.css('jhi-pozycjazamowienia div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class PozycjazamowieniaUpdatePage {
    pageTitle = element(by.id('jhi-pozycjazamowienia-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    pozycjaInput = element(by.id('field_pozycja'));
    cenasztukaInput = element(by.id('field_cenasztuka'));
    iloscInput = element(by.id('field_ilosc'));
    zamowienieSelect = element(by.id('field_zamowienie'));
    produktSelect = element(by.id('field_produkt'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setPozycjaInput(pozycja) {
        await this.pozycjaInput.sendKeys(pozycja);
    }

    async getPozycjaInput() {
        return this.pozycjaInput.getAttribute('value');
    }

    async setCenasztukaInput(cenasztuka) {
        await this.cenasztukaInput.sendKeys(cenasztuka);
    }

    async getCenasztukaInput() {
        return this.cenasztukaInput.getAttribute('value');
    }

    async setIloscInput(ilosc) {
        await this.iloscInput.sendKeys(ilosc);
    }

    async getIloscInput() {
        return this.iloscInput.getAttribute('value');
    }

    async zamowienieSelectLastOption() {
        await this.zamowienieSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async zamowienieSelectOption(option) {
        await this.zamowienieSelect.sendKeys(option);
    }

    getZamowienieSelect(): ElementFinder {
        return this.zamowienieSelect;
    }

    async getZamowienieSelectedOption() {
        return this.zamowienieSelect.element(by.css('option:checked')).getText();
    }

    async produktSelectLastOption() {
        await this.produktSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async produktSelectOption(option) {
        await this.produktSelect.sendKeys(option);
    }

    getProduktSelect(): ElementFinder {
        return this.produktSelect;
    }

    async getProduktSelectedOption() {
        return this.produktSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class PozycjazamowieniaDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-pozycjazamowienia-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-pozycjazamowienia'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
