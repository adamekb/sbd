/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
    PozycjazamowieniaComponentsPage,
    PozycjazamowieniaDeleteDialog,
    PozycjazamowieniaUpdatePage
} from './pozycjazamowienia.page-object';

const expect = chai.expect;

describe('Pozycjazamowienia e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let pozycjazamowieniaUpdatePage: PozycjazamowieniaUpdatePage;
    let pozycjazamowieniaComponentsPage: PozycjazamowieniaComponentsPage;
    let pozycjazamowieniaDeleteDialog: PozycjazamowieniaDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Pozycjazamowienias', async () => {
        await navBarPage.goToEntity('pozycjazamowienia');
        pozycjazamowieniaComponentsPage = new PozycjazamowieniaComponentsPage();
        expect(await pozycjazamowieniaComponentsPage.getTitle()).to.eq('Pozycjazamowienias');
    });

    it('should load create Pozycjazamowienia page', async () => {
        await pozycjazamowieniaComponentsPage.clickOnCreateButton();
        pozycjazamowieniaUpdatePage = new PozycjazamowieniaUpdatePage();
        expect(await pozycjazamowieniaUpdatePage.getPageTitle()).to.eq('Create or edit a Pozycjazamowienia');
        await pozycjazamowieniaUpdatePage.cancel();
    });

    it('should create and save Pozycjazamowienias', async () => {
        const nbButtonsBeforeCreate = await pozycjazamowieniaComponentsPage.countDeleteButtons();

        await pozycjazamowieniaComponentsPage.clickOnCreateButton();
        await promise.all([
            pozycjazamowieniaUpdatePage.setPozycjaInput('5'),
            pozycjazamowieniaUpdatePage.setCenasztukaInput('5'),
            pozycjazamowieniaUpdatePage.setIloscInput('5'),
            pozycjazamowieniaUpdatePage.zamowienieSelectLastOption(),
            pozycjazamowieniaUpdatePage.produktSelectLastOption()
        ]);
        expect(await pozycjazamowieniaUpdatePage.getPozycjaInput()).to.eq('5');
        expect(await pozycjazamowieniaUpdatePage.getCenasztukaInput()).to.eq('5');
        expect(await pozycjazamowieniaUpdatePage.getIloscInput()).to.eq('5');
        await pozycjazamowieniaUpdatePage.save();
        expect(await pozycjazamowieniaUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await pozycjazamowieniaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Pozycjazamowienia', async () => {
        const nbButtonsBeforeDelete = await pozycjazamowieniaComponentsPage.countDeleteButtons();
        await pozycjazamowieniaComponentsPage.clickOnLastDeleteButton();

        pozycjazamowieniaDeleteDialog = new PozycjazamowieniaDeleteDialog();
        expect(await pozycjazamowieniaDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Pozycjazamowienia?');
        await pozycjazamowieniaDeleteDialog.clickOnConfirmButton();

        expect(await pozycjazamowieniaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
