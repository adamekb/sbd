import { element, by, ElementFinder } from 'protractor';

export class AdresComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-adres div table .btn-danger'));
    title = element.all(by.css('jhi-adres div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class AdresUpdatePage {
    pageTitle = element(by.id('jhi-adres-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    panstwoInput = element(by.id('field_panstwo'));
    miastoInput = element(by.id('field_miasto'));
    ulicaInput = element(by.id('field_ulica'));
    numerdomuInput = element(by.id('field_numerdomu'));
    numermieszkaniaInput = element(by.id('field_numermieszkania'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setPanstwoInput(panstwo) {
        await this.panstwoInput.sendKeys(panstwo);
    }

    async getPanstwoInput() {
        return this.panstwoInput.getAttribute('value');
    }

    async setMiastoInput(miasto) {
        await this.miastoInput.sendKeys(miasto);
    }

    async getMiastoInput() {
        return this.miastoInput.getAttribute('value');
    }

    async setUlicaInput(ulica) {
        await this.ulicaInput.sendKeys(ulica);
    }

    async getUlicaInput() {
        return this.ulicaInput.getAttribute('value');
    }

    async setNumerdomuInput(numerdomu) {
        await this.numerdomuInput.sendKeys(numerdomu);
    }

    async getNumerdomuInput() {
        return this.numerdomuInput.getAttribute('value');
    }

    async setNumermieszkaniaInput(numermieszkania) {
        await this.numermieszkaniaInput.sendKeys(numermieszkania);
    }

    async getNumermieszkaniaInput() {
        return this.numermieszkaniaInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AdresDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-adres-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-adres'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
