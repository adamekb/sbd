/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AdresComponentsPage, AdresDeleteDialog, AdresUpdatePage } from './adres.page-object';

const expect = chai.expect;

describe('Adres e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let adresUpdatePage: AdresUpdatePage;
    let adresComponentsPage: AdresComponentsPage;
    let adresDeleteDialog: AdresDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Adres', async () => {
        await navBarPage.goToEntity('adres');
        adresComponentsPage = new AdresComponentsPage();
        expect(await adresComponentsPage.getTitle()).to.eq('Adres');
    });

    it('should load create Adres page', async () => {
        await adresComponentsPage.clickOnCreateButton();
        adresUpdatePage = new AdresUpdatePage();
        expect(await adresUpdatePage.getPageTitle()).to.eq('Create or edit a Adres');
        await adresUpdatePage.cancel();
    });

    it('should create and save Adres', async () => {
        const nbButtonsBeforeCreate = await adresComponentsPage.countDeleteButtons();

        await adresComponentsPage.clickOnCreateButton();
        await promise.all([
            adresUpdatePage.setPanstwoInput('panstwo'),
            adresUpdatePage.setMiastoInput('miasto'),
            adresUpdatePage.setUlicaInput('ulica'),
            adresUpdatePage.setNumerdomuInput('5'),
            adresUpdatePage.setNumermieszkaniaInput('5')
        ]);
        expect(await adresUpdatePage.getPanstwoInput()).to.eq('panstwo');
        expect(await adresUpdatePage.getMiastoInput()).to.eq('miasto');
        expect(await adresUpdatePage.getUlicaInput()).to.eq('ulica');
        expect(await adresUpdatePage.getNumerdomuInput()).to.eq('5');
        expect(await adresUpdatePage.getNumermieszkaniaInput()).to.eq('5');
        await adresUpdatePage.save();
        expect(await adresUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await adresComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Adres', async () => {
        const nbButtonsBeforeDelete = await adresComponentsPage.countDeleteButtons();
        await adresComponentsPage.clickOnLastDeleteButton();

        adresDeleteDialog = new AdresDeleteDialog();
        expect(await adresDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Adres?');
        await adresDeleteDialog.clickOnConfirmButton();

        expect(await adresComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
