/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SposobdostawyComponentsPage, SposobdostawyDeleteDialog, SposobdostawyUpdatePage } from './sposobdostawy.page-object';

const expect = chai.expect;

describe('Sposobdostawy e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let sposobdostawyUpdatePage: SposobdostawyUpdatePage;
    let sposobdostawyComponentsPage: SposobdostawyComponentsPage;
    let sposobdostawyDeleteDialog: SposobdostawyDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Sposobdostawies', async () => {
        await navBarPage.goToEntity('sposobdostawy');
        sposobdostawyComponentsPage = new SposobdostawyComponentsPage();
        expect(await sposobdostawyComponentsPage.getTitle()).to.eq('Sposobdostawies');
    });

    it('should load create Sposobdostawy page', async () => {
        await sposobdostawyComponentsPage.clickOnCreateButton();
        sposobdostawyUpdatePage = new SposobdostawyUpdatePage();
        expect(await sposobdostawyUpdatePage.getPageTitle()).to.eq('Create or edit a Sposobdostawy');
        await sposobdostawyUpdatePage.cancel();
    });

    it('should create and save Sposobdostawies', async () => {
        const nbButtonsBeforeCreate = await sposobdostawyComponentsPage.countDeleteButtons();

        await sposobdostawyComponentsPage.clickOnCreateButton();
        await promise.all([
            sposobdostawyUpdatePage.setRodzajInput('rodzaj'),
            sposobdostawyUpdatePage.setCenaInput('5'),
            sposobdostawyUpdatePage.zamowienieSelectLastOption()
        ]);
        expect(await sposobdostawyUpdatePage.getRodzajInput()).to.eq('rodzaj');
        expect(await sposobdostawyUpdatePage.getCenaInput()).to.eq('5');
        await sposobdostawyUpdatePage.save();
        expect(await sposobdostawyUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await sposobdostawyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Sposobdostawy', async () => {
        const nbButtonsBeforeDelete = await sposobdostawyComponentsPage.countDeleteButtons();
        await sposobdostawyComponentsPage.clickOnLastDeleteButton();

        sposobdostawyDeleteDialog = new SposobdostawyDeleteDialog();
        expect(await sposobdostawyDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Sposobdostawy?');
        await sposobdostawyDeleteDialog.clickOnConfirmButton();

        expect(await sposobdostawyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
