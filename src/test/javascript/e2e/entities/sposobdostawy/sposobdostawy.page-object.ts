import { element, by, ElementFinder } from 'protractor';

export class SposobdostawyComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-sposobdostawy div table .btn-danger'));
    title = element.all(by.css('jhi-sposobdostawy div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class SposobdostawyUpdatePage {
    pageTitle = element(by.id('jhi-sposobdostawy-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    rodzajInput = element(by.id('field_rodzaj'));
    cenaInput = element(by.id('field_cena'));
    zamowienieSelect = element(by.id('field_zamowienie'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setRodzajInput(rodzaj) {
        await this.rodzajInput.sendKeys(rodzaj);
    }

    async getRodzajInput() {
        return this.rodzajInput.getAttribute('value');
    }

    async setCenaInput(cena) {
        await this.cenaInput.sendKeys(cena);
    }

    async getCenaInput() {
        return this.cenaInput.getAttribute('value');
    }

    async zamowienieSelectLastOption() {
        await this.zamowienieSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async zamowienieSelectOption(option) {
        await this.zamowienieSelect.sendKeys(option);
    }

    getZamowienieSelect(): ElementFinder {
        return this.zamowienieSelect;
    }

    async getZamowienieSelectedOption() {
        return this.zamowienieSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class SposobdostawyDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-sposobdostawy-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-sposobdostawy'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
