import { element, by, ElementFinder } from 'protractor';

export class ZamowienieComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-zamowienie div table .btn-danger'));
    title = element.all(by.css('jhi-zamowienie div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ZamowienieUpdatePage {
    pageTitle = element(by.id('jhi-zamowienie-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    cenacalkowitaInput = element(by.id('field_cenacalkowita'));
    iloscproduktowInput = element(by.id('field_iloscproduktow'));
    rabatSelect = element(by.id('field_rabat'));
    adresSelect = element(by.id('field_adres'));
    userSelect = element(by.id('field_user'));
    statusSelect = element(by.id('field_status'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setCenacalkowitaInput(cenacalkowita) {
        await this.cenacalkowitaInput.sendKeys(cenacalkowita);
    }

    async getCenacalkowitaInput() {
        return this.cenacalkowitaInput.getAttribute('value');
    }

    async setIloscproduktowInput(iloscproduktow) {
        await this.iloscproduktowInput.sendKeys(iloscproduktow);
    }

    async getIloscproduktowInput() {
        return this.iloscproduktowInput.getAttribute('value');
    }

    async rabatSelectLastOption() {
        await this.rabatSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async rabatSelectOption(option) {
        await this.rabatSelect.sendKeys(option);
    }

    getRabatSelect(): ElementFinder {
        return this.rabatSelect;
    }

    async getRabatSelectedOption() {
        return this.rabatSelect.element(by.css('option:checked')).getText();
    }

    async adresSelectLastOption() {
        await this.adresSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async adresSelectOption(option) {
        await this.adresSelect.sendKeys(option);
    }

    getAdresSelect(): ElementFinder {
        return this.adresSelect;
    }

    async getAdresSelectedOption() {
        return this.adresSelect.element(by.css('option:checked')).getText();
    }

    async userSelectLastOption() {
        await this.userSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async userSelectOption(option) {
        await this.userSelect.sendKeys(option);
    }

    getUserSelect(): ElementFinder {
        return this.userSelect;
    }

    async getUserSelectedOption() {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    async statusSelectLastOption() {
        await this.statusSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async statusSelectOption(option) {
        await this.statusSelect.sendKeys(option);
    }

    getStatusSelect(): ElementFinder {
        return this.statusSelect;
    }

    async getStatusSelectedOption() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ZamowienieDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-zamowienie-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-zamowienie'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
