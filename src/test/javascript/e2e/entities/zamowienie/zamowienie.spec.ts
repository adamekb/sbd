/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ZamowienieComponentsPage, ZamowienieDeleteDialog, ZamowienieUpdatePage } from './zamowienie.page-object';

const expect = chai.expect;

describe('Zamowienie e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let zamowienieUpdatePage: ZamowienieUpdatePage;
    let zamowienieComponentsPage: ZamowienieComponentsPage;
    let zamowienieDeleteDialog: ZamowienieDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Zamowienies', async () => {
        await navBarPage.goToEntity('zamowienie');
        zamowienieComponentsPage = new ZamowienieComponentsPage();
        expect(await zamowienieComponentsPage.getTitle()).to.eq('Zamowienies');
    });

    it('should load create Zamowienie page', async () => {
        await zamowienieComponentsPage.clickOnCreateButton();
        zamowienieUpdatePage = new ZamowienieUpdatePage();
        expect(await zamowienieUpdatePage.getPageTitle()).to.eq('Create or edit a Zamowienie');
        await zamowienieUpdatePage.cancel();
    });

    it('should create and save Zamowienies', async () => {
        const nbButtonsBeforeCreate = await zamowienieComponentsPage.countDeleteButtons();

        await zamowienieComponentsPage.clickOnCreateButton();
        await promise.all([
            zamowienieUpdatePage.setCenacalkowitaInput('5'),
            zamowienieUpdatePage.setIloscproduktowInput('5'),
            zamowienieUpdatePage.rabatSelectLastOption(),
            zamowienieUpdatePage.adresSelectLastOption(),
            zamowienieUpdatePage.userSelectLastOption(),
            zamowienieUpdatePage.statusSelectLastOption()
        ]);
        expect(await zamowienieUpdatePage.getCenacalkowitaInput()).to.eq('5');
        expect(await zamowienieUpdatePage.getIloscproduktowInput()).to.eq('5');
        await zamowienieUpdatePage.save();
        expect(await zamowienieUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await zamowienieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Zamowienie', async () => {
        const nbButtonsBeforeDelete = await zamowienieComponentsPage.countDeleteButtons();
        await zamowienieComponentsPage.clickOnLastDeleteButton();

        zamowienieDeleteDialog = new ZamowienieDeleteDialog();
        expect(await zamowienieDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Zamowienie?');
        await zamowienieDeleteDialog.clickOnConfirmButton();

        expect(await zamowienieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
