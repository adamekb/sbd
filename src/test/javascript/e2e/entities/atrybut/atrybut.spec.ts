/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AtrybutComponentsPage, AtrybutDeleteDialog, AtrybutUpdatePage } from './atrybut.page-object';

const expect = chai.expect;

describe('Atrybut e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let atrybutUpdatePage: AtrybutUpdatePage;
    let atrybutComponentsPage: AtrybutComponentsPage;
    let atrybutDeleteDialog: AtrybutDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Atrybuts', async () => {
        await navBarPage.goToEntity('atrybut');
        atrybutComponentsPage = new AtrybutComponentsPage();
        expect(await atrybutComponentsPage.getTitle()).to.eq('Atrybuts');
    });

    it('should load create Atrybut page', async () => {
        await atrybutComponentsPage.clickOnCreateButton();
        atrybutUpdatePage = new AtrybutUpdatePage();
        expect(await atrybutUpdatePage.getPageTitle()).to.eq('Create or edit a Atrybut');
        await atrybutUpdatePage.cancel();
    });

    it('should create and save Atrybuts', async () => {
        const nbButtonsBeforeCreate = await atrybutComponentsPage.countDeleteButtons();

        await atrybutComponentsPage.clickOnCreateButton();
        await promise.all([atrybutUpdatePage.setNazwaInput('nazwa')]);
        expect(await atrybutUpdatePage.getNazwaInput()).to.eq('nazwa');
        await atrybutUpdatePage.save();
        expect(await atrybutUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await atrybutComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Atrybut', async () => {
        const nbButtonsBeforeDelete = await atrybutComponentsPage.countDeleteButtons();
        await atrybutComponentsPage.clickOnLastDeleteButton();

        atrybutDeleteDialog = new AtrybutDeleteDialog();
        expect(await atrybutDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Atrybut?');
        await atrybutDeleteDialog.clickOnConfirmButton();

        expect(await atrybutComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
