/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ZdjecieComponentsPage, ZdjecieDeleteDialog, ZdjecieUpdatePage } from './zdjecie.page-object';

const expect = chai.expect;

describe('Zdjecie e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let zdjecieUpdatePage: ZdjecieUpdatePage;
    let zdjecieComponentsPage: ZdjecieComponentsPage;
    let zdjecieDeleteDialog: ZdjecieDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Zdjecies', async () => {
        await navBarPage.goToEntity('zdjecie');
        zdjecieComponentsPage = new ZdjecieComponentsPage();
        expect(await zdjecieComponentsPage.getTitle()).to.eq('Zdjecies');
    });

    it('should load create Zdjecie page', async () => {
        await zdjecieComponentsPage.clickOnCreateButton();
        zdjecieUpdatePage = new ZdjecieUpdatePage();
        expect(await zdjecieUpdatePage.getPageTitle()).to.eq('Create or edit a Zdjecie');
        await zdjecieUpdatePage.cancel();
    });

    it('should create and save Zdjecies', async () => {
        const nbButtonsBeforeCreate = await zdjecieComponentsPage.countDeleteButtons();

        await zdjecieComponentsPage.clickOnCreateButton();
        await promise.all([zdjecieUpdatePage.setOpisInput('opis')]);
        expect(await zdjecieUpdatePage.getOpisInput()).to.eq('opis');
        await zdjecieUpdatePage.save();
        expect(await zdjecieUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await zdjecieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Zdjecie', async () => {
        const nbButtonsBeforeDelete = await zdjecieComponentsPage.countDeleteButtons();
        await zdjecieComponentsPage.clickOnLastDeleteButton();

        zdjecieDeleteDialog = new ZdjecieDeleteDialog();
        expect(await zdjecieDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Zdjecie?');
        await zdjecieDeleteDialog.clickOnConfirmButton();

        expect(await zdjecieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
