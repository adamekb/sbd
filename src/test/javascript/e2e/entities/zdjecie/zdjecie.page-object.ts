import { element, by, ElementFinder } from 'protractor';

export class ZdjecieComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-zdjecie div table .btn-danger'));
    title = element.all(by.css('jhi-zdjecie div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ZdjecieUpdatePage {
    pageTitle = element(by.id('jhi-zdjecie-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    opisInput = element(by.id('field_opis'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setOpisInput(opis) {
        await this.opisInput.sendKeys(opis);
    }

    async getOpisInput() {
        return this.opisInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ZdjecieDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-zdjecie-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-zdjecie'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
