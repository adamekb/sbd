/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { RabatComponentsPage, RabatDeleteDialog, RabatUpdatePage } from './rabat.page-object';

const expect = chai.expect;

describe('Rabat e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let rabatUpdatePage: RabatUpdatePage;
    let rabatComponentsPage: RabatComponentsPage;
    let rabatDeleteDialog: RabatDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Rabats', async () => {
        await navBarPage.goToEntity('rabat');
        rabatComponentsPage = new RabatComponentsPage();
        expect(await rabatComponentsPage.getTitle()).to.eq('Rabats');
    });

    it('should load create Rabat page', async () => {
        await rabatComponentsPage.clickOnCreateButton();
        rabatUpdatePage = new RabatUpdatePage();
        expect(await rabatUpdatePage.getPageTitle()).to.eq('Create or edit a Rabat');
        await rabatUpdatePage.cancel();
    });

    it('should create and save Rabats', async () => {
        const nbButtonsBeforeCreate = await rabatComponentsPage.countDeleteButtons();

        await rabatComponentsPage.clickOnCreateButton();
        await promise.all([rabatUpdatePage.setNazwaInput('nazwa'), rabatUpdatePage.setWielkoscInput('5')]);
        expect(await rabatUpdatePage.getNazwaInput()).to.eq('nazwa');
        expect(await rabatUpdatePage.getWielkoscInput()).to.eq('5');
        await rabatUpdatePage.save();
        expect(await rabatUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await rabatComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Rabat', async () => {
        const nbButtonsBeforeDelete = await rabatComponentsPage.countDeleteButtons();
        await rabatComponentsPage.clickOnLastDeleteButton();

        rabatDeleteDialog = new RabatDeleteDialog();
        expect(await rabatDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Rabat?');
        await rabatDeleteDialog.clickOnConfirmButton();

        expect(await rabatComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
