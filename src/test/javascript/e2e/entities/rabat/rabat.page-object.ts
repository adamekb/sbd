import { element, by, ElementFinder } from 'protractor';

export class RabatComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-rabat div table .btn-danger'));
    title = element.all(by.css('jhi-rabat div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class RabatUpdatePage {
    pageTitle = element(by.id('jhi-rabat-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nazwaInput = element(by.id('field_nazwa'));
    wielkoscInput = element(by.id('field_wielkosc'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNazwaInput(nazwa) {
        await this.nazwaInput.sendKeys(nazwa);
    }

    async getNazwaInput() {
        return this.nazwaInput.getAttribute('value');
    }

    async setWielkoscInput(wielkosc) {
        await this.wielkoscInput.sendKeys(wielkosc);
    }

    async getWielkoscInput() {
        return this.wielkoscInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class RabatDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-rabat-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-rabat'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
