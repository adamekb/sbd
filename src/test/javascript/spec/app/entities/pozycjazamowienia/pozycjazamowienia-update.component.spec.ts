/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { PozycjazamowieniaUpdateComponent } from 'app/entities/pozycjazamowienia/pozycjazamowienia-update.component';
import { PozycjazamowieniaService } from 'app/entities/pozycjazamowienia/pozycjazamowienia.service';
import { Pozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';

describe('Component Tests', () => {
    describe('Pozycjazamowienia Management Update Component', () => {
        let comp: PozycjazamowieniaUpdateComponent;
        let fixture: ComponentFixture<PozycjazamowieniaUpdateComponent>;
        let service: PozycjazamowieniaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [PozycjazamowieniaUpdateComponent]
            })
                .overrideTemplate(PozycjazamowieniaUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PozycjazamowieniaUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PozycjazamowieniaService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Pozycjazamowienia(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.pozycjazamowienia = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Pozycjazamowienia();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.pozycjazamowienia = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
