/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SbdsklepTestModule } from '../../../test.module';
import { PozycjazamowieniaDeleteDialogComponent } from 'app/entities/pozycjazamowienia/pozycjazamowienia-delete-dialog.component';
import { PozycjazamowieniaService } from 'app/entities/pozycjazamowienia/pozycjazamowienia.service';

describe('Component Tests', () => {
    describe('Pozycjazamowienia Management Delete Component', () => {
        let comp: PozycjazamowieniaDeleteDialogComponent;
        let fixture: ComponentFixture<PozycjazamowieniaDeleteDialogComponent>;
        let service: PozycjazamowieniaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [PozycjazamowieniaDeleteDialogComponent]
            })
                .overrideTemplate(PozycjazamowieniaDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PozycjazamowieniaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PozycjazamowieniaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
