/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SbdsklepTestModule } from '../../../test.module';
import { PozycjazamowieniaComponent } from 'app/entities/pozycjazamowienia/pozycjazamowienia.component';
import { PozycjazamowieniaService } from 'app/entities/pozycjazamowienia/pozycjazamowienia.service';
import { Pozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';

describe('Component Tests', () => {
    describe('Pozycjazamowienia Management Component', () => {
        let comp: PozycjazamowieniaComponent;
        let fixture: ComponentFixture<PozycjazamowieniaComponent>;
        let service: PozycjazamowieniaService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [PozycjazamowieniaComponent],
                providers: []
            })
                .overrideTemplate(PozycjazamowieniaComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PozycjazamowieniaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PozycjazamowieniaService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Pozycjazamowienia(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.pozycjazamowienias[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
