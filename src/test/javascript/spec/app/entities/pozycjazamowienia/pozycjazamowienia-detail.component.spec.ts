/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { PozycjazamowieniaDetailComponent } from 'app/entities/pozycjazamowienia/pozycjazamowienia-detail.component';
import { Pozycjazamowienia } from 'app/shared/model/pozycjazamowienia.model';

describe('Component Tests', () => {
    describe('Pozycjazamowienia Management Detail Component', () => {
        let comp: PozycjazamowieniaDetailComponent;
        let fixture: ComponentFixture<PozycjazamowieniaDetailComponent>;
        const route = ({ data: of({ pozycjazamowienia: new Pozycjazamowienia(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [PozycjazamowieniaDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PozycjazamowieniaDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PozycjazamowieniaDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.pozycjazamowienia).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
