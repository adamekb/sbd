/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SbdsklepTestModule } from '../../../test.module';
import { AdresDeleteDialogComponent } from 'app/entities/adres/adres-delete-dialog.component';
import { AdresService } from 'app/entities/adres/adres.service';

describe('Component Tests', () => {
    describe('Adres Management Delete Component', () => {
        let comp: AdresDeleteDialogComponent;
        let fixture: ComponentFixture<AdresDeleteDialogComponent>;
        let service: AdresService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [AdresDeleteDialogComponent]
            })
                .overrideTemplate(AdresDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AdresDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdresService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
