/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SbdsklepTestModule } from '../../../test.module';
import { AtrybutComponent } from 'app/entities/atrybut/atrybut.component';
import { AtrybutService } from 'app/entities/atrybut/atrybut.service';
import { Atrybut } from 'app/shared/model/atrybut.model';

describe('Component Tests', () => {
    describe('Atrybut Management Component', () => {
        let comp: AtrybutComponent;
        let fixture: ComponentFixture<AtrybutComponent>;
        let service: AtrybutService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [AtrybutComponent],
                providers: []
            })
                .overrideTemplate(AtrybutComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AtrybutComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AtrybutService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Atrybut(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.atrybuts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
