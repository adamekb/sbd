/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { AtrybutUpdateComponent } from 'app/entities/atrybut/atrybut-update.component';
import { AtrybutService } from 'app/entities/atrybut/atrybut.service';
import { Atrybut } from 'app/shared/model/atrybut.model';

describe('Component Tests', () => {
    describe('Atrybut Management Update Component', () => {
        let comp: AtrybutUpdateComponent;
        let fixture: ComponentFixture<AtrybutUpdateComponent>;
        let service: AtrybutService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [AtrybutUpdateComponent]
            })
                .overrideTemplate(AtrybutUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AtrybutUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AtrybutService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Atrybut(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.atrybut = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Atrybut();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.atrybut = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
