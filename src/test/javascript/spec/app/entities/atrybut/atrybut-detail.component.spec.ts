/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { AtrybutDetailComponent } from 'app/entities/atrybut/atrybut-detail.component';
import { Atrybut } from 'app/shared/model/atrybut.model';

describe('Component Tests', () => {
    describe('Atrybut Management Detail Component', () => {
        let comp: AtrybutDetailComponent;
        let fixture: ComponentFixture<AtrybutDetailComponent>;
        const route = ({ data: of({ atrybut: new Atrybut(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [AtrybutDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AtrybutDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AtrybutDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.atrybut).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
