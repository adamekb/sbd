/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SbdsklepTestModule } from '../../../test.module';
import { ZdjecieComponent } from 'app/entities/zdjecie/zdjecie.component';
import { ZdjecieService } from 'app/entities/zdjecie/zdjecie.service';
import { Zdjecie } from 'app/shared/model/zdjecie.model';

describe('Component Tests', () => {
    describe('Zdjecie Management Component', () => {
        let comp: ZdjecieComponent;
        let fixture: ComponentFixture<ZdjecieComponent>;
        let service: ZdjecieService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [ZdjecieComponent],
                providers: []
            })
                .overrideTemplate(ZdjecieComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ZdjecieComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZdjecieService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Zdjecie(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.zdjecies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
