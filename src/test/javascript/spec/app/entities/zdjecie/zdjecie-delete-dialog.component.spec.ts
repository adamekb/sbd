/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SbdsklepTestModule } from '../../../test.module';
import { ZdjecieDeleteDialogComponent } from 'app/entities/zdjecie/zdjecie-delete-dialog.component';
import { ZdjecieService } from 'app/entities/zdjecie/zdjecie.service';

describe('Component Tests', () => {
    describe('Zdjecie Management Delete Component', () => {
        let comp: ZdjecieDeleteDialogComponent;
        let fixture: ComponentFixture<ZdjecieDeleteDialogComponent>;
        let service: ZdjecieService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [ZdjecieDeleteDialogComponent]
            })
                .overrideTemplate(ZdjecieDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ZdjecieDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZdjecieService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
