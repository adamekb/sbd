/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { ZdjecieUpdateComponent } from 'app/entities/zdjecie/zdjecie-update.component';
import { ZdjecieService } from 'app/entities/zdjecie/zdjecie.service';
import { Zdjecie } from 'app/shared/model/zdjecie.model';

describe('Component Tests', () => {
    describe('Zdjecie Management Update Component', () => {
        let comp: ZdjecieUpdateComponent;
        let fixture: ComponentFixture<ZdjecieUpdateComponent>;
        let service: ZdjecieService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [ZdjecieUpdateComponent]
            })
                .overrideTemplate(ZdjecieUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ZdjecieUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZdjecieService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Zdjecie(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.zdjecie = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Zdjecie();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.zdjecie = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
