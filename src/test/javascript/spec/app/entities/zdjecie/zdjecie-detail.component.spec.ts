/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { ZdjecieDetailComponent } from 'app/entities/zdjecie/zdjecie-detail.component';
import { Zdjecie } from 'app/shared/model/zdjecie.model';

describe('Component Tests', () => {
    describe('Zdjecie Management Detail Component', () => {
        let comp: ZdjecieDetailComponent;
        let fixture: ComponentFixture<ZdjecieDetailComponent>;
        const route = ({ data: of({ zdjecie: new Zdjecie(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [ZdjecieDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ZdjecieDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ZdjecieDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.zdjecie).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
