/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SbdsklepTestModule } from '../../../test.module';
import { RabatDeleteDialogComponent } from 'app/entities/rabat/rabat-delete-dialog.component';
import { RabatService } from 'app/entities/rabat/rabat.service';

describe('Component Tests', () => {
    describe('Rabat Management Delete Component', () => {
        let comp: RabatDeleteDialogComponent;
        let fixture: ComponentFixture<RabatDeleteDialogComponent>;
        let service: RabatService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [RabatDeleteDialogComponent]
            })
                .overrideTemplate(RabatDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RabatDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RabatService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
