/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { RabatUpdateComponent } from 'app/entities/rabat/rabat-update.component';
import { RabatService } from 'app/entities/rabat/rabat.service';
import { Rabat } from 'app/shared/model/rabat.model';

describe('Component Tests', () => {
    describe('Rabat Management Update Component', () => {
        let comp: RabatUpdateComponent;
        let fixture: ComponentFixture<RabatUpdateComponent>;
        let service: RabatService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [RabatUpdateComponent]
            })
                .overrideTemplate(RabatUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RabatUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RabatService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Rabat(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.rabat = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Rabat();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.rabat = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
