/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SbdsklepTestModule } from '../../../test.module';
import { RabatComponent } from 'app/entities/rabat/rabat.component';
import { RabatService } from 'app/entities/rabat/rabat.service';
import { Rabat } from 'app/shared/model/rabat.model';

describe('Component Tests', () => {
    describe('Rabat Management Component', () => {
        let comp: RabatComponent;
        let fixture: ComponentFixture<RabatComponent>;
        let service: RabatService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [RabatComponent],
                providers: []
            })
                .overrideTemplate(RabatComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RabatComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RabatService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Rabat(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.rabats[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
