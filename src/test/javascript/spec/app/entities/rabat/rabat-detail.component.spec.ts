/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { RabatDetailComponent } from 'app/entities/rabat/rabat-detail.component';
import { Rabat } from 'app/shared/model/rabat.model';

describe('Component Tests', () => {
    describe('Rabat Management Detail Component', () => {
        let comp: RabatDetailComponent;
        let fixture: ComponentFixture<RabatDetailComponent>;
        const route = ({ data: of({ rabat: new Rabat(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [RabatDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RabatDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RabatDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.rabat).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
