/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { SposobdostawyDetailComponent } from 'app/entities/sposobdostawy/sposobdostawy-detail.component';
import { Sposobdostawy } from 'app/shared/model/sposobdostawy.model';

describe('Component Tests', () => {
    describe('Sposobdostawy Management Detail Component', () => {
        let comp: SposobdostawyDetailComponent;
        let fixture: ComponentFixture<SposobdostawyDetailComponent>;
        const route = ({ data: of({ sposobdostawy: new Sposobdostawy(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [SposobdostawyDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SposobdostawyDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SposobdostawyDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sposobdostawy).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
