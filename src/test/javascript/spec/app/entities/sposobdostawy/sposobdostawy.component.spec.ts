/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SbdsklepTestModule } from '../../../test.module';
import { SposobdostawyComponent } from 'app/entities/sposobdostawy/sposobdostawy.component';
import { SposobdostawyService } from 'app/entities/sposobdostawy/sposobdostawy.service';
import { Sposobdostawy } from 'app/shared/model/sposobdostawy.model';

describe('Component Tests', () => {
    describe('Sposobdostawy Management Component', () => {
        let comp: SposobdostawyComponent;
        let fixture: ComponentFixture<SposobdostawyComponent>;
        let service: SposobdostawyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [SposobdostawyComponent],
                providers: []
            })
                .overrideTemplate(SposobdostawyComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SposobdostawyComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SposobdostawyService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Sposobdostawy(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.sposobdostawies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
