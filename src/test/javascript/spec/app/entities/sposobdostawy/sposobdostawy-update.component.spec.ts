/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SbdsklepTestModule } from '../../../test.module';
import { SposobdostawyUpdateComponent } from 'app/entities/sposobdostawy/sposobdostawy-update.component';
import { SposobdostawyService } from 'app/entities/sposobdostawy/sposobdostawy.service';
import { Sposobdostawy } from 'app/shared/model/sposobdostawy.model';

describe('Component Tests', () => {
    describe('Sposobdostawy Management Update Component', () => {
        let comp: SposobdostawyUpdateComponent;
        let fixture: ComponentFixture<SposobdostawyUpdateComponent>;
        let service: SposobdostawyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [SposobdostawyUpdateComponent]
            })
                .overrideTemplate(SposobdostawyUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SposobdostawyUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SposobdostawyService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Sposobdostawy(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sposobdostawy = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Sposobdostawy();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sposobdostawy = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
