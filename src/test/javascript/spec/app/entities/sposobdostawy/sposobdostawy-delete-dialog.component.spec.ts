/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SbdsklepTestModule } from '../../../test.module';
import { SposobdostawyDeleteDialogComponent } from 'app/entities/sposobdostawy/sposobdostawy-delete-dialog.component';
import { SposobdostawyService } from 'app/entities/sposobdostawy/sposobdostawy.service';

describe('Component Tests', () => {
    describe('Sposobdostawy Management Delete Component', () => {
        let comp: SposobdostawyDeleteDialogComponent;
        let fixture: ComponentFixture<SposobdostawyDeleteDialogComponent>;
        let service: SposobdostawyService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SbdsklepTestModule],
                declarations: [SposobdostawyDeleteDialogComponent]
            })
                .overrideTemplate(SposobdostawyDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SposobdostawyDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SposobdostawyService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
