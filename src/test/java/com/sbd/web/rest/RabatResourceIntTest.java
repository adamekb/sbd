package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Rabat;
import com.sbd.repository.RabatRepository;
import com.sbd.service.RabatService;
import com.sbd.service.dto.RabatDTO;
import com.sbd.service.mapper.RabatMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RabatResource REST controller.
 *
 * @see RabatResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class RabatResourceIntTest {

    private static final String DEFAULT_NAZWA = "AAAAAAAAAA";
    private static final String UPDATED_NAZWA = "BBBBBBBBBB";

    private static final Float DEFAULT_WIELKOSC = 1F;
    private static final Float UPDATED_WIELKOSC = 2F;

    @Autowired
    private RabatRepository rabatRepository;

    @Autowired
    private RabatMapper rabatMapper;

    @Autowired
    private RabatService rabatService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRabatMockMvc;

    private Rabat rabat;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RabatResource rabatResource = new RabatResource(rabatService);
        this.restRabatMockMvc = MockMvcBuilders.standaloneSetup(rabatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Rabat createEntity(EntityManager em) {
        Rabat rabat = new Rabat()
            .nazwa(DEFAULT_NAZWA)
            .wielkosc(DEFAULT_WIELKOSC);
        return rabat;
    }

    @Before
    public void initTest() {
        rabat = createEntity(em);
    }

    @Test
    @Transactional
    public void createRabat() throws Exception {
        int databaseSizeBeforeCreate = rabatRepository.findAll().size();

        // Create the Rabat
        RabatDTO rabatDTO = rabatMapper.toDto(rabat);
        restRabatMockMvc.perform(post("/api/rabats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rabatDTO)))
            .andExpect(status().isCreated());

        // Validate the Rabat in the database
        List<Rabat> rabatList = rabatRepository.findAll();
        assertThat(rabatList).hasSize(databaseSizeBeforeCreate + 1);
        Rabat testRabat = rabatList.get(rabatList.size() - 1);
        assertThat(testRabat.getNazwa()).isEqualTo(DEFAULT_NAZWA);
        assertThat(testRabat.getWielkosc()).isEqualTo(DEFAULT_WIELKOSC);
    }

    @Test
    @Transactional
    public void createRabatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rabatRepository.findAll().size();

        // Create the Rabat with an existing ID
        rabat.setId(1L);
        RabatDTO rabatDTO = rabatMapper.toDto(rabat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRabatMockMvc.perform(post("/api/rabats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rabatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Rabat in the database
        List<Rabat> rabatList = rabatRepository.findAll();
        assertThat(rabatList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRabats() throws Exception {
        // Initialize the database
        rabatRepository.saveAndFlush(rabat);

        // Get all the rabatList
        restRabatMockMvc.perform(get("/api/rabats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rabat.getId().intValue())))
            .andExpect(jsonPath("$.[*].nazwa").value(hasItem(DEFAULT_NAZWA.toString())))
            .andExpect(jsonPath("$.[*].wielkosc").value(hasItem(DEFAULT_WIELKOSC.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getRabat() throws Exception {
        // Initialize the database
        rabatRepository.saveAndFlush(rabat);

        // Get the rabat
        restRabatMockMvc.perform(get("/api/rabats/{id}", rabat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rabat.getId().intValue()))
            .andExpect(jsonPath("$.nazwa").value(DEFAULT_NAZWA.toString()))
            .andExpect(jsonPath("$.wielkosc").value(DEFAULT_WIELKOSC.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRabat() throws Exception {
        // Get the rabat
        restRabatMockMvc.perform(get("/api/rabats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRabat() throws Exception {
        // Initialize the database
        rabatRepository.saveAndFlush(rabat);

        int databaseSizeBeforeUpdate = rabatRepository.findAll().size();

        // Update the rabat
        Rabat updatedRabat = rabatRepository.findById(rabat.getId()).get();
        // Disconnect from session so that the updates on updatedRabat are not directly saved in db
        em.detach(updatedRabat);
        updatedRabat
            .nazwa(UPDATED_NAZWA)
            .wielkosc(UPDATED_WIELKOSC);
        RabatDTO rabatDTO = rabatMapper.toDto(updatedRabat);

        restRabatMockMvc.perform(put("/api/rabats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rabatDTO)))
            .andExpect(status().isOk());

        // Validate the Rabat in the database
        List<Rabat> rabatList = rabatRepository.findAll();
        assertThat(rabatList).hasSize(databaseSizeBeforeUpdate);
        Rabat testRabat = rabatList.get(rabatList.size() - 1);
        assertThat(testRabat.getNazwa()).isEqualTo(UPDATED_NAZWA);
        assertThat(testRabat.getWielkosc()).isEqualTo(UPDATED_WIELKOSC);
    }

    @Test
    @Transactional
    public void updateNonExistingRabat() throws Exception {
        int databaseSizeBeforeUpdate = rabatRepository.findAll().size();

        // Create the Rabat
        RabatDTO rabatDTO = rabatMapper.toDto(rabat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRabatMockMvc.perform(put("/api/rabats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rabatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Rabat in the database
        List<Rabat> rabatList = rabatRepository.findAll();
        assertThat(rabatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRabat() throws Exception {
        // Initialize the database
        rabatRepository.saveAndFlush(rabat);

        int databaseSizeBeforeDelete = rabatRepository.findAll().size();

        // Get the rabat
        restRabatMockMvc.perform(delete("/api/rabats/{id}", rabat.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Rabat> rabatList = rabatRepository.findAll();
        assertThat(rabatList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Rabat.class);
        Rabat rabat1 = new Rabat();
        rabat1.setId(1L);
        Rabat rabat2 = new Rabat();
        rabat2.setId(rabat1.getId());
        assertThat(rabat1).isEqualTo(rabat2);
        rabat2.setId(2L);
        assertThat(rabat1).isNotEqualTo(rabat2);
        rabat1.setId(null);
        assertThat(rabat1).isNotEqualTo(rabat2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RabatDTO.class);
        RabatDTO rabatDTO1 = new RabatDTO();
        rabatDTO1.setId(1L);
        RabatDTO rabatDTO2 = new RabatDTO();
        assertThat(rabatDTO1).isNotEqualTo(rabatDTO2);
        rabatDTO2.setId(rabatDTO1.getId());
        assertThat(rabatDTO1).isEqualTo(rabatDTO2);
        rabatDTO2.setId(2L);
        assertThat(rabatDTO1).isNotEqualTo(rabatDTO2);
        rabatDTO1.setId(null);
        assertThat(rabatDTO1).isNotEqualTo(rabatDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(rabatMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(rabatMapper.fromId(null)).isNull();
    }
}
