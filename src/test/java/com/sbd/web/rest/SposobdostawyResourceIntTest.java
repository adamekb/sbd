package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Sposobdostawy;
import com.sbd.repository.SposobdostawyRepository;
import com.sbd.service.SposobdostawyService;
import com.sbd.service.dto.SposobdostawyDTO;
import com.sbd.service.mapper.SposobdostawyMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SposobdostawyResource REST controller.
 *
 * @see SposobdostawyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class SposobdostawyResourceIntTest {

    private static final String DEFAULT_RODZAJ = "AAAAAAAAAA";
    private static final String UPDATED_RODZAJ = "BBBBBBBBBB";

    private static final Float DEFAULT_CENA = 1F;
    private static final Float UPDATED_CENA = 2F;

    @Autowired
    private SposobdostawyRepository sposobdostawyRepository;

    @Autowired
    private SposobdostawyMapper sposobdostawyMapper;

    @Autowired
    private SposobdostawyService sposobdostawyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSposobdostawyMockMvc;

    private Sposobdostawy sposobdostawy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SposobdostawyResource sposobdostawyResource = new SposobdostawyResource(sposobdostawyService);
        this.restSposobdostawyMockMvc = MockMvcBuilders.standaloneSetup(sposobdostawyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sposobdostawy createEntity(EntityManager em) {
        Sposobdostawy sposobdostawy = new Sposobdostawy()
            .rodzaj(DEFAULT_RODZAJ)
            .cena(DEFAULT_CENA);
        return sposobdostawy;
    }

    @Before
    public void initTest() {
        sposobdostawy = createEntity(em);
    }

    @Test
    @Transactional
    public void createSposobdostawy() throws Exception {
        int databaseSizeBeforeCreate = sposobdostawyRepository.findAll().size();

        // Create the Sposobdostawy
        SposobdostawyDTO sposobdostawyDTO = sposobdostawyMapper.toDto(sposobdostawy);
        restSposobdostawyMockMvc.perform(post("/api/sposobdostawies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sposobdostawyDTO)))
            .andExpect(status().isCreated());

        // Validate the Sposobdostawy in the database
        List<Sposobdostawy> sposobdostawyList = sposobdostawyRepository.findAll();
        assertThat(sposobdostawyList).hasSize(databaseSizeBeforeCreate + 1);
        Sposobdostawy testSposobdostawy = sposobdostawyList.get(sposobdostawyList.size() - 1);
        assertThat(testSposobdostawy.getRodzaj()).isEqualTo(DEFAULT_RODZAJ);
        assertThat(testSposobdostawy.getCena()).isEqualTo(DEFAULT_CENA);
    }

    @Test
    @Transactional
    public void createSposobdostawyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sposobdostawyRepository.findAll().size();

        // Create the Sposobdostawy with an existing ID
        sposobdostawy.setId(1L);
        SposobdostawyDTO sposobdostawyDTO = sposobdostawyMapper.toDto(sposobdostawy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSposobdostawyMockMvc.perform(post("/api/sposobdostawies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sposobdostawyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sposobdostawy in the database
        List<Sposobdostawy> sposobdostawyList = sposobdostawyRepository.findAll();
        assertThat(sposobdostawyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSposobdostawies() throws Exception {
        // Initialize the database
        sposobdostawyRepository.saveAndFlush(sposobdostawy);

        // Get all the sposobdostawyList
        restSposobdostawyMockMvc.perform(get("/api/sposobdostawies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sposobdostawy.getId().intValue())))
            .andExpect(jsonPath("$.[*].rodzaj").value(hasItem(DEFAULT_RODZAJ.toString())))
            .andExpect(jsonPath("$.[*].cena").value(hasItem(DEFAULT_CENA.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getSposobdostawy() throws Exception {
        // Initialize the database
        sposobdostawyRepository.saveAndFlush(sposobdostawy);

        // Get the sposobdostawy
        restSposobdostawyMockMvc.perform(get("/api/sposobdostawies/{id}", sposobdostawy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sposobdostawy.getId().intValue()))
            .andExpect(jsonPath("$.rodzaj").value(DEFAULT_RODZAJ.toString()))
            .andExpect(jsonPath("$.cena").value(DEFAULT_CENA.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSposobdostawy() throws Exception {
        // Get the sposobdostawy
        restSposobdostawyMockMvc.perform(get("/api/sposobdostawies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSposobdostawy() throws Exception {
        // Initialize the database
        sposobdostawyRepository.saveAndFlush(sposobdostawy);

        int databaseSizeBeforeUpdate = sposobdostawyRepository.findAll().size();

        // Update the sposobdostawy
        Sposobdostawy updatedSposobdostawy = sposobdostawyRepository.findById(sposobdostawy.getId()).get();
        // Disconnect from session so that the updates on updatedSposobdostawy are not directly saved in db
        em.detach(updatedSposobdostawy);
        updatedSposobdostawy
            .rodzaj(UPDATED_RODZAJ)
            .cena(UPDATED_CENA);
        SposobdostawyDTO sposobdostawyDTO = sposobdostawyMapper.toDto(updatedSposobdostawy);

        restSposobdostawyMockMvc.perform(put("/api/sposobdostawies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sposobdostawyDTO)))
            .andExpect(status().isOk());

        // Validate the Sposobdostawy in the database
        List<Sposobdostawy> sposobdostawyList = sposobdostawyRepository.findAll();
        assertThat(sposobdostawyList).hasSize(databaseSizeBeforeUpdate);
        Sposobdostawy testSposobdostawy = sposobdostawyList.get(sposobdostawyList.size() - 1);
        assertThat(testSposobdostawy.getRodzaj()).isEqualTo(UPDATED_RODZAJ);
        assertThat(testSposobdostawy.getCena()).isEqualTo(UPDATED_CENA);
    }

    @Test
    @Transactional
    public void updateNonExistingSposobdostawy() throws Exception {
        int databaseSizeBeforeUpdate = sposobdostawyRepository.findAll().size();

        // Create the Sposobdostawy
        SposobdostawyDTO sposobdostawyDTO = sposobdostawyMapper.toDto(sposobdostawy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSposobdostawyMockMvc.perform(put("/api/sposobdostawies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sposobdostawyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sposobdostawy in the database
        List<Sposobdostawy> sposobdostawyList = sposobdostawyRepository.findAll();
        assertThat(sposobdostawyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSposobdostawy() throws Exception {
        // Initialize the database
        sposobdostawyRepository.saveAndFlush(sposobdostawy);

        int databaseSizeBeforeDelete = sposobdostawyRepository.findAll().size();

        // Get the sposobdostawy
        restSposobdostawyMockMvc.perform(delete("/api/sposobdostawies/{id}", sposobdostawy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Sposobdostawy> sposobdostawyList = sposobdostawyRepository.findAll();
        assertThat(sposobdostawyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sposobdostawy.class);
        Sposobdostawy sposobdostawy1 = new Sposobdostawy();
        sposobdostawy1.setId(1L);
        Sposobdostawy sposobdostawy2 = new Sposobdostawy();
        sposobdostawy2.setId(sposobdostawy1.getId());
        assertThat(sposobdostawy1).isEqualTo(sposobdostawy2);
        sposobdostawy2.setId(2L);
        assertThat(sposobdostawy1).isNotEqualTo(sposobdostawy2);
        sposobdostawy1.setId(null);
        assertThat(sposobdostawy1).isNotEqualTo(sposobdostawy2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SposobdostawyDTO.class);
        SposobdostawyDTO sposobdostawyDTO1 = new SposobdostawyDTO();
        sposobdostawyDTO1.setId(1L);
        SposobdostawyDTO sposobdostawyDTO2 = new SposobdostawyDTO();
        assertThat(sposobdostawyDTO1).isNotEqualTo(sposobdostawyDTO2);
        sposobdostawyDTO2.setId(sposobdostawyDTO1.getId());
        assertThat(sposobdostawyDTO1).isEqualTo(sposobdostawyDTO2);
        sposobdostawyDTO2.setId(2L);
        assertThat(sposobdostawyDTO1).isNotEqualTo(sposobdostawyDTO2);
        sposobdostawyDTO1.setId(null);
        assertThat(sposobdostawyDTO1).isNotEqualTo(sposobdostawyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sposobdostawyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sposobdostawyMapper.fromId(null)).isNull();
    }
}
