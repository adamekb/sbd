package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Produkt;
import com.sbd.repository.ProduktRepository;
import com.sbd.service.ProduktService;
import com.sbd.service.dto.ProduktDTO;
import com.sbd.service.mapper.ProduktMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProduktResource REST controller.
 *
 * @see ProduktResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class ProduktResourceIntTest {

    private static final String DEFAULT_NAZWA = "AAAAAAAAAA";
    private static final String UPDATED_NAZWA = "BBBBBBBBBB";

    private static final Float DEFAULT_CENA = 1F;
    private static final Float UPDATED_CENA = 2F;

    private static final Integer DEFAULT_ILOSC = 1;
    private static final Integer UPDATED_ILOSC = 2;

    @Autowired
    private ProduktRepository produktRepository;

    @Mock
    private ProduktRepository produktRepositoryMock;

    @Autowired
    private ProduktMapper produktMapper;

    @Mock
    private ProduktService produktServiceMock;

    @Autowired
    private ProduktService produktService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProduktMockMvc;

    private Produkt produkt;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProduktResource produktResource = new ProduktResource(produktService);
        this.restProduktMockMvc = MockMvcBuilders.standaloneSetup(produktResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Produkt createEntity(EntityManager em) {
        Produkt produkt = new Produkt()
            .nazwa(DEFAULT_NAZWA)
            .cena(DEFAULT_CENA)
            .ilosc(DEFAULT_ILOSC);
        return produkt;
    }

    @Before
    public void initTest() {
        produkt = createEntity(em);
    }

    @Test
    @Transactional
    public void createProdukt() throws Exception {
        int databaseSizeBeforeCreate = produktRepository.findAll().size();

        // Create the Produkt
        ProduktDTO produktDTO = produktMapper.toDto(produkt);
        restProduktMockMvc.perform(post("/api/produkts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produktDTO)))
            .andExpect(status().isCreated());

        // Validate the Produkt in the database
        List<Produkt> produktList = produktRepository.findAll();
        assertThat(produktList).hasSize(databaseSizeBeforeCreate + 1);
        Produkt testProdukt = produktList.get(produktList.size() - 1);
        assertThat(testProdukt.getNazwa()).isEqualTo(DEFAULT_NAZWA);
        assertThat(testProdukt.getCena()).isEqualTo(DEFAULT_CENA);
        assertThat(testProdukt.getIlosc()).isEqualTo(DEFAULT_ILOSC);
    }

    @Test
    @Transactional
    public void createProduktWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = produktRepository.findAll().size();

        // Create the Produkt with an existing ID
        produkt.setId(1L);
        ProduktDTO produktDTO = produktMapper.toDto(produkt);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProduktMockMvc.perform(post("/api/produkts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produktDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Produkt in the database
        List<Produkt> produktList = produktRepository.findAll();
        assertThat(produktList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProdukts() throws Exception {
        // Initialize the database
        produktRepository.saveAndFlush(produkt);

        // Get all the produktList
        restProduktMockMvc.perform(get("/api/produkts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(produkt.getId().intValue())))
            .andExpect(jsonPath("$.[*].nazwa").value(hasItem(DEFAULT_NAZWA.toString())))
            .andExpect(jsonPath("$.[*].cena").value(hasItem(DEFAULT_CENA.doubleValue())))
            .andExpect(jsonPath("$.[*].ilosc").value(hasItem(DEFAULT_ILOSC)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllProduktsWithEagerRelationshipsIsEnabled() throws Exception {
        ProduktResource produktResource = new ProduktResource(produktServiceMock);
        when(produktServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restProduktMockMvc = MockMvcBuilders.standaloneSetup(produktResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restProduktMockMvc.perform(get("/api/produkts?eagerload=true"))
        .andExpect(status().isOk());

        verify(produktServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllProduktsWithEagerRelationshipsIsNotEnabled() throws Exception {
        ProduktResource produktResource = new ProduktResource(produktServiceMock);
            when(produktServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restProduktMockMvc = MockMvcBuilders.standaloneSetup(produktResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restProduktMockMvc.perform(get("/api/produkts?eagerload=true"))
        .andExpect(status().isOk());

            verify(produktServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getProdukt() throws Exception {
        // Initialize the database
        produktRepository.saveAndFlush(produkt);

        // Get the produkt
        restProduktMockMvc.perform(get("/api/produkts/{id}", produkt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(produkt.getId().intValue()))
            .andExpect(jsonPath("$.nazwa").value(DEFAULT_NAZWA.toString()))
            .andExpect(jsonPath("$.cena").value(DEFAULT_CENA.doubleValue()))
            .andExpect(jsonPath("$.ilosc").value(DEFAULT_ILOSC));
    }

    @Test
    @Transactional
    public void getNonExistingProdukt() throws Exception {
        // Get the produkt
        restProduktMockMvc.perform(get("/api/produkts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProdukt() throws Exception {
        // Initialize the database
        produktRepository.saveAndFlush(produkt);

        int databaseSizeBeforeUpdate = produktRepository.findAll().size();

        // Update the produkt
        Produkt updatedProdukt = produktRepository.findById(produkt.getId()).get();
        // Disconnect from session so that the updates on updatedProdukt are not directly saved in db
        em.detach(updatedProdukt);
        updatedProdukt
            .nazwa(UPDATED_NAZWA)
            .cena(UPDATED_CENA)
            .ilosc(UPDATED_ILOSC);
        ProduktDTO produktDTO = produktMapper.toDto(updatedProdukt);

        restProduktMockMvc.perform(put("/api/produkts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produktDTO)))
            .andExpect(status().isOk());

        // Validate the Produkt in the database
        List<Produkt> produktList = produktRepository.findAll();
        assertThat(produktList).hasSize(databaseSizeBeforeUpdate);
        Produkt testProdukt = produktList.get(produktList.size() - 1);
        assertThat(testProdukt.getNazwa()).isEqualTo(UPDATED_NAZWA);
        assertThat(testProdukt.getCena()).isEqualTo(UPDATED_CENA);
        assertThat(testProdukt.getIlosc()).isEqualTo(UPDATED_ILOSC);
    }

    @Test
    @Transactional
    public void updateNonExistingProdukt() throws Exception {
        int databaseSizeBeforeUpdate = produktRepository.findAll().size();

        // Create the Produkt
        ProduktDTO produktDTO = produktMapper.toDto(produkt);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProduktMockMvc.perform(put("/api/produkts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produktDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Produkt in the database
        List<Produkt> produktList = produktRepository.findAll();
        assertThat(produktList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProdukt() throws Exception {
        // Initialize the database
        produktRepository.saveAndFlush(produkt);

        int databaseSizeBeforeDelete = produktRepository.findAll().size();

        // Get the produkt
        restProduktMockMvc.perform(delete("/api/produkts/{id}", produkt.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Produkt> produktList = produktRepository.findAll();
        assertThat(produktList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Produkt.class);
        Produkt produkt1 = new Produkt();
        produkt1.setId(1L);
        Produkt produkt2 = new Produkt();
        produkt2.setId(produkt1.getId());
        assertThat(produkt1).isEqualTo(produkt2);
        produkt2.setId(2L);
        assertThat(produkt1).isNotEqualTo(produkt2);
        produkt1.setId(null);
        assertThat(produkt1).isNotEqualTo(produkt2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProduktDTO.class);
        ProduktDTO produktDTO1 = new ProduktDTO();
        produktDTO1.setId(1L);
        ProduktDTO produktDTO2 = new ProduktDTO();
        assertThat(produktDTO1).isNotEqualTo(produktDTO2);
        produktDTO2.setId(produktDTO1.getId());
        assertThat(produktDTO1).isEqualTo(produktDTO2);
        produktDTO2.setId(2L);
        assertThat(produktDTO1).isNotEqualTo(produktDTO2);
        produktDTO1.setId(null);
        assertThat(produktDTO1).isNotEqualTo(produktDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(produktMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(produktMapper.fromId(null)).isNull();
    }
}
