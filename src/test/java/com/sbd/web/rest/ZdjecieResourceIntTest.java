package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Zdjecie;
import com.sbd.repository.ZdjecieRepository;
import com.sbd.service.ZdjecieService;
import com.sbd.service.dto.ZdjecieDTO;
import com.sbd.service.mapper.ZdjecieMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ZdjecieResource REST controller.
 *
 * @see ZdjecieResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class ZdjecieResourceIntTest {

    private static final String DEFAULT_OPIS = "AAAAAAAAAA";
    private static final String UPDATED_OPIS = "BBBBBBBBBB";

    @Autowired
    private ZdjecieRepository zdjecieRepository;

    @Autowired
    private ZdjecieMapper zdjecieMapper;

    @Autowired
    private ZdjecieService zdjecieService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restZdjecieMockMvc;

    private Zdjecie zdjecie;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ZdjecieResource zdjecieResource = new ZdjecieResource(zdjecieService);
        this.restZdjecieMockMvc = MockMvcBuilders.standaloneSetup(zdjecieResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zdjecie createEntity(EntityManager em) {
        Zdjecie zdjecie = new Zdjecie()
            .opis(DEFAULT_OPIS);
        return zdjecie;
    }

    @Before
    public void initTest() {
        zdjecie = createEntity(em);
    }

    @Test
    @Transactional
    public void createZdjecie() throws Exception {
        int databaseSizeBeforeCreate = zdjecieRepository.findAll().size();

        // Create the Zdjecie
        ZdjecieDTO zdjecieDTO = zdjecieMapper.toDto(zdjecie);
        restZdjecieMockMvc.perform(post("/api/zdjecies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zdjecieDTO)))
            .andExpect(status().isCreated());

        // Validate the Zdjecie in the database
        List<Zdjecie> zdjecieList = zdjecieRepository.findAll();
        assertThat(zdjecieList).hasSize(databaseSizeBeforeCreate + 1);
        Zdjecie testZdjecie = zdjecieList.get(zdjecieList.size() - 1);
        assertThat(testZdjecie.getOpis()).isEqualTo(DEFAULT_OPIS);
    }

    @Test
    @Transactional
    public void createZdjecieWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = zdjecieRepository.findAll().size();

        // Create the Zdjecie with an existing ID
        zdjecie.setId(1L);
        ZdjecieDTO zdjecieDTO = zdjecieMapper.toDto(zdjecie);

        // An entity with an existing ID cannot be created, so this API call must fail
        restZdjecieMockMvc.perform(post("/api/zdjecies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zdjecieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Zdjecie in the database
        List<Zdjecie> zdjecieList = zdjecieRepository.findAll();
        assertThat(zdjecieList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllZdjecies() throws Exception {
        // Initialize the database
        zdjecieRepository.saveAndFlush(zdjecie);

        // Get all the zdjecieList
        restZdjecieMockMvc.perform(get("/api/zdjecies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zdjecie.getId().intValue())))
            .andExpect(jsonPath("$.[*].opis").value(hasItem(DEFAULT_OPIS.toString())));
    }
    
    @Test
    @Transactional
    public void getZdjecie() throws Exception {
        // Initialize the database
        zdjecieRepository.saveAndFlush(zdjecie);

        // Get the zdjecie
        restZdjecieMockMvc.perform(get("/api/zdjecies/{id}", zdjecie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(zdjecie.getId().intValue()))
            .andExpect(jsonPath("$.opis").value(DEFAULT_OPIS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingZdjecie() throws Exception {
        // Get the zdjecie
        restZdjecieMockMvc.perform(get("/api/zdjecies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZdjecie() throws Exception {
        // Initialize the database
        zdjecieRepository.saveAndFlush(zdjecie);

        int databaseSizeBeforeUpdate = zdjecieRepository.findAll().size();

        // Update the zdjecie
        Zdjecie updatedZdjecie = zdjecieRepository.findById(zdjecie.getId()).get();
        // Disconnect from session so that the updates on updatedZdjecie are not directly saved in db
        em.detach(updatedZdjecie);
        updatedZdjecie
            .opis(UPDATED_OPIS);
        ZdjecieDTO zdjecieDTO = zdjecieMapper.toDto(updatedZdjecie);

        restZdjecieMockMvc.perform(put("/api/zdjecies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zdjecieDTO)))
            .andExpect(status().isOk());

        // Validate the Zdjecie in the database
        List<Zdjecie> zdjecieList = zdjecieRepository.findAll();
        assertThat(zdjecieList).hasSize(databaseSizeBeforeUpdate);
        Zdjecie testZdjecie = zdjecieList.get(zdjecieList.size() - 1);
        assertThat(testZdjecie.getOpis()).isEqualTo(UPDATED_OPIS);
    }

    @Test
    @Transactional
    public void updateNonExistingZdjecie() throws Exception {
        int databaseSizeBeforeUpdate = zdjecieRepository.findAll().size();

        // Create the Zdjecie
        ZdjecieDTO zdjecieDTO = zdjecieMapper.toDto(zdjecie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restZdjecieMockMvc.perform(put("/api/zdjecies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zdjecieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Zdjecie in the database
        List<Zdjecie> zdjecieList = zdjecieRepository.findAll();
        assertThat(zdjecieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteZdjecie() throws Exception {
        // Initialize the database
        zdjecieRepository.saveAndFlush(zdjecie);

        int databaseSizeBeforeDelete = zdjecieRepository.findAll().size();

        // Get the zdjecie
        restZdjecieMockMvc.perform(delete("/api/zdjecies/{id}", zdjecie.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Zdjecie> zdjecieList = zdjecieRepository.findAll();
        assertThat(zdjecieList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Zdjecie.class);
        Zdjecie zdjecie1 = new Zdjecie();
        zdjecie1.setId(1L);
        Zdjecie zdjecie2 = new Zdjecie();
        zdjecie2.setId(zdjecie1.getId());
        assertThat(zdjecie1).isEqualTo(zdjecie2);
        zdjecie2.setId(2L);
        assertThat(zdjecie1).isNotEqualTo(zdjecie2);
        zdjecie1.setId(null);
        assertThat(zdjecie1).isNotEqualTo(zdjecie2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ZdjecieDTO.class);
        ZdjecieDTO zdjecieDTO1 = new ZdjecieDTO();
        zdjecieDTO1.setId(1L);
        ZdjecieDTO zdjecieDTO2 = new ZdjecieDTO();
        assertThat(zdjecieDTO1).isNotEqualTo(zdjecieDTO2);
        zdjecieDTO2.setId(zdjecieDTO1.getId());
        assertThat(zdjecieDTO1).isEqualTo(zdjecieDTO2);
        zdjecieDTO2.setId(2L);
        assertThat(zdjecieDTO1).isNotEqualTo(zdjecieDTO2);
        zdjecieDTO1.setId(null);
        assertThat(zdjecieDTO1).isNotEqualTo(zdjecieDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(zdjecieMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(zdjecieMapper.fromId(null)).isNull();
    }
}
