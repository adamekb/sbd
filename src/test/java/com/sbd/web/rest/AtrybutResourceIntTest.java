package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Atrybut;
import com.sbd.repository.AtrybutRepository;
import com.sbd.service.AtrybutService;
import com.sbd.service.dto.AtrybutDTO;
import com.sbd.service.mapper.AtrybutMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AtrybutResource REST controller.
 *
 * @see AtrybutResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class AtrybutResourceIntTest {

    private static final String DEFAULT_NAZWA = "AAAAAAAAAA";
    private static final String UPDATED_NAZWA = "BBBBBBBBBB";

    @Autowired
    private AtrybutRepository atrybutRepository;

    @Autowired
    private AtrybutMapper atrybutMapper;

    @Autowired
    private AtrybutService atrybutService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAtrybutMockMvc;

    private Atrybut atrybut;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AtrybutResource atrybutResource = new AtrybutResource(atrybutService);
        this.restAtrybutMockMvc = MockMvcBuilders.standaloneSetup(atrybutResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atrybut createEntity(EntityManager em) {
        Atrybut atrybut = new Atrybut()
            .nazwa(DEFAULT_NAZWA);
        return atrybut;
    }

    @Before
    public void initTest() {
        atrybut = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtrybut() throws Exception {
        int databaseSizeBeforeCreate = atrybutRepository.findAll().size();

        // Create the Atrybut
        AtrybutDTO atrybutDTO = atrybutMapper.toDto(atrybut);
        restAtrybutMockMvc.perform(post("/api/atrybuts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrybutDTO)))
            .andExpect(status().isCreated());

        // Validate the Atrybut in the database
        List<Atrybut> atrybutList = atrybutRepository.findAll();
        assertThat(atrybutList).hasSize(databaseSizeBeforeCreate + 1);
        Atrybut testAtrybut = atrybutList.get(atrybutList.size() - 1);
        assertThat(testAtrybut.getNazwa()).isEqualTo(DEFAULT_NAZWA);
    }

    @Test
    @Transactional
    public void createAtrybutWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atrybutRepository.findAll().size();

        // Create the Atrybut with an existing ID
        atrybut.setId(1L);
        AtrybutDTO atrybutDTO = atrybutMapper.toDto(atrybut);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtrybutMockMvc.perform(post("/api/atrybuts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrybutDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Atrybut in the database
        List<Atrybut> atrybutList = atrybutRepository.findAll();
        assertThat(atrybutList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAtrybuts() throws Exception {
        // Initialize the database
        atrybutRepository.saveAndFlush(atrybut);

        // Get all the atrybutList
        restAtrybutMockMvc.perform(get("/api/atrybuts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atrybut.getId().intValue())))
            .andExpect(jsonPath("$.[*].nazwa").value(hasItem(DEFAULT_NAZWA.toString())));
    }
    
    @Test
    @Transactional
    public void getAtrybut() throws Exception {
        // Initialize the database
        atrybutRepository.saveAndFlush(atrybut);

        // Get the atrybut
        restAtrybutMockMvc.perform(get("/api/atrybuts/{id}", atrybut.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(atrybut.getId().intValue()))
            .andExpect(jsonPath("$.nazwa").value(DEFAULT_NAZWA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAtrybut() throws Exception {
        // Get the atrybut
        restAtrybutMockMvc.perform(get("/api/atrybuts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtrybut() throws Exception {
        // Initialize the database
        atrybutRepository.saveAndFlush(atrybut);

        int databaseSizeBeforeUpdate = atrybutRepository.findAll().size();

        // Update the atrybut
        Atrybut updatedAtrybut = atrybutRepository.findById(atrybut.getId()).get();
        // Disconnect from session so that the updates on updatedAtrybut are not directly saved in db
        em.detach(updatedAtrybut);
        updatedAtrybut
            .nazwa(UPDATED_NAZWA);
        AtrybutDTO atrybutDTO = atrybutMapper.toDto(updatedAtrybut);

        restAtrybutMockMvc.perform(put("/api/atrybuts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrybutDTO)))
            .andExpect(status().isOk());

        // Validate the Atrybut in the database
        List<Atrybut> atrybutList = atrybutRepository.findAll();
        assertThat(atrybutList).hasSize(databaseSizeBeforeUpdate);
        Atrybut testAtrybut = atrybutList.get(atrybutList.size() - 1);
        assertThat(testAtrybut.getNazwa()).isEqualTo(UPDATED_NAZWA);
    }

    @Test
    @Transactional
    public void updateNonExistingAtrybut() throws Exception {
        int databaseSizeBeforeUpdate = atrybutRepository.findAll().size();

        // Create the Atrybut
        AtrybutDTO atrybutDTO = atrybutMapper.toDto(atrybut);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtrybutMockMvc.perform(put("/api/atrybuts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(atrybutDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Atrybut in the database
        List<Atrybut> atrybutList = atrybutRepository.findAll();
        assertThat(atrybutList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAtrybut() throws Exception {
        // Initialize the database
        atrybutRepository.saveAndFlush(atrybut);

        int databaseSizeBeforeDelete = atrybutRepository.findAll().size();

        // Get the atrybut
        restAtrybutMockMvc.perform(delete("/api/atrybuts/{id}", atrybut.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Atrybut> atrybutList = atrybutRepository.findAll();
        assertThat(atrybutList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Atrybut.class);
        Atrybut atrybut1 = new Atrybut();
        atrybut1.setId(1L);
        Atrybut atrybut2 = new Atrybut();
        atrybut2.setId(atrybut1.getId());
        assertThat(atrybut1).isEqualTo(atrybut2);
        atrybut2.setId(2L);
        assertThat(atrybut1).isNotEqualTo(atrybut2);
        atrybut1.setId(null);
        assertThat(atrybut1).isNotEqualTo(atrybut2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtrybutDTO.class);
        AtrybutDTO atrybutDTO1 = new AtrybutDTO();
        atrybutDTO1.setId(1L);
        AtrybutDTO atrybutDTO2 = new AtrybutDTO();
        assertThat(atrybutDTO1).isNotEqualTo(atrybutDTO2);
        atrybutDTO2.setId(atrybutDTO1.getId());
        assertThat(atrybutDTO1).isEqualTo(atrybutDTO2);
        atrybutDTO2.setId(2L);
        assertThat(atrybutDTO1).isNotEqualTo(atrybutDTO2);
        atrybutDTO1.setId(null);
        assertThat(atrybutDTO1).isNotEqualTo(atrybutDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(atrybutMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(atrybutMapper.fromId(null)).isNull();
    }
}
