package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Pozycjazamowienia;
import com.sbd.repository.PozycjazamowieniaRepository;
import com.sbd.service.PozycjazamowieniaService;
import com.sbd.service.dto.PozycjazamowieniaDTO;
import com.sbd.service.mapper.PozycjazamowieniaMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PozycjazamowieniaResource REST controller.
 *
 * @see PozycjazamowieniaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class PozycjazamowieniaResourceIntTest {

    private static final Integer DEFAULT_POZYCJA = 1;
    private static final Integer UPDATED_POZYCJA = 2;

    private static final Float DEFAULT_CENASZTUKA = 1F;
    private static final Float UPDATED_CENASZTUKA = 2F;

    private static final Integer DEFAULT_ILOSC = 1;
    private static final Integer UPDATED_ILOSC = 2;

    @Autowired
    private PozycjazamowieniaRepository pozycjazamowieniaRepository;

    @Autowired
    private PozycjazamowieniaMapper pozycjazamowieniaMapper;

    @Autowired
    private PozycjazamowieniaService pozycjazamowieniaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPozycjazamowieniaMockMvc;

    private Pozycjazamowienia pozycjazamowienia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PozycjazamowieniaResource pozycjazamowieniaResource = new PozycjazamowieniaResource(pozycjazamowieniaService);
        this.restPozycjazamowieniaMockMvc = MockMvcBuilders.standaloneSetup(pozycjazamowieniaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pozycjazamowienia createEntity(EntityManager em) {
        Pozycjazamowienia pozycjazamowienia = new Pozycjazamowienia()
            .pozycja(DEFAULT_POZYCJA)
            .cenasztuka(DEFAULT_CENASZTUKA)
            .ilosc(DEFAULT_ILOSC);
        return pozycjazamowienia;
    }

    @Before
    public void initTest() {
        pozycjazamowienia = createEntity(em);
    }

    @Test
    @Transactional
    public void createPozycjazamowienia() throws Exception {
        int databaseSizeBeforeCreate = pozycjazamowieniaRepository.findAll().size();

        // Create the Pozycjazamowienia
        PozycjazamowieniaDTO pozycjazamowieniaDTO = pozycjazamowieniaMapper.toDto(pozycjazamowienia);
        restPozycjazamowieniaMockMvc.perform(post("/api/pozycjazamowienias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pozycjazamowieniaDTO)))
            .andExpect(status().isCreated());

        // Validate the Pozycjazamowienia in the database
        List<Pozycjazamowienia> pozycjazamowieniaList = pozycjazamowieniaRepository.findAll();
        assertThat(pozycjazamowieniaList).hasSize(databaseSizeBeforeCreate + 1);
        Pozycjazamowienia testPozycjazamowienia = pozycjazamowieniaList.get(pozycjazamowieniaList.size() - 1);
        assertThat(testPozycjazamowienia.getPozycja()).isEqualTo(DEFAULT_POZYCJA);
        assertThat(testPozycjazamowienia.getCenasztuka()).isEqualTo(DEFAULT_CENASZTUKA);
        assertThat(testPozycjazamowienia.getIlosc()).isEqualTo(DEFAULT_ILOSC);
    }

    @Test
    @Transactional
    public void createPozycjazamowieniaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pozycjazamowieniaRepository.findAll().size();

        // Create the Pozycjazamowienia with an existing ID
        pozycjazamowienia.setId(1L);
        PozycjazamowieniaDTO pozycjazamowieniaDTO = pozycjazamowieniaMapper.toDto(pozycjazamowienia);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPozycjazamowieniaMockMvc.perform(post("/api/pozycjazamowienias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pozycjazamowieniaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pozycjazamowienia in the database
        List<Pozycjazamowienia> pozycjazamowieniaList = pozycjazamowieniaRepository.findAll();
        assertThat(pozycjazamowieniaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPozycjazamowienias() throws Exception {
        // Initialize the database
        pozycjazamowieniaRepository.saveAndFlush(pozycjazamowienia);

        // Get all the pozycjazamowieniaList
        restPozycjazamowieniaMockMvc.perform(get("/api/pozycjazamowienias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pozycjazamowienia.getId().intValue())))
            .andExpect(jsonPath("$.[*].pozycja").value(hasItem(DEFAULT_POZYCJA)))
            .andExpect(jsonPath("$.[*].cenasztuka").value(hasItem(DEFAULT_CENASZTUKA.doubleValue())))
            .andExpect(jsonPath("$.[*].ilosc").value(hasItem(DEFAULT_ILOSC)));
    }
    
    @Test
    @Transactional
    public void getPozycjazamowienia() throws Exception {
        // Initialize the database
        pozycjazamowieniaRepository.saveAndFlush(pozycjazamowienia);

        // Get the pozycjazamowienia
        restPozycjazamowieniaMockMvc.perform(get("/api/pozycjazamowienias/{id}", pozycjazamowienia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pozycjazamowienia.getId().intValue()))
            .andExpect(jsonPath("$.pozycja").value(DEFAULT_POZYCJA))
            .andExpect(jsonPath("$.cenasztuka").value(DEFAULT_CENASZTUKA.doubleValue()))
            .andExpect(jsonPath("$.ilosc").value(DEFAULT_ILOSC));
    }

    @Test
    @Transactional
    public void getNonExistingPozycjazamowienia() throws Exception {
        // Get the pozycjazamowienia
        restPozycjazamowieniaMockMvc.perform(get("/api/pozycjazamowienias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePozycjazamowienia() throws Exception {
        // Initialize the database
        pozycjazamowieniaRepository.saveAndFlush(pozycjazamowienia);

        int databaseSizeBeforeUpdate = pozycjazamowieniaRepository.findAll().size();

        // Update the pozycjazamowienia
        Pozycjazamowienia updatedPozycjazamowienia = pozycjazamowieniaRepository.findById(pozycjazamowienia.getId()).get();
        // Disconnect from session so that the updates on updatedPozycjazamowienia are not directly saved in db
        em.detach(updatedPozycjazamowienia);
        updatedPozycjazamowienia
            .pozycja(UPDATED_POZYCJA)
            .cenasztuka(UPDATED_CENASZTUKA)
            .ilosc(UPDATED_ILOSC);
        PozycjazamowieniaDTO pozycjazamowieniaDTO = pozycjazamowieniaMapper.toDto(updatedPozycjazamowienia);

        restPozycjazamowieniaMockMvc.perform(put("/api/pozycjazamowienias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pozycjazamowieniaDTO)))
            .andExpect(status().isOk());

        // Validate the Pozycjazamowienia in the database
        List<Pozycjazamowienia> pozycjazamowieniaList = pozycjazamowieniaRepository.findAll();
        assertThat(pozycjazamowieniaList).hasSize(databaseSizeBeforeUpdate);
        Pozycjazamowienia testPozycjazamowienia = pozycjazamowieniaList.get(pozycjazamowieniaList.size() - 1);
        assertThat(testPozycjazamowienia.getPozycja()).isEqualTo(UPDATED_POZYCJA);
        assertThat(testPozycjazamowienia.getCenasztuka()).isEqualTo(UPDATED_CENASZTUKA);
        assertThat(testPozycjazamowienia.getIlosc()).isEqualTo(UPDATED_ILOSC);
    }

    @Test
    @Transactional
    public void updateNonExistingPozycjazamowienia() throws Exception {
        int databaseSizeBeforeUpdate = pozycjazamowieniaRepository.findAll().size();

        // Create the Pozycjazamowienia
        PozycjazamowieniaDTO pozycjazamowieniaDTO = pozycjazamowieniaMapper.toDto(pozycjazamowienia);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPozycjazamowieniaMockMvc.perform(put("/api/pozycjazamowienias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pozycjazamowieniaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pozycjazamowienia in the database
        List<Pozycjazamowienia> pozycjazamowieniaList = pozycjazamowieniaRepository.findAll();
        assertThat(pozycjazamowieniaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePozycjazamowienia() throws Exception {
        // Initialize the database
        pozycjazamowieniaRepository.saveAndFlush(pozycjazamowienia);

        int databaseSizeBeforeDelete = pozycjazamowieniaRepository.findAll().size();

        // Get the pozycjazamowienia
        restPozycjazamowieniaMockMvc.perform(delete("/api/pozycjazamowienias/{id}", pozycjazamowienia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Pozycjazamowienia> pozycjazamowieniaList = pozycjazamowieniaRepository.findAll();
        assertThat(pozycjazamowieniaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pozycjazamowienia.class);
        Pozycjazamowienia pozycjazamowienia1 = new Pozycjazamowienia();
        pozycjazamowienia1.setId(1L);
        Pozycjazamowienia pozycjazamowienia2 = new Pozycjazamowienia();
        pozycjazamowienia2.setId(pozycjazamowienia1.getId());
        assertThat(pozycjazamowienia1).isEqualTo(pozycjazamowienia2);
        pozycjazamowienia2.setId(2L);
        assertThat(pozycjazamowienia1).isNotEqualTo(pozycjazamowienia2);
        pozycjazamowienia1.setId(null);
        assertThat(pozycjazamowienia1).isNotEqualTo(pozycjazamowienia2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PozycjazamowieniaDTO.class);
        PozycjazamowieniaDTO pozycjazamowieniaDTO1 = new PozycjazamowieniaDTO();
        pozycjazamowieniaDTO1.setId(1L);
        PozycjazamowieniaDTO pozycjazamowieniaDTO2 = new PozycjazamowieniaDTO();
        assertThat(pozycjazamowieniaDTO1).isNotEqualTo(pozycjazamowieniaDTO2);
        pozycjazamowieniaDTO2.setId(pozycjazamowieniaDTO1.getId());
        assertThat(pozycjazamowieniaDTO1).isEqualTo(pozycjazamowieniaDTO2);
        pozycjazamowieniaDTO2.setId(2L);
        assertThat(pozycjazamowieniaDTO1).isNotEqualTo(pozycjazamowieniaDTO2);
        pozycjazamowieniaDTO1.setId(null);
        assertThat(pozycjazamowieniaDTO1).isNotEqualTo(pozycjazamowieniaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pozycjazamowieniaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pozycjazamowieniaMapper.fromId(null)).isNull();
    }
}
