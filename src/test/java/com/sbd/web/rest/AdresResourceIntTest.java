package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Adres;
import com.sbd.repository.AdresRepository;
import com.sbd.service.AdresService;
import com.sbd.service.dto.AdresDTO;
import com.sbd.service.mapper.AdresMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdresResource REST controller.
 *
 * @see AdresResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class AdresResourceIntTest {

    private static final String DEFAULT_PANSTWO = "AAAAAAAAAA";
    private static final String UPDATED_PANSTWO = "BBBBBBBBBB";

    private static final String DEFAULT_MIASTO = "AAAAAAAAAA";
    private static final String UPDATED_MIASTO = "BBBBBBBBBB";

    private static final String DEFAULT_ULICA = "AAAAAAAAAA";
    private static final String UPDATED_ULICA = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERDOMU = 1;
    private static final Integer UPDATED_NUMERDOMU = 2;

    private static final Integer DEFAULT_NUMERMIESZKANIA = 1;
    private static final Integer UPDATED_NUMERMIESZKANIA = 2;

    @Autowired
    private AdresRepository adresRepository;

    @Autowired
    private AdresMapper adresMapper;

    @Autowired
    private AdresService adresService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdresMockMvc;

    private Adres adres;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdresResource adresResource = new AdresResource(adresService);
        this.restAdresMockMvc = MockMvcBuilders.standaloneSetup(adresResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Adres createEntity(EntityManager em) {
        Adres adres = new Adres()
            .panstwo(DEFAULT_PANSTWO)
            .miasto(DEFAULT_MIASTO)
            .ulica(DEFAULT_ULICA)
            .numerdomu(DEFAULT_NUMERDOMU)
            .numermieszkania(DEFAULT_NUMERMIESZKANIA);
        return adres;
    }

    @Before
    public void initTest() {
        adres = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdres() throws Exception {
        int databaseSizeBeforeCreate = adresRepository.findAll().size();

        // Create the Adres
        AdresDTO adresDTO = adresMapper.toDto(adres);
        restAdresMockMvc.perform(post("/api/adres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adresDTO)))
            .andExpect(status().isCreated());

        // Validate the Adres in the database
        List<Adres> adresList = adresRepository.findAll();
        assertThat(adresList).hasSize(databaseSizeBeforeCreate + 1);
        Adres testAdres = adresList.get(adresList.size() - 1);
        assertThat(testAdres.getPanstwo()).isEqualTo(DEFAULT_PANSTWO);
        assertThat(testAdres.getMiasto()).isEqualTo(DEFAULT_MIASTO);
        assertThat(testAdres.getUlica()).isEqualTo(DEFAULT_ULICA);
        assertThat(testAdres.getNumerdomu()).isEqualTo(DEFAULT_NUMERDOMU);
        assertThat(testAdres.getNumermieszkania()).isEqualTo(DEFAULT_NUMERMIESZKANIA);
    }

    @Test
    @Transactional
    public void createAdresWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = adresRepository.findAll().size();

        // Create the Adres with an existing ID
        adres.setId(1L);
        AdresDTO adresDTO = adresMapper.toDto(adres);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdresMockMvc.perform(post("/api/adres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adresDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Adres in the database
        List<Adres> adresList = adresRepository.findAll();
        assertThat(adresList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAdres() throws Exception {
        // Initialize the database
        adresRepository.saveAndFlush(adres);

        // Get all the adresList
        restAdresMockMvc.perform(get("/api/adres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adres.getId().intValue())))
            .andExpect(jsonPath("$.[*].panstwo").value(hasItem(DEFAULT_PANSTWO.toString())))
            .andExpect(jsonPath("$.[*].miasto").value(hasItem(DEFAULT_MIASTO.toString())))
            .andExpect(jsonPath("$.[*].ulica").value(hasItem(DEFAULT_ULICA.toString())))
            .andExpect(jsonPath("$.[*].numerdomu").value(hasItem(DEFAULT_NUMERDOMU)))
            .andExpect(jsonPath("$.[*].numermieszkania").value(hasItem(DEFAULT_NUMERMIESZKANIA)));
    }
    
    @Test
    @Transactional
    public void getAdres() throws Exception {
        // Initialize the database
        adresRepository.saveAndFlush(adres);

        // Get the adres
        restAdresMockMvc.perform(get("/api/adres/{id}", adres.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(adres.getId().intValue()))
            .andExpect(jsonPath("$.panstwo").value(DEFAULT_PANSTWO.toString()))
            .andExpect(jsonPath("$.miasto").value(DEFAULT_MIASTO.toString()))
            .andExpect(jsonPath("$.ulica").value(DEFAULT_ULICA.toString()))
            .andExpect(jsonPath("$.numerdomu").value(DEFAULT_NUMERDOMU))
            .andExpect(jsonPath("$.numermieszkania").value(DEFAULT_NUMERMIESZKANIA));
    }

    @Test
    @Transactional
    public void getNonExistingAdres() throws Exception {
        // Get the adres
        restAdresMockMvc.perform(get("/api/adres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdres() throws Exception {
        // Initialize the database
        adresRepository.saveAndFlush(adres);

        int databaseSizeBeforeUpdate = adresRepository.findAll().size();

        // Update the adres
        Adres updatedAdres = adresRepository.findById(adres.getId()).get();
        // Disconnect from session so that the updates on updatedAdres are not directly saved in db
        em.detach(updatedAdres);
        updatedAdres
            .panstwo(UPDATED_PANSTWO)
            .miasto(UPDATED_MIASTO)
            .ulica(UPDATED_ULICA)
            .numerdomu(UPDATED_NUMERDOMU)
            .numermieszkania(UPDATED_NUMERMIESZKANIA);
        AdresDTO adresDTO = adresMapper.toDto(updatedAdres);

        restAdresMockMvc.perform(put("/api/adres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adresDTO)))
            .andExpect(status().isOk());

        // Validate the Adres in the database
        List<Adres> adresList = adresRepository.findAll();
        assertThat(adresList).hasSize(databaseSizeBeforeUpdate);
        Adres testAdres = adresList.get(adresList.size() - 1);
        assertThat(testAdres.getPanstwo()).isEqualTo(UPDATED_PANSTWO);
        assertThat(testAdres.getMiasto()).isEqualTo(UPDATED_MIASTO);
        assertThat(testAdres.getUlica()).isEqualTo(UPDATED_ULICA);
        assertThat(testAdres.getNumerdomu()).isEqualTo(UPDATED_NUMERDOMU);
        assertThat(testAdres.getNumermieszkania()).isEqualTo(UPDATED_NUMERMIESZKANIA);
    }

    @Test
    @Transactional
    public void updateNonExistingAdres() throws Exception {
        int databaseSizeBeforeUpdate = adresRepository.findAll().size();

        // Create the Adres
        AdresDTO adresDTO = adresMapper.toDto(adres);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdresMockMvc.perform(put("/api/adres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adresDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Adres in the database
        List<Adres> adresList = adresRepository.findAll();
        assertThat(adresList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdres() throws Exception {
        // Initialize the database
        adresRepository.saveAndFlush(adres);

        int databaseSizeBeforeDelete = adresRepository.findAll().size();

        // Get the adres
        restAdresMockMvc.perform(delete("/api/adres/{id}", adres.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Adres> adresList = adresRepository.findAll();
        assertThat(adresList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Adres.class);
        Adres adres1 = new Adres();
        adres1.setId(1L);
        Adres adres2 = new Adres();
        adres2.setId(adres1.getId());
        assertThat(adres1).isEqualTo(adres2);
        adres2.setId(2L);
        assertThat(adres1).isNotEqualTo(adres2);
        adres1.setId(null);
        assertThat(adres1).isNotEqualTo(adres2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdresDTO.class);
        AdresDTO adresDTO1 = new AdresDTO();
        adresDTO1.setId(1L);
        AdresDTO adresDTO2 = new AdresDTO();
        assertThat(adresDTO1).isNotEqualTo(adresDTO2);
        adresDTO2.setId(adresDTO1.getId());
        assertThat(adresDTO1).isEqualTo(adresDTO2);
        adresDTO2.setId(2L);
        assertThat(adresDTO1).isNotEqualTo(adresDTO2);
        adresDTO1.setId(null);
        assertThat(adresDTO1).isNotEqualTo(adresDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(adresMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(adresMapper.fromId(null)).isNull();
    }
}
