package com.sbd.web.rest;

import com.sbd.SbdsklepApp;

import com.sbd.domain.Zamowienie;
import com.sbd.repository.ZamowienieRepository;
import com.sbd.service.ZamowienieService;
import com.sbd.service.dto.ZamowienieDTO;
import com.sbd.service.mapper.ZamowienieMapper;
import com.sbd.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sbd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ZamowienieResource REST controller.
 *
 * @see ZamowienieResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SbdsklepApp.class)
public class ZamowienieResourceIntTest {

    private static final Float DEFAULT_CENACALKOWITA = 1F;
    private static final Float UPDATED_CENACALKOWITA = 2F;

    private static final Integer DEFAULT_ILOSCPRODUKTOW = 1;
    private static final Integer UPDATED_ILOSCPRODUKTOW = 2;

    @Autowired
    private ZamowienieRepository zamowienieRepository;

    @Autowired
    private ZamowienieMapper zamowienieMapper;

    @Autowired
    private ZamowienieService zamowienieService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restZamowienieMockMvc;

    private Zamowienie zamowienie;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ZamowienieResource zamowienieResource = new ZamowienieResource(zamowienieService);
        this.restZamowienieMockMvc = MockMvcBuilders.standaloneSetup(zamowienieResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zamowienie createEntity(EntityManager em) {
        Zamowienie zamowienie = new Zamowienie()
            .cenacalkowita(DEFAULT_CENACALKOWITA)
            .iloscproduktow(DEFAULT_ILOSCPRODUKTOW);
        return zamowienie;
    }

    @Before
    public void initTest() {
        zamowienie = createEntity(em);
    }

    @Test
    @Transactional
    public void createZamowienie() throws Exception {
        int databaseSizeBeforeCreate = zamowienieRepository.findAll().size();

        // Create the Zamowienie
        ZamowienieDTO zamowienieDTO = zamowienieMapper.toDto(zamowienie);
        restZamowienieMockMvc.perform(post("/api/zamowienies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zamowienieDTO)))
            .andExpect(status().isCreated());

        // Validate the Zamowienie in the database
        List<Zamowienie> zamowienieList = zamowienieRepository.findAll();
        assertThat(zamowienieList).hasSize(databaseSizeBeforeCreate + 1);
        Zamowienie testZamowienie = zamowienieList.get(zamowienieList.size() - 1);
        assertThat(testZamowienie.getCenacalkowita()).isEqualTo(DEFAULT_CENACALKOWITA);
        assertThat(testZamowienie.getIloscproduktow()).isEqualTo(DEFAULT_ILOSCPRODUKTOW);
    }

    @Test
    @Transactional
    public void createZamowienieWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = zamowienieRepository.findAll().size();

        // Create the Zamowienie with an existing ID
        zamowienie.setId(1L);
        ZamowienieDTO zamowienieDTO = zamowienieMapper.toDto(zamowienie);

        // An entity with an existing ID cannot be created, so this API call must fail
        restZamowienieMockMvc.perform(post("/api/zamowienies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zamowienieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Zamowienie in the database
        List<Zamowienie> zamowienieList = zamowienieRepository.findAll();
        assertThat(zamowienieList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllZamowienies() throws Exception {
        // Initialize the database
        zamowienieRepository.saveAndFlush(zamowienie);

        // Get all the zamowienieList
        restZamowienieMockMvc.perform(get("/api/zamowienies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zamowienie.getId().intValue())))
            .andExpect(jsonPath("$.[*].cenacalkowita").value(hasItem(DEFAULT_CENACALKOWITA.doubleValue())))
            .andExpect(jsonPath("$.[*].iloscproduktow").value(hasItem(DEFAULT_ILOSCPRODUKTOW)));
    }
    
    @Test
    @Transactional
    public void getZamowienie() throws Exception {
        // Initialize the database
        zamowienieRepository.saveAndFlush(zamowienie);

        // Get the zamowienie
        restZamowienieMockMvc.perform(get("/api/zamowienies/{id}", zamowienie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(zamowienie.getId().intValue()))
            .andExpect(jsonPath("$.cenacalkowita").value(DEFAULT_CENACALKOWITA.doubleValue()))
            .andExpect(jsonPath("$.iloscproduktow").value(DEFAULT_ILOSCPRODUKTOW));
    }

    @Test
    @Transactional
    public void getNonExistingZamowienie() throws Exception {
        // Get the zamowienie
        restZamowienieMockMvc.perform(get("/api/zamowienies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZamowienie() throws Exception {
        // Initialize the database
        zamowienieRepository.saveAndFlush(zamowienie);

        int databaseSizeBeforeUpdate = zamowienieRepository.findAll().size();

        // Update the zamowienie
        Zamowienie updatedZamowienie = zamowienieRepository.findById(zamowienie.getId()).get();
        // Disconnect from session so that the updates on updatedZamowienie are not directly saved in db
        em.detach(updatedZamowienie);
        updatedZamowienie
            .cenacalkowita(UPDATED_CENACALKOWITA)
            .iloscproduktow(UPDATED_ILOSCPRODUKTOW);
        ZamowienieDTO zamowienieDTO = zamowienieMapper.toDto(updatedZamowienie);

        restZamowienieMockMvc.perform(put("/api/zamowienies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zamowienieDTO)))
            .andExpect(status().isOk());

        // Validate the Zamowienie in the database
        List<Zamowienie> zamowienieList = zamowienieRepository.findAll();
        assertThat(zamowienieList).hasSize(databaseSizeBeforeUpdate);
        Zamowienie testZamowienie = zamowienieList.get(zamowienieList.size() - 1);
        assertThat(testZamowienie.getCenacalkowita()).isEqualTo(UPDATED_CENACALKOWITA);
        assertThat(testZamowienie.getIloscproduktow()).isEqualTo(UPDATED_ILOSCPRODUKTOW);
    }

    @Test
    @Transactional
    public void updateNonExistingZamowienie() throws Exception {
        int databaseSizeBeforeUpdate = zamowienieRepository.findAll().size();

        // Create the Zamowienie
        ZamowienieDTO zamowienieDTO = zamowienieMapper.toDto(zamowienie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restZamowienieMockMvc.perform(put("/api/zamowienies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zamowienieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Zamowienie in the database
        List<Zamowienie> zamowienieList = zamowienieRepository.findAll();
        assertThat(zamowienieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteZamowienie() throws Exception {
        // Initialize the database
        zamowienieRepository.saveAndFlush(zamowienie);

        int databaseSizeBeforeDelete = zamowienieRepository.findAll().size();

        // Get the zamowienie
        restZamowienieMockMvc.perform(delete("/api/zamowienies/{id}", zamowienie.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Zamowienie> zamowienieList = zamowienieRepository.findAll();
        assertThat(zamowienieList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Zamowienie.class);
        Zamowienie zamowienie1 = new Zamowienie();
        zamowienie1.setId(1L);
        Zamowienie zamowienie2 = new Zamowienie();
        zamowienie2.setId(zamowienie1.getId());
        assertThat(zamowienie1).isEqualTo(zamowienie2);
        zamowienie2.setId(2L);
        assertThat(zamowienie1).isNotEqualTo(zamowienie2);
        zamowienie1.setId(null);
        assertThat(zamowienie1).isNotEqualTo(zamowienie2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ZamowienieDTO.class);
        ZamowienieDTO zamowienieDTO1 = new ZamowienieDTO();
        zamowienieDTO1.setId(1L);
        ZamowienieDTO zamowienieDTO2 = new ZamowienieDTO();
        assertThat(zamowienieDTO1).isNotEqualTo(zamowienieDTO2);
        zamowienieDTO2.setId(zamowienieDTO1.getId());
        assertThat(zamowienieDTO1).isEqualTo(zamowienieDTO2);
        zamowienieDTO2.setId(2L);
        assertThat(zamowienieDTO1).isNotEqualTo(zamowienieDTO2);
        zamowienieDTO1.setId(null);
        assertThat(zamowienieDTO1).isNotEqualTo(zamowienieDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(zamowienieMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(zamowienieMapper.fromId(null)).isNull();
    }
}
